/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[87985530b0b8ba7f825c77196a17d928]
 */

package com.lowes.app.cmdm.mrp.entityObject;

import com.dwl.base.EObjCommon;
import com.ibm.mdm.base.db.DataType;
import com.ibm.pdq.annotation.Column;
import com.ibm.pdq.annotation.Table;


import com.ibm.pdq.annotation.Id;

import java.sql.Timestamp;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * The entity object corresponding to the MilitaryInfo business object. This
 * entity object should include all the attributes as defined by the business
 * object.
 * 
 * @generated
 */
@SuppressWarnings("serial")
@Table(name=EObjMilitaryInfo.tableName)
public class EObjMilitaryInfo extends EObjCommon {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public static final String tableName = "T417_MTY_CUS_AFL";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String militaryInfoPkIdColumn = "T417_MTY_CUS_AFL_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String militaryInfoPkIdJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    militaryInfoPkIdPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String partyIdColumn = "CONT_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String partyIdJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    partyIdPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String bRANCH_TP_CDColumn = "T416_MTY_LCT_CD";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String bRANCH_TP_CDJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    bRANCH_TP_CDPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String affiliationColumn = "T395_MTY_AFL_STS_CD";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String affiliationJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    affiliationPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String serviceBeginDateColumn = "SRV_BGN_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String serviceBeginDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String serviceEndDateColumn = "SRV_END_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String serviceEndDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String startDateColumn = "START_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String startDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String endDateColumn = "END_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String endDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String lastVerifiedByColumn = "LAST_VRF_BY_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String lastVerifiedByJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    lastVerifiedByPrecision = 20;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long militaryInfoPkId;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long partyId;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long bRANCH_TP_CD;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long affiliation;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp serviceBeginDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp serviceEndDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp startDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp endDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String lastVerifiedBy;



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.     
     *
     * @generated
     */
    public EObjMilitaryInfo() {
        super();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the militaryInfoPkId attribute. 
     *
     * @generated
     */
    @Id
    @Column(name=militaryInfoPkIdColumn)
    @DataType(jdbcType=militaryInfoPkIdJdbcType, precision=militaryInfoPkIdPrecision)
    public Long getMilitaryInfoPkId (){
        return militaryInfoPkId;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the militaryInfoPkId attribute. 
     *
     * @param militaryInfoPkId
     *     The new value of MilitaryInfoPkId. 
     * @generated
     */
    public void setMilitaryInfoPkId( Long militaryInfoPkId ){
        this.militaryInfoPkId = militaryInfoPkId;
    
        super.setIdPK(militaryInfoPkId);
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the partyId attribute. 
     *
     * @generated
     */
    @Column(name=partyIdColumn)
    @DataType(jdbcType=partyIdJdbcType, precision=partyIdPrecision)
    public Long getPartyId (){
        return partyId;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the partyId attribute. 
     *
     * @param partyId
     *     The new value of PartyId. 
     * @generated
     */
    public void setPartyId( Long partyId ){
        this.partyId = partyId;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the bRANCH_TP_CD attribute. 
     *
     * @generated
     */
    @Column(name=bRANCH_TP_CDColumn)
    @DataType(jdbcType=bRANCH_TP_CDJdbcType, precision=bRANCH_TP_CDPrecision)
    public Long getBRANCH_TP_CD (){
        return bRANCH_TP_CD;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the bRANCH_TP_CD attribute. 
     *
     * @param bRANCH_TP_CD
     *     The new value of BRANCH_TP_CD. 
     * @generated
     */
    public void setBRANCH_TP_CD( Long bRANCH_TP_CD ){
        this.bRANCH_TP_CD = bRANCH_TP_CD;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the affiliation attribute. 
     *
     * @generated
     */
    @Column(name=affiliationColumn)
    @DataType(jdbcType=affiliationJdbcType, precision=affiliationPrecision)
    public Long getAffiliation (){
        return affiliation;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the affiliation attribute. 
     *
     * @param affiliation
     *     The new value of Affiliation. 
     * @generated
     */
    public void setAffiliation( Long affiliation ){
        this.affiliation = affiliation;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the serviceBeginDate attribute. 
     *
     * @generated
     */
    @Column(name=serviceBeginDateColumn)
    @DataType(jdbcType=serviceBeginDateJdbcType)
    public Timestamp getServiceBeginDate (){
        return serviceBeginDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the serviceBeginDate attribute. 
     *
     * @param serviceBeginDate
     *     The new value of ServiceBeginDate. 
     * @generated
     */
    public void setServiceBeginDate( Timestamp serviceBeginDate ){
        this.serviceBeginDate = serviceBeginDate;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the serviceEndDate attribute. 
     *
     * @generated
     */
    @Column(name=serviceEndDateColumn)
    @DataType(jdbcType=serviceEndDateJdbcType)
    public Timestamp getServiceEndDate (){
        return serviceEndDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the serviceEndDate attribute. 
     *
     * @param serviceEndDate
     *     The new value of ServiceEndDate. 
     * @generated
     */
    public void setServiceEndDate( Timestamp serviceEndDate ){
        this.serviceEndDate = serviceEndDate;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the startDate attribute. 
     *
     * @generated
     */
    @Column(name=startDateColumn)
    @DataType(jdbcType=startDateJdbcType)
    public Timestamp getStartDate (){
        return startDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the startDate attribute. 
     *
     * @param startDate
     *     The new value of StartDate. 
     * @generated
     */
    public void setStartDate( Timestamp startDate ){
        this.startDate = startDate;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the endDate attribute. 
     *
     * @generated
     */
    @Column(name=endDateColumn)
    @DataType(jdbcType=endDateJdbcType)
    public Timestamp getEndDate (){
        return endDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the endDate attribute. 
     *
     * @param endDate
     *     The new value of EndDate. 
     * @generated
     */
    public void setEndDate( Timestamp endDate ){
        this.endDate = endDate;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the lastVerifiedBy attribute. 
     *
     * @generated
     */
    @Column(name=lastVerifiedByColumn)
    @DataType(jdbcType=lastVerifiedByJdbcType, precision=lastVerifiedByPrecision)
    public String getLastVerifiedBy (){
        return lastVerifiedBy;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the lastVerifiedBy attribute. 
     *
     * @param lastVerifiedBy
     *     The new value of LastVerifiedBy. 
     * @generated
     */
    public void setLastVerifiedBy( String lastVerifiedBy ){
        this.lastVerifiedBy = lastVerifiedBy;
    
  }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the primary key. 
     *
     * @param aUniqueId
     *     The new value of the primary key. 
     * @generated
   */
	public void setPrimaryKey(Object aUniqueId) {
    this.setMilitaryInfoPkId((Long)aUniqueId);
  }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the primary key.
     *
     * @generated
     */
	public Object getPrimaryKey() {
    return this.getMilitaryInfoPkId();
  }
	 
}


