/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[c8bad58008a790c26f67729433db4e66]
 */


package com.lowes.app.cmdm.mrp.entityObject;

import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;
import com.ibm.pdq.annotation.Update;

import com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface EObjMilitaryInfoData {


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getEObjMilitaryInfoSql = "select T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ? ";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String createEObjMilitaryInfoSql = "insert into T417_MTY_CUS_AFL (T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :militaryInfoPkId, :partyId, :bRANCH_TP_CD, :affiliation, :serviceBeginDate, :serviceEndDate, :startDate, :endDate, :lastVerifiedBy, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String updateEObjMilitaryInfoSql = "update T417_MTY_CUS_AFL set CONT_ID = :partyId, T416_MTY_LCT_CD = :bRANCH_TP_CD, T395_MTY_AFL_STS_CD = :affiliation, SRV_BGN_DT = :serviceBeginDate, SRV_END_DT = :serviceEndDate, START_DT = :startDate, END_DT = :endDate, LAST_VRF_BY_ID = :lastVerifiedBy, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where T417_MTY_CUS_AFL_ID = :militaryInfoPkId and LAST_UPDATE_DT = :oldLastUpdateDt";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String deleteEObjMilitaryInfoSql = "delete from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ?";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjMilitaryInfoKeyField = "EObjMilitaryInfo.militaryInfoPkId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjMilitaryInfoGetFields =
    "EObjMilitaryInfo.militaryInfoPkId," +
    "EObjMilitaryInfo.partyId," +
    "EObjMilitaryInfo.bRANCH_TP_CD," +
    "EObjMilitaryInfo.affiliation," +
    "EObjMilitaryInfo.serviceBeginDate," +
    "EObjMilitaryInfo.serviceEndDate," +
    "EObjMilitaryInfo.startDate," +
    "EObjMilitaryInfo.endDate," +
    "EObjMilitaryInfo.lastVerifiedBy," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateUser," +
    "EObjMilitaryInfo.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjMilitaryInfoAllFields =
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.militaryInfoPkId," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.partyId," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.bRANCH_TP_CD," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.affiliation," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.serviceBeginDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.serviceEndDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.startDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.endDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastVerifiedBy," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateDt," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateUser," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjMilitaryInfoUpdateFields =
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.partyId," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.bRANCH_TP_CD," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.affiliation," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.serviceBeginDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.serviceEndDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.startDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.endDate," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastVerifiedBy," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateDt," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateUser," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.lastUpdateTxId," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.militaryInfoPkId," +
    "com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo.oldLastUpdateDt";   

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Select MilitaryInfo by parameters.
   * @generated
   */
  @Select(sql=getEObjMilitaryInfoSql)
  @EntityMapping(parameters=EObjMilitaryInfoKeyField, results=EObjMilitaryInfoGetFields)
  Iterator<EObjMilitaryInfo> getEObjMilitaryInfo(Long militaryInfoPkId);  
   
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Create MilitaryInfo by EObjMilitaryInfo Object.
   * @generated
   */
  @Update(sql=createEObjMilitaryInfoSql)
  @EntityMapping(parameters=EObjMilitaryInfoAllFields)
    int createEObjMilitaryInfo(EObjMilitaryInfo e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Update one MilitaryInfo by EObjMilitaryInfo object.
   * @generated
   */
  @Update(sql=updateEObjMilitaryInfoSql)
  @EntityMapping(parameters=EObjMilitaryInfoUpdateFields)
    int updateEObjMilitaryInfo(EObjMilitaryInfo e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Delete MilitaryInfo by parameters.
   * @generated
   */
  @Update(sql=deleteEObjMilitaryInfoSql)
  @EntityMapping(parameters=EObjMilitaryInfoKeyField)
  int deleteEObjMilitaryInfo(Long militaryInfoPkId);

}

