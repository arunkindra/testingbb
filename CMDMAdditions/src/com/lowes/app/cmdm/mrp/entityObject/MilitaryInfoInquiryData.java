/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[9ee374ba24acb65e2c73d873e8e707a7]
 */

package com.lowes.app.cmdm.mrp.entityObject;


import com.ibm.mdm.base.db.ResultQueue1;
import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface MilitaryInfoInquiryData {
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String tableAliasString = "tableAlias (" + 
                                            "T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo, " +
                                            "H_T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo" +
                                            ")";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getMilitaryInfoSql = "SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.T417_MTY_CUS_AFL_ID = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getMilitaryInfoParameters =
    "EObjMilitaryInfo.MilitaryInfoPkId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getMilitaryInfoResults =
    "EObjMilitaryInfo.MilitaryInfoPkId," +
    "EObjMilitaryInfo.PartyId," +
    "EObjMilitaryInfo.BRANCH_TP_CD," +
    "EObjMilitaryInfo.Affiliation," +
    "EObjMilitaryInfo.ServiceBeginDate," +
    "EObjMilitaryInfo.ServiceEndDate," +
    "EObjMilitaryInfo.StartDate," +
    "EObjMilitaryInfo.EndDate," +
    "EObjMilitaryInfo.LastVerifiedBy," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateUser," +
    "EObjMilitaryInfo.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getMilitaryInfoHistorySql = "SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.H_T417_MTY_CUS_AFL_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getMilitaryInfoHistoryParameters =
    "EObjMilitaryInfo.MilitaryInfoPkId," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateDt";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getMilitaryInfoHistoryResults =
    "EObjMilitaryInfo.historyIdPK," +
    "EObjMilitaryInfo.histActionCode," +
    "EObjMilitaryInfo.histCreatedBy," +
    "EObjMilitaryInfo.histCreateDt," +
    "EObjMilitaryInfo.histEndDt," +
    "EObjMilitaryInfo.MilitaryInfoPkId," +
    "EObjMilitaryInfo.PartyId," +
    "EObjMilitaryInfo.BRANCH_TP_CD," +
    "EObjMilitaryInfo.Affiliation," +
    "EObjMilitaryInfo.ServiceBeginDate," +
    "EObjMilitaryInfo.ServiceEndDate," +
    "EObjMilitaryInfo.StartDate," +
    "EObjMilitaryInfo.EndDate," +
    "EObjMilitaryInfo.LastVerifiedBy," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateUser," +
    "EObjMilitaryInfo.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getAllMilitaryInfoByPartyIdSql = "SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.CONT_ID = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllMilitaryInfoByPartyIdParameters =
    "EObjMilitaryInfo.PartyId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllMilitaryInfoByPartyIdResults =
    "EObjMilitaryInfo.MilitaryInfoPkId," +
    "EObjMilitaryInfo.PartyId," +
    "EObjMilitaryInfo.BRANCH_TP_CD," +
    "EObjMilitaryInfo.Affiliation," +
    "EObjMilitaryInfo.ServiceBeginDate," +
    "EObjMilitaryInfo.ServiceEndDate," +
    "EObjMilitaryInfo.StartDate," +
    "EObjMilitaryInfo.EndDate," +
    "EObjMilitaryInfo.LastVerifiedBy," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateUser," +
    "EObjMilitaryInfo.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getAllMilitaryInfoByPartyIdHistorySql = "SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.CONT_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllMilitaryInfoByPartyIdHistoryParameters =
    "EObjMilitaryInfo.PartyId," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateDt";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllMilitaryInfoByPartyIdHistoryResults =
    "EObjMilitaryInfo.historyIdPK," +
    "EObjMilitaryInfo.histActionCode," +
    "EObjMilitaryInfo.histCreatedBy," +
    "EObjMilitaryInfo.histCreateDt," +
    "EObjMilitaryInfo.histEndDt," +
    "EObjMilitaryInfo.MilitaryInfoPkId," +
    "EObjMilitaryInfo.PartyId," +
    "EObjMilitaryInfo.BRANCH_TP_CD," +
    "EObjMilitaryInfo.Affiliation," +
    "EObjMilitaryInfo.ServiceBeginDate," +
    "EObjMilitaryInfo.ServiceEndDate," +
    "EObjMilitaryInfo.StartDate," +
    "EObjMilitaryInfo.EndDate," +
    "EObjMilitaryInfo.LastVerifiedBy," +
    "EObjMilitaryInfo.lastUpdateDt," +
    "EObjMilitaryInfo.lastUpdateUser," +
    "EObjMilitaryInfo.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getMilitaryInfoSql, pattern=tableAliasString)
  @EntityMapping(parameters=getMilitaryInfoParameters, results=getMilitaryInfoResults)
  Iterator<ResultQueue1<EObjMilitaryInfo>> getMilitaryInfo(Object[] parameters);  


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getMilitaryInfoHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getMilitaryInfoHistoryParameters, results=getMilitaryInfoHistoryResults)
  Iterator<ResultQueue1<EObjMilitaryInfo>> getMilitaryInfoHistory(Object[] parameters);


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllMilitaryInfoByPartyIdSql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllMilitaryInfoByPartyIdParameters, results=getAllMilitaryInfoByPartyIdResults)
  Iterator<ResultQueue1<EObjMilitaryInfo>> getAllMilitaryInfoByPartyId(Object[] parameters);


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllMilitaryInfoByPartyIdHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllMilitaryInfoByPartyIdHistoryParameters, results=getAllMilitaryInfoByPartyIdHistoryResults)
  Iterator<ResultQueue1<EObjMilitaryInfo>> getAllMilitaryInfoByPartyIdHistory(Object[] parameters);  


}


