package com.lowes.app.cmdm.mrp.entityObject;

import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.ibm.mdm.base.db.ResultQueue1;
import com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.statement.StatementDescriptor;
import com.ibm.pdq.runtime.generator.BaseData;
import java.sql.SQLException;
import com.ibm.pdq.annotation.Metadata;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import java.sql.Types;


@SuppressWarnings("unchecked")

/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MilitaryInfoInquiryDataImpl  extends BaseData implements MilitaryInfoInquiryData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "MilitaryInfoInquiryData";

  /**
   * @generated
   */
  public static final long generationTime = 0x000001585242a04eL;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public MilitaryInfoInquiryDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.T417_MTY_CUS_AFL_ID = ? ", pattern="tableAlias (T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo, H_T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjMilitaryInfo>> getMilitaryInfo (Object[] parameters)
  {
    return queryIterator (getMilitaryInfoStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getMilitaryInfoStatementDescriptor = createStatementDescriptor (
    "getMilitaryInfo(Object[])",
    "SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.T417_MTY_CUS_AFL_ID = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"t417_mty_cus_afl_id", "cont_id", "t416_mty_lct_cd", "t395_mty_afl_sts_cd", "srv_bgn_dt", "srv_end_dt", "start_dt", "end_dt", "last_vrf_by_id", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetMilitaryInfoParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetMilitaryInfoRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetMilitaryInfoParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetMilitaryInfoRowHandler extends BaseRowHandler<ResultQueue1<EObjMilitaryInfo>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjMilitaryInfo> handle (java.sql.ResultSet rs, ResultQueue1<EObjMilitaryInfo> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjMilitaryInfo> ();

      EObjMilitaryInfo returnObject1 = new EObjMilitaryInfo ();
      returnObject1.setMilitaryInfoPkId(getLongObject (rs, 1)); 
      returnObject1.setPartyId(getLongObject (rs, 2)); 
      returnObject1.setBRANCH_TP_CD(getLongObject (rs, 3)); 
      returnObject1.setAffiliation(getLongObject (rs, 4)); 
      returnObject1.setServiceBeginDate(getTimestamp (rs, 5)); 
      returnObject1.setServiceEndDate(getTimestamp (rs, 6)); 
      returnObject1.setStartDate(getTimestamp (rs, 7)); 
      returnObject1.setEndDate(getTimestamp (rs, 8)); 
      returnObject1.setLastVerifiedBy(getString (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.H_T417_MTY_CUS_AFL_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))", pattern="tableAlias (T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo, H_T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjMilitaryInfo>> getMilitaryInfoHistory (Object[] parameters)
  {
    return queryIterator (getMilitaryInfoHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getMilitaryInfoHistoryStatementDescriptor = createStatementDescriptor (
    "getMilitaryInfoHistory(Object[])",
    "SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.H_T417_MTY_CUS_AFL_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "t417_mty_cus_afl_id", "cont_id", "t416_mty_lct_cd", "t395_mty_afl_sts_cd", "srv_bgn_dt", "srv_end_dt", "start_dt", "end_dt", "last_vrf_by_id", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetMilitaryInfoHistoryParameterHandler (),
    new int[][]{{Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {19, 0, 0}, {0, 0, 0}, {1, 1, 1}},
    null,
    new GetMilitaryInfoHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class GetMilitaryInfoHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
      setObject (stmt, 2, Types.TIMESTAMP, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetMilitaryInfoHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjMilitaryInfo>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjMilitaryInfo> handle (java.sql.ResultSet rs, ResultQueue1<EObjMilitaryInfo> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjMilitaryInfo> ();

      EObjMilitaryInfo returnObject1 = new EObjMilitaryInfo ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setMilitaryInfoPkId(getLongObject (rs, 6)); 
      returnObject1.setPartyId(getLongObject (rs, 7)); 
      returnObject1.setBRANCH_TP_CD(getLongObject (rs, 8)); 
      returnObject1.setAffiliation(getLongObject (rs, 9)); 
      returnObject1.setServiceBeginDate(getTimestamp (rs, 10)); 
      returnObject1.setServiceEndDate(getTimestamp (rs, 11)); 
      returnObject1.setStartDate(getTimestamp (rs, 12)); 
      returnObject1.setEndDate(getTimestamp (rs, 13)); 
      returnObject1.setLastVerifiedBy(getString (rs, 14)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 15)); 
      returnObject1.setLastUpdateUser(getString (rs, 16)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 17)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.CONT_ID = ? ", pattern="tableAlias (T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo, H_T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjMilitaryInfo>> getAllMilitaryInfoByPartyId (Object[] parameters)
  {
    return queryIterator (getAllMilitaryInfoByPartyIdStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllMilitaryInfoByPartyIdStatementDescriptor = createStatementDescriptor (
    "getAllMilitaryInfoByPartyId(Object[])",
    "SELECT r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM T417_MTY_CUS_AFL r WHERE r.CONT_ID = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"t417_mty_cus_afl_id", "cont_id", "t416_mty_lct_cd", "t395_mty_afl_sts_cd", "srv_bgn_dt", "srv_end_dt", "start_dt", "end_dt", "last_vrf_by_id", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllMilitaryInfoByPartyIdParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetAllMilitaryInfoByPartyIdRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    3);

  /**
   * @generated
   */
  public static class GetAllMilitaryInfoByPartyIdParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllMilitaryInfoByPartyIdRowHandler extends BaseRowHandler<ResultQueue1<EObjMilitaryInfo>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjMilitaryInfo> handle (java.sql.ResultSet rs, ResultQueue1<EObjMilitaryInfo> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjMilitaryInfo> ();

      EObjMilitaryInfo returnObject1 = new EObjMilitaryInfo ();
      returnObject1.setMilitaryInfoPkId(getLongObject (rs, 1)); 
      returnObject1.setPartyId(getLongObject (rs, 2)); 
      returnObject1.setBRANCH_TP_CD(getLongObject (rs, 3)); 
      returnObject1.setAffiliation(getLongObject (rs, 4)); 
      returnObject1.setServiceBeginDate(getTimestamp (rs, 5)); 
      returnObject1.setServiceEndDate(getTimestamp (rs, 6)); 
      returnObject1.setStartDate(getTimestamp (rs, 7)); 
      returnObject1.setEndDate(getTimestamp (rs, 8)); 
      returnObject1.setLastVerifiedBy(getString (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.CONT_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))", pattern="tableAlias (T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo, H_T417_MTY_CUS_AFL => com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjMilitaryInfo>> getAllMilitaryInfoByPartyIdHistory (Object[] parameters)
  {
    return queryIterator (getAllMilitaryInfoByPartyIdHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllMilitaryInfoByPartyIdHistoryStatementDescriptor = createStatementDescriptor (
    "getAllMilitaryInfoByPartyIdHistory(Object[])",
    "SELECT r.H_T417_MTY_CUS_AFL_ID hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.T417_MTY_CUS_AFL_ID T417_MTY_CUS_AFL_ID, r.CONT_ID CONT_ID, r.T416_MTY_LCT_CD T416_MTY_LCT_CD, r.T395_MTY_AFL_STS_CD T395_MTY_AFL_STS_CD, r.SRV_BGN_DT SRV_BGN_DT, r.SRV_END_DT SRV_END_DT, r.START_DT START_DT, r.END_DT END_DT, r.LAST_VRF_BY_ID LAST_VRF_BY_ID, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_T417_MTY_CUS_AFL r WHERE r.CONT_ID = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "t417_mty_cus_afl_id", "cont_id", "t416_mty_lct_cd", "t395_mty_afl_sts_cd", "srv_bgn_dt", "srv_end_dt", "start_dt", "end_dt", "last_vrf_by_id", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllMilitaryInfoByPartyIdHistoryParameterHandler (),
    new int[][]{{Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {19, 0, 0}, {0, 0, 0}, {1, 1, 1}},
    null,
    new GetAllMilitaryInfoByPartyIdHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    4);

  /**
   * @generated
   */
  public static class GetAllMilitaryInfoByPartyIdHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
      setObject (stmt, 2, Types.TIMESTAMP, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllMilitaryInfoByPartyIdHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjMilitaryInfo>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjMilitaryInfo> handle (java.sql.ResultSet rs, ResultQueue1<EObjMilitaryInfo> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjMilitaryInfo> ();

      EObjMilitaryInfo returnObject1 = new EObjMilitaryInfo ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setMilitaryInfoPkId(getLongObject (rs, 6)); 
      returnObject1.setPartyId(getLongObject (rs, 7)); 
      returnObject1.setBRANCH_TP_CD(getLongObject (rs, 8)); 
      returnObject1.setAffiliation(getLongObject (rs, 9)); 
      returnObject1.setServiceBeginDate(getTimestamp (rs, 10)); 
      returnObject1.setServiceEndDate(getTimestamp (rs, 11)); 
      returnObject1.setStartDate(getTimestamp (rs, 12)); 
      returnObject1.setEndDate(getTimestamp (rs, 13)); 
      returnObject1.setLastVerifiedBy(getString (rs, 14)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 15)); 
      returnObject1.setLastUpdateUser(getString (rs, 16)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 17)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

}
