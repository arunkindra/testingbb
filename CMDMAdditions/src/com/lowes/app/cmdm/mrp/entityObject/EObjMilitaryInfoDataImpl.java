package com.lowes.app.cmdm.mrp.entityObject;

import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.statement.StatementDescriptor;
import com.ibm.pdq.runtime.generator.BaseData;
import java.sql.SQLException;
import com.ibm.pdq.annotation.Metadata;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import java.sql.Types;


/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class EObjMilitaryInfoDataImpl  extends BaseData implements EObjMilitaryInfoData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "EObjMilitaryInfoData";

  /**
   * @generated
   */
  public static final long generationTime = 0x0000015852429f05L;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public EObjMilitaryInfoDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="select T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ? " )
   * 
   * @generated
   */
  public Iterator<EObjMilitaryInfo> getEObjMilitaryInfo (Long militaryInfoPkId)
  {
    return queryIterator (getEObjMilitaryInfoStatementDescriptor, militaryInfoPkId);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getEObjMilitaryInfoStatementDescriptor = createStatementDescriptor (
    "getEObjMilitaryInfo(Long)",
    "select T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"t417_mty_cus_afl_id", "cont_id", "t416_mty_lct_cd", "t395_mty_afl_sts_cd", "srv_bgn_dt", "srv_end_dt", "start_dt", "end_dt", "last_vrf_by_id", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetEObjMilitaryInfoParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetEObjMilitaryInfoRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetEObjMilitaryInfoParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

  /**
   * @generated
   */
  public static class GetEObjMilitaryInfoRowHandler extends BaseRowHandler<EObjMilitaryInfo>
  {
    /**
     * @generated
     */
    public EObjMilitaryInfo handle (java.sql.ResultSet rs, EObjMilitaryInfo returnObject) throws java.sql.SQLException
    {
      returnObject = new EObjMilitaryInfo ();
      returnObject.setMilitaryInfoPkId(getLongObject (rs, 1)); 
      returnObject.setPartyId(getLongObject (rs, 2)); 
      returnObject.setBRANCH_TP_CD(getLongObject (rs, 3)); 
      returnObject.setAffiliation(getLongObject (rs, 4)); 
      returnObject.setServiceBeginDate(getTimestamp (rs, 5)); 
      returnObject.setServiceEndDate(getTimestamp (rs, 6)); 
      returnObject.setStartDate(getTimestamp (rs, 7)); 
      returnObject.setEndDate(getTimestamp (rs, 8)); 
      returnObject.setLastVerifiedBy(getString (rs, 9)); 
      returnObject.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject.setLastUpdateUser(getString (rs, 11)); 
      returnObject.setLastUpdateTxId(getLongObject (rs, 12)); 
    
      return returnObject;
    }
  }

  /**
   * @Update( sql="insert into T417_MTY_CUS_AFL (T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :militaryInfoPkId, :partyId, :bRANCH_TP_CD, :affiliation, :serviceBeginDate, :serviceEndDate, :startDate, :endDate, :lastVerifiedBy, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)" )
   * 
   * @generated
   */
  public int createEObjMilitaryInfo (EObjMilitaryInfo e)
  {
    return update (createEObjMilitaryInfoStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor createEObjMilitaryInfoStatementDescriptor = createStatementDescriptor (
    "createEObjMilitaryInfo(com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)",
    "insert into T417_MTY_CUS_AFL (T417_MTY_CUS_AFL_ID, CONT_ID, T416_MTY_LCT_CD, T395_MTY_AFL_STS_CD, SRV_BGN_DT, SRV_END_DT, START_DT, END_DT, LAST_VRF_BY_ID, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values(  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? )",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.INSERT,
    null,
    new CreateEObjMilitaryInfoParameterHandler (),
    new int[][]{{Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 19, 19, 0, 0, 0, 0, 20, 0, 0, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class CreateEObjMilitaryInfoParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjMilitaryInfo bean0 = (EObjMilitaryInfo) parameters[0];
      setLong (stmt, 1, Types.BIGINT, (Long)bean0.getMilitaryInfoPkId());
      setLong (stmt, 2, Types.BIGINT, (Long)bean0.getPartyId());
      setLong (stmt, 3, Types.BIGINT, (Long)bean0.getBRANCH_TP_CD());
      setLong (stmt, 4, Types.BIGINT, (Long)bean0.getAffiliation());
      setTimestamp (stmt, 5, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getServiceBeginDate());
      setTimestamp (stmt, 6, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getServiceEndDate());
      setTimestamp (stmt, 7, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getStartDate());
      setTimestamp (stmt, 8, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEndDate());
      setString (stmt, 9, Types.VARCHAR, (String)bean0.getLastVerifiedBy());
      setTimestamp (stmt, 10, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 11, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 12, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
    }
  }

  /**
   * @Update( sql="update T417_MTY_CUS_AFL set CONT_ID = :partyId, T416_MTY_LCT_CD = :bRANCH_TP_CD, T395_MTY_AFL_STS_CD = :affiliation, SRV_BGN_DT = :serviceBeginDate, SRV_END_DT = :serviceEndDate, START_DT = :startDate, END_DT = :endDate, LAST_VRF_BY_ID = :lastVerifiedBy, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where T417_MTY_CUS_AFL_ID = :militaryInfoPkId and LAST_UPDATE_DT = :oldLastUpdateDt" )
   * 
   * @generated
   */
  public int updateEObjMilitaryInfo (EObjMilitaryInfo e)
  {
    return update (updateEObjMilitaryInfoStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor updateEObjMilitaryInfoStatementDescriptor = createStatementDescriptor (
    "updateEObjMilitaryInfo(com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo)",
    "update T417_MTY_CUS_AFL set CONT_ID =  ? , T416_MTY_LCT_CD =  ? , T395_MTY_AFL_STS_CD =  ? , SRV_BGN_DT =  ? , SRV_END_DT =  ? , START_DT =  ? , END_DT =  ? , LAST_VRF_BY_ID =  ? , LAST_UPDATE_DT =  ? , LAST_UPDATE_USER =  ? , LAST_UPDATE_TX_ID =  ?  where T417_MTY_CUS_AFL_ID =  ?  and LAST_UPDATE_DT =  ? ",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.UPDATE,
    null,
    new UpdateEObjMilitaryInfoParameterHandler (),
    new int[][]{{Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP}, {19, 19, 19, 0, 0, 0, 0, 20, 0, 0, 19, 19, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    3);

  /**
   * @generated
   */
  public static class UpdateEObjMilitaryInfoParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjMilitaryInfo bean0 = (EObjMilitaryInfo) parameters[0];
      setLong (stmt, 1, Types.BIGINT, (Long)bean0.getPartyId());
      setLong (stmt, 2, Types.BIGINT, (Long)bean0.getBRANCH_TP_CD());
      setLong (stmt, 3, Types.BIGINT, (Long)bean0.getAffiliation());
      setTimestamp (stmt, 4, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getServiceBeginDate());
      setTimestamp (stmt, 5, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getServiceEndDate());
      setTimestamp (stmt, 6, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getStartDate());
      setTimestamp (stmt, 7, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEndDate());
      setString (stmt, 8, Types.VARCHAR, (String)bean0.getLastVerifiedBy());
      setTimestamp (stmt, 9, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 10, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 11, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
      setLong (stmt, 12, Types.BIGINT, (Long)bean0.getMilitaryInfoPkId());
      setTimestamp (stmt, 13, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getOldLastUpdateDt());
    }
  }

  /**
   * @Update( sql="delete from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ?" )
   * 
   * @generated
   */
  public int deleteEObjMilitaryInfo (Long militaryInfoPkId)
  {
    return update (deleteEObjMilitaryInfoStatementDescriptor, militaryInfoPkId);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor deleteEObjMilitaryInfoStatementDescriptor = createStatementDescriptor (
    "deleteEObjMilitaryInfo(Long)",
    "delete from T417_MTY_CUS_AFL where T417_MTY_CUS_AFL_ID = ?",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.DELETE,
    null,
    new DeleteEObjMilitaryInfoParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    4);

  /**
   * @generated
   */
  public static class DeleteEObjMilitaryInfoParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

}
