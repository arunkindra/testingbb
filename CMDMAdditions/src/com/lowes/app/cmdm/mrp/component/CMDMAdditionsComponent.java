
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[b79509ce06660bafe5e386b6befde12d]
 */

package com.lowes.app.cmdm.mrp.component;

import com.dwl.base.DWLControl;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.base.DWLResponse;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.ServiceLocatorException;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.util.DWLFunctionUtils;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Component;
import com.ibm.mdm.annotations.TxMetadata;


import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.exception.DWLUpdateException;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.base.util.PaginationUtils;
import com.dwl.bobj.query.BObjQuery;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.bobj.query.Persistence;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.utilities.FunctionUtils;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMExceptionUtils;
import com.ibm.mdm.common.brokers.BObjPersistenceFactoryBroker;
import com.ibm.mdm.common.brokers.BObjQueryFactoryBroker;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;
import com.lowes.app.cmdm.mrp.bobj.query.CMDMAdditionsModuleBObjPersistenceFactory;
import com.lowes.app.cmdm.mrp.bobj.query.CMDMAdditionsModuleBObjQueryFactory;
import com.lowes.app.cmdm.mrp.bobj.query.MilitaryInfoBObjQuery;
import com.lowes.app.cmdm.mrp.component.MilitaryInfoBObj;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;
import com.lowes.app.cmdm.mrp.constant.ResourceBundleNames;
import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditions;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Business component class for handling CMDMAdditions related transactions and
 * inquiries.
 * @generated
 */
 @Component(errorComponentID = CMDMAdditionsComponentID.CMDMADDITIONS_COMPONENT)
public class CMDMAdditionsComponent extends TCRMCommonComponent implements CMDMAdditions {
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	private final static String EXCEPTION_DUPLICATE_KEY = "Exception_Shared_DuplicateKey";

	/**
    * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
    * @generated 
    */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CMDMAdditionsComponent.class);
			
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private IDWLErrorMessage errHandler;
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @generated
     */
    public CMDMAdditionsComponent() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjQuery factory class from the configuration.
     * @generated
     **/
    private CMDMAdditionsModuleBObjQueryFactory getBObjQueryFactory() throws BObjQueryException{
        return (CMDMAdditionsModuleBObjQueryFactory) BObjQueryFactoryBroker.getBObjQueryFactory(CMDMAdditionsModuleBObjQueryFactory.class.getName());
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjPersistenceFactory class from the configuration.
     * @generated
     **/
    private CMDMAdditionsModuleBObjPersistenceFactory getBObjPersistenceFactory() throws BObjQueryException {
        return (CMDMAdditionsModuleBObjPersistenceFactory) BObjPersistenceFactoryBroker.getBObjPersistenceFactory(CMDMAdditionsModuleBObjPersistenceFactory.class.getName());
    }
    



	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getMilitaryInfo.
     *
     * @param MilitaryInfoPkId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetMilitaryInfo
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.GETMILITARYINFO_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetMilitaryInfo"
    )
     public DWLResponse getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) throws DWLBaseException {
    logger.finest("ENTER getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(MilitaryInfoPkId);
        DWLTransaction txObj = new  DWLTransactionInquiry("getMilitaryInfo", params, control);
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction getMilitaryInfo.";
      logger.finest("getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + infoForLogging);
    }	
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction getMilitaryInfo.";
      logger.finest("getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + returnValue);
    }
        return retObj;
    }
    
    
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a MilitaryInfo from the database.
     * 
     * @generated
     */
    public DWLResponse handleGetMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
        
        BObjQuery bObjQuery = null;
           
        String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

        // History data inquiry: if inquireAsOfDate field has value in request xml 
        if (StringUtils.isNonBlank(asOfDate)) {
            Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CMDMAdditionsComponentID.CMDMADDITIONS_COMPONENT,
                                                     CMDMAdditionsErrorReasonCode.GETMILITARYINFO_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                     status, control);

            bObjQuery = getBObjQueryFactory().createMilitaryInfoBObjQuery(MilitaryInfoBObjQuery.MILITARY_INFO_HISTORY_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(MilitaryInfoPkId));
            bObjQuery.setParameter(1, tsAsOfDate);
            bObjQuery.setParameter(2, tsAsOfDate);
        } else {
            bObjQuery = getBObjQueryFactory().createMilitaryInfoBObjQuery(MilitaryInfoBObjQuery.MILITARY_INFO_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(MilitaryInfoPkId));
        }


        MilitaryInfoBObj o = (MilitaryInfoBObj) bObjQuery.getSingleResult();
        if( o == null ){
        	return null;
        } 
        postRetrieveMilitaryInfoBObj(o, "0", "ALL", control); 
          
        if (o.getStatus()==null) {
            o.setStatus(status);
        }
        response.addStatus(o.getStatus());
        response.setData(o);

      return response;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetMilitaryInfo(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        String pk = null;
        if (arguments!=null && arguments.length>0) {
            pk = (String)arguments[0];
        }
        // Check if the parameter passed in exists.
        if ((pk == null) || (pk.trim().length() == 0)) {
            TCRMExceptionUtils.throwTCRMException(null, new TCRMReadException(), transaction.getStatus(), DWLStatus.FATAL,
                CMDMAdditionsComponentID.MILITARY_INFO_BOBJ,
                TCRMErrorCode.FIELD_VALIDATION_ERROR,
                CMDMAdditionsErrorReasonCode.MILITARYINFO_MILITARYINFOPKID_NULL,
                transaction.getTxnControl(), errHandler);
        }
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllMilitaryInfoByPartyId.
     *
     * @param PartyId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllMilitaryInfoByPartyId
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.GETALLMILITARYINFOBYPARTYID_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetAllMilitaryInfoByPartyId"
    )
     public DWLResponse getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) throws DWLBaseException {
    logger.finest("ENTER getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(PartyId);
        DWLTransaction txObj = new  DWLTransactionInquiry("getAllMilitaryInfoByPartyId", params, control);
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction getAllMilitaryInfoByPartyId.";
      logger.finest("getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + infoForLogging);
    }	
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction getAllMilitaryInfoByPartyId.";
      logger.finest("getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + returnValue);
    }
        return retObj;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a MilitaryInfo from the database.
     * 
     * @generated
     */
    public DWLResponse handleGetAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
        
        BObjQuery bObjQuery = null;
           
        String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

        // History data inquiry: if inquireAsOfDate field has value in request xml 
        if (StringUtils.isNonBlank(asOfDate)) {
            Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CMDMAdditionsComponentID.CMDMADDITIONS_COMPONENT,
                                                     CMDMAdditionsErrorReasonCode.GETALLMILITARYINFOBYPARTYID_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                     status, control);

            bObjQuery = getBObjQueryFactory().createMilitaryInfoBObjQuery(MilitaryInfoBObjQuery.ALL_MILITARY_INFO_BY_PARTY_ID_HISTORY_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(PartyId));
            bObjQuery.setParameter(1, tsAsOfDate);
            bObjQuery.setParameter(2, tsAsOfDate);
        } else {
            bObjQuery = getBObjQueryFactory().createMilitaryInfoBObjQuery(MilitaryInfoBObjQuery.ALL_MILITARY_INFO_BY_PARTY_ID_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(PartyId));
        }

        boolean considerForPagination = PaginationUtils
                            .considerForPagintion(MilitaryInfoBObj.class.getName(), control);
        control.setConsiderForPagintionFlag(considerForPagination);

        // set returned object
        List<?> list = bObjQuery.getResults();

        if (list.size() == 0) {
            return null;
        }
    Vector<MilitaryInfoBObj> vector = new Vector<MilitaryInfoBObj>();
        for (Iterator<?> it = list.iterator(); it.hasNext();) {
            MilitaryInfoBObj o = (MilitaryInfoBObj) it.next();
            postRetrieveMilitaryInfoBObj(o, "0", "ALL", control); 
            vector.add(o);
          
            if (o.getStatus()==null) {
                o.setStatus(status);
            }
            response.addStatus(o.getStatus());
        }
        response.setData(vector);

      return response;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetAllMilitaryInfoByPartyId(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
    if (arguments!=null && arguments.length>0) {
            // MDM_TODO0: CDKWB0003I Add argument validation logic
    }
    }

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction addMilitaryInfo.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleAddMilitaryInfo
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.ADDMILITARYINFO_FAILED
    )
     public DWLResponse addMilitaryInfo(MilitaryInfoBObj theBObj) throws DWLBaseException {
    logger.finest("ENTER addMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addMilitaryInfo", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction addMilitaryInfo.";
      logger.finest("addMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
    }	
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction addMilitaryInfo.";
      logger.finest("addMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN addMilitaryInfo(MilitaryInfoBObj theBObj) " + returnValue);
    }
        return retObj;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a MilitaryInfo to the database.
     *
     * @param theMilitaryInfoBObj
     *     The object that contains MilitaryInfo attribute values.
     * @return
     *     DWLResponse containing a MilitaryInfoBObj object.
     * @exception Exception
     * @generated
     */
    public DWLResponse handleAddMilitaryInfo(MilitaryInfoBObj theMilitaryInfoBObj) throws Exception {
        DWLResponse response = null;
        DWLStatus status = theMilitaryInfoBObj.getStatus();
        if (status == null) {
            status = new DWLStatus();
            theMilitaryInfoBObj.setStatus(status);
        }

        String strPluggableID = null;

        try {
            // Pluggable Key Structure implementation
            strPluggableID = getSuppliedIdPKFromBObj(theMilitaryInfoBObj);

            if ((strPluggableID != null) && (strPluggableID.length() > 0)) {
                theMilitaryInfoBObj.getEObjMilitaryInfo().setMilitaryInfoPkId(FunctionUtils.getLongFromString(strPluggableID));
            } else {
                strPluggableID = null;
                theMilitaryInfoBObj.getEObjMilitaryInfo().setMilitaryInfoPkId(null);
            }
         Persistence theMilitaryInfoBObjPersistence = getBObjPersistenceFactory().createMilitaryInfoBObjPersistence(MilitaryInfoBObjQuery.MILITARY_INFO_ADD, theMilitaryInfoBObj);
         theMilitaryInfoBObjPersistence.persistAdd();

            response = new DWLResponse();
            response.setData(theMilitaryInfoBObj);
            response.setStatus(theMilitaryInfoBObj.getStatus());
    } catch (Exception ex) {
 				TCRMExceptionUtils.throwTCRMException(ex, new TCRMInsertException(ex.getMessage()), status,
                    DWLStatus.FATAL, CMDMAdditionsComponentID.CMDMADDITIONS_COMPONENT, TCRMErrorCode.INSERT_RECORD_ERROR,
                    CMDMAdditionsErrorReasonCode.ADDMILITARYINFO_FAILED, theMilitaryInfoBObj.getControl(), errHandler);
        }
        
        return response;
    }
    

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction updateMilitaryInfo.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleUpdateMilitaryInfo
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.UPDATEMILITARYINFO_FAILED
    )
     public DWLResponse updateMilitaryInfo(MilitaryInfoBObj theBObj) throws DWLBaseException {
    logger.finest("ENTER updateMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateMilitaryInfo", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction updateMilitaryInfo.";
      logger.finest("updateMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
    }	
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction updateMilitaryInfo.";
      logger.finest("updateMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN updateMilitaryInfo(MilitaryInfoBObj theBObj) " + returnValue);
    }
        return retObj;
    }
    
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates the specified MilitaryInfo with new attribute values.
     *
     * @param theMilitaryInfoBObj
     *     The object that contains MilitaryInfo attribute values to be updated
     * @return
     *     DWLResponse containing a MilitaryInfoBObj of the updated object.
     * @exception Exception
     * @generated
     */
    public DWLResponse handleUpdateMilitaryInfo(MilitaryInfoBObj theMilitaryInfoBObj) throws Exception {

        DWLStatus status = theMilitaryInfoBObj.getStatus();

        if (status == null) {
            status = new DWLStatus();
            theMilitaryInfoBObj.setStatus(status);
        }
        
            // set lastupdatetxid with txnid from dwlcontrol
            theMilitaryInfoBObj.getEObjMilitaryInfo().setLastUpdateTxId(new Long(theMilitaryInfoBObj.getControl().getTxnId()));
         Persistence theMilitaryInfoBObjPersistence = getBObjPersistenceFactory().createMilitaryInfoBObjPersistence(MilitaryInfoBObjQuery.MILITARY_INFO_UPDATE, theMilitaryInfoBObj);
         theMilitaryInfoBObjPersistence.persistUpdate();

        DWLResponse response = createDWLResponse();
        response.setData(theMilitaryInfoBObj);
        response.setStatus(theMilitaryInfoBObj.getStatus());

        return response;
    }



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     **/
    public void loadBeforeImage(MilitaryInfoBObj bObj) throws DWLBaseException {
    logger.finest("ENTER loadBeforeImage(MilitaryInfoBObj bObj)");
    	if( bObj.BeforeImage() == null ){
    	
    		MilitaryInfoBObj beforeImage = null;
    		DWLResponse response = null;
    		
    		try {
    			response = getMilitaryInfo( bObj.getMilitaryInfoPkId(), bObj.getControl());
    			beforeImage = (MilitaryInfoBObj)response.getData();
    			
    		} catch( Exception e){
        if (logger.isFinestEnabled()) {
    				String infoForLogging="Error: Exception " + e.getMessage() + " while updating a record ";
      logger.finest("loadBeforeImage(MilitaryInfoBObj bObj) " + infoForLogging);
        }
              DWLExceptionUtils.throwDWLBaseException(e, 
            									new DWLUpdateException(e.getMessage()), 
            									bObj.getStatus(), DWLStatus.FATAL,
                                  CMDMAdditionsComponentID.MILITARY_INFO_BOBJ, 
                                  "DIERR",
                                  CMDMAdditionsErrorReasonCode.MILITARYINFO_BEFORE_IMAGE_NOT_POPULATED, 
                                  bObj.getControl(), errHandler);
    		}
    		
    		if( beforeImage == null ){
        if (logger.isFinestEnabled()) {
    		    	String infoForLogging="Error: Before image for updating a record is null ";
      logger.finest("loadBeforeImage(MilitaryInfoBObj bObj) " + infoForLogging);
        }
              DWLExceptionUtils.throwDWLBaseException( new DWLUpdateException(), 
            									bObj.getStatus(), DWLStatus.FATAL,
                                  CMDMAdditionsComponentID.MILITARY_INFO_BOBJ, 
                                  "DIERR",
                                  CMDMAdditionsErrorReasonCode.MILITARYINFO_BEFORE_IMAGE_NOT_POPULATED, 
                                  bObj.getControl(), errHandler);
    		}
    		
    		bObj.setBeforeImage(beforeImage);
    		
    	}
    logger.finest("RETURN loadBeforeImage(MilitaryInfoBObj bObj)");
    }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Populates the parent MilitaryInfoBObj with all contained BObjs and Type
     * Codes.
     * @generated
    **/
    public void postRetrieveMilitaryInfoBObj(MilitaryInfoBObj theBObj, String inquiryLevel, String filter, DWLControl control) throws Exception {
    logger.finest("ENTER postRetrieveMilitaryInfoBObj(MilitaryInfoBObj theBObj, String inquiryLevel, String filter, DWLControl control)");
        // processing code types  
        String langId = (String) theBObj.getControl().get(TCRMControlKeys.LANG_ID);
        CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
  
        String BranchTp = theBObj.getBranchType();
        if( BranchTp != null && !BranchTp.equals("")){
            CodeTypeBObj BranchBObj = codeTypeCompHelper
                    .getCodeTypeByCode("t416_mty_lct_typ", langId, BranchTp,
                          theBObj.getControl());

            if (BranchBObj != null) {
                theBObj.setBranchValue(BranchBObj.getvalue());
            }

    }
        String AffiliationTp = theBObj.getAffiliationType();
        if( AffiliationTp != null && !AffiliationTp.equals("")){
            CodeTypeBObj AffiliationBObj = codeTypeCompHelper
                    .getCodeTypeByCode("t395_mty_afl_sts_typ", langId, AffiliationTp,
                          theBObj.getControl());

            if (AffiliationBObj != null) {
                theBObj.setAffiliationValue(AffiliationBObj.getvalue());
            }

    }
    logger.finest("RETURN postRetrieveMilitaryInfoBObj(MilitaryInfoBObj theBObj, String inquiryLevel, String filter, DWLControl control)");
  }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Builds duplicated key throwable message. There are only two elements in
     * the vector, one is the primary key, and the other is the class name.
     * @generated
     **/
    @SuppressWarnings("unused")
    private String buildDupThrowableMessage(String[] errParams) {
    return ResourceBundleHelper.resolve(
        ResourceBundleNames.COMMON_SERVICES_STRINGS,
        EXCEPTION_DUPLICATE_KEY, errParams);

    }
    /**
     * ITLS-11-11-2016-SB Custom delete Record
     * @param partyId
     * @throws Exception
     */
    public void deleteMilitaryInfo(String partyId) throws Exception {
    	QueryConnection conn = null;
    	try {
    		conn = DataManager.getInstance().getQueryConnection();
    		String deleteSQL = "DELETE FROM T417_MTY_CUS_AFL WHERE CONT_ID = ?";

    		Object[] parameters = new Object[2];
    		parameters[0] = partyId;

    		conn.update(deleteSQL, parameters);
    		
    	} catch (ServiceLocatorException e) {
    		throw e;
    	} finally {
    			if (conn != null) {
    				conn.close();
    			}
    		}
    }

}


