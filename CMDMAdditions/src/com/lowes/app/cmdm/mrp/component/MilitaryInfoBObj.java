
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[0e3cbba193585fe78d3adcd0d36b4657]
 */

package com.lowes.app.cmdm.mrp.component;

import com.dwl.tcrm.common.TCRMCommon;



import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.base.DWLControl;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.constant.DWLUtilErrorReasonCode;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.DWLUpdateException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.base.util.DWLFunctionUtils;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.common.ITCRMValidation;
import com.dwl.tcrm.utilities.DateFormatter;
import com.dwl.tcrm.utilities.DateValidator;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsPropertyKeys;
import com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfo;
import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditions;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides the implementation of the business object
 * <code>MilitaryInfoBObj</code>.
 * 
 * @see com.dwl.tcrm.common.TCRMCommon
 * @generated
 */
 

@SuppressWarnings("serial")
public class MilitaryInfoBObj extends TCRMCommon  {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected EObjMilitaryInfo eObjMilitaryInfo;
	/**
    * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
    * @generated 
    */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(MilitaryInfoBObj.class);
		
 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected String branchValue;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected String affiliationValue;
	protected boolean isValidServiceBeginDate = true;
	
	protected boolean isValidServiceEndDate = true;
	
	protected boolean isValidStartDate = true;
	
	protected boolean isValidEndDate = true;
	


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */     
    public MilitaryInfoBObj() {
        super();
        init();
        eObjMilitaryInfo = new EObjMilitaryInfo();
        setComponentID(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ);
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Initializes the fields required to populate the metaDataMap. Each key is
     * an element-level field of the business object.
     *
     * @generated
     */
    private void init() {
        metaDataMap.put("MilitaryInfoPkId", null);
        metaDataMap.put("PartyId", null);
        metaDataMap.put("BranchType", null);
        metaDataMap.put("BranchValue", null);
        metaDataMap.put("AffiliationType", null);
        metaDataMap.put("AffiliationValue", null);
        metaDataMap.put("ServiceBeginDate", null);
        metaDataMap.put("ServiceEndDate", null);
        metaDataMap.put("StartDate", null);
        metaDataMap.put("EndDate", null);
        metaDataMap.put("LastVerifiedBy", null);
        metaDataMap.put("MilitaryInfoHistActionCode", null);
        metaDataMap.put("MilitaryInfoHistCreateDate", null);
        metaDataMap.put("MilitaryInfoHistCreatedBy", null);
        metaDataMap.put("MilitaryInfoHistEndDate", null);
        metaDataMap.put("MilitaryInfoHistoryIdPK", null);
        metaDataMap.put("MilitaryInfoLastUpdateDate", null);
        metaDataMap.put("MilitaryInfoLastUpdateTxId", null);
        metaDataMap.put("MilitaryInfoLastUpdateUser", null);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Refreshes all the attributes this business object supports.
     *
     * @see com.dwl.base.DWLCommon#refreshMap()
     * @generated
     */
    public void refreshMap() {

        if (bRequireMapRefresh) {
            super.refreshMap();
            metaDataMap.put("MilitaryInfoPkId", getMilitaryInfoPkId());
            metaDataMap.put("PartyId", getPartyId());
            metaDataMap.put("BranchType", getBranchType());
            metaDataMap.put("BranchValue", getBranchValue());
            metaDataMap.put("AffiliationType", getAffiliationType());
            metaDataMap.put("AffiliationValue", getAffiliationValue());
            metaDataMap.put("ServiceBeginDate", getServiceBeginDate());
            metaDataMap.put("ServiceEndDate", getServiceEndDate());
            metaDataMap.put("StartDate", getStartDate());
            metaDataMap.put("EndDate", getEndDate());
            metaDataMap.put("LastVerifiedBy", getLastVerifiedBy());
            metaDataMap.put("MilitaryInfoHistActionCode", getMilitaryInfoHistActionCode());
            metaDataMap.put("MilitaryInfoHistCreateDate", getMilitaryInfoHistCreateDate());
            metaDataMap.put("MilitaryInfoHistCreatedBy", getMilitaryInfoHistCreatedBy());
            metaDataMap.put("MilitaryInfoHistEndDate", getMilitaryInfoHistEndDate());
            metaDataMap.put("MilitaryInfoHistoryIdPK", getMilitaryInfoHistoryIdPK());
            metaDataMap.put("MilitaryInfoLastUpdateDate", getMilitaryInfoLastUpdateDate());
            metaDataMap.put("MilitaryInfoLastUpdateTxId", getMilitaryInfoLastUpdateTxId());
            metaDataMap.put("MilitaryInfoLastUpdateUser", getMilitaryInfoLastUpdateUser());
            bRequireMapRefresh = false;
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the control object on this business object.
     *
     * @see com.dwl.base.DWLCommon#setControl(DWLControl)
     * @generated
     */
    public void setControl(DWLControl newDWLControl) {
        super.setControl(newDWLControl);

        if (eObjMilitaryInfo != null) {
            eObjMilitaryInfo.setControl(newDWLControl);
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the entity object associated with this business object.
     *
     * @generated
     */
    public EObjMilitaryInfo getEObjMilitaryInfo() {
        bRequireMapRefresh = true;
        return eObjMilitaryInfo;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the entity object associated with this business object.
     *
     * @param eObjMilitaryInfo
     *            The eObjMilitaryInfo to set.
     * @generated
     */
    public void setEObjMilitaryInfo(EObjMilitaryInfo eObjMilitaryInfo) {
        bRequireMapRefresh = true;
        this.eObjMilitaryInfo = eObjMilitaryInfo;
        if (this.eObjMilitaryInfo != null && this.eObjMilitaryInfo.getControl() == null) {
            DWLControl control = this.getControl();
            if (control != null) {
                this.eObjMilitaryInfo.setControl(control);
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the militaryInfoPkId attribute.
     * 
     * @generated
     */
    public String getMilitaryInfoPkId (){
   
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getMilitaryInfoPkId());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the militaryInfoPkId attribute.
     * 
     * @param newMilitaryInfoPkId
     *     The new value of militaryInfoPkId.
     * @generated
     */
    public void setMilitaryInfoPkId( String newMilitaryInfoPkId ) throws Exception {
        metaDataMap.put("MilitaryInfoPkId", newMilitaryInfoPkId);

        if (newMilitaryInfoPkId == null || newMilitaryInfoPkId.equals("")) {
            newMilitaryInfoPkId = null;


        }
        eObjMilitaryInfo.setMilitaryInfoPkId( DWLFunctionUtils.getLongFromString(newMilitaryInfoPkId) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the partyId attribute.
     * 
     * @generated
     */
    public String getPartyId (){
   
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getPartyId());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the partyId attribute.
     * 
     * @param newPartyId
     *     The new value of partyId.
     * @generated
     */
    public void setPartyId( String newPartyId ) throws Exception {
        metaDataMap.put("PartyId", newPartyId);

        if (newPartyId == null || newPartyId.equals("")) {
            newPartyId = null;


        }
        eObjMilitaryInfo.setPartyId( DWLFunctionUtils.getLongFromString(newPartyId) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the branchType attribute.
     * 
     * @generated
     */
    public String getBranchType (){
   
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getBRANCH_TP_CD());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the branchType attribute.
     * 
     * @param newBranchType
     *     The new value of branchType.
     * @generated
     */
    public void setBranchType( String newBranchType ) throws Exception {
        metaDataMap.put("BranchType", newBranchType);

        if (newBranchType == null || newBranchType.equals("")) {
            newBranchType = null;


        }
        eObjMilitaryInfo.setBRANCH_TP_CD( DWLFunctionUtils.getLongFromString(newBranchType) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the branchValue attribute.
     * 
     * @generated
     */
    public String getBranchValue (){
      return branchValue;
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the branchValue attribute.
     * 
     * @param newBranchValue
     *     The new value of branchValue.
     * @generated
     */
    public void setBranchValue( String newBranchValue ) throws Exception {
        metaDataMap.put("BranchValue", newBranchValue);

        if (newBranchValue == null || newBranchValue.equals("")) {
            newBranchValue = null;


        }
        branchValue = newBranchValue;
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the affiliationType attribute.
     * 
     * @generated
     */
    public String getAffiliationType (){
   
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getAffiliation());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the affiliationType attribute.
     * 
     * @param newAffiliationType
     *     The new value of affiliationType.
     * @generated
     */
    public void setAffiliationType( String newAffiliationType ) throws Exception {
        metaDataMap.put("AffiliationType", newAffiliationType);

        if (newAffiliationType == null || newAffiliationType.equals("")) {
            newAffiliationType = null;


        }
        eObjMilitaryInfo.setAffiliation( DWLFunctionUtils.getLongFromString(newAffiliationType) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the affiliationValue attribute.
     * 
     * @generated
     */
    public String getAffiliationValue (){
      return affiliationValue;
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the affiliationValue attribute.
     * 
     * @param newAffiliationValue
     *     The new value of affiliationValue.
     * @generated
     */
    public void setAffiliationValue( String newAffiliationValue ) throws Exception {
        metaDataMap.put("AffiliationValue", newAffiliationValue);

        if (newAffiliationValue == null || newAffiliationValue.equals("")) {
            newAffiliationValue = null;


        }
        affiliationValue = newAffiliationValue;
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the serviceBeginDate attribute.
     * 
     * @generated
     */
    public String getServiceBeginDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getServiceBeginDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the serviceBeginDate attribute.
     * 
     * @param newServiceBeginDate
     *     The new value of serviceBeginDate.
     * @generated
     */
    public void setServiceBeginDate( String newServiceBeginDate ) throws Exception {
        metaDataMap.put("ServiceBeginDate", newServiceBeginDate);
       	isValidServiceBeginDate = true;

        if (newServiceBeginDate == null || newServiceBeginDate.equals("")) {
            newServiceBeginDate = null;
            eObjMilitaryInfo.setServiceBeginDate(null);


        }
    else {
        	if (DateValidator.validates(newServiceBeginDate)) {
           		eObjMilitaryInfo.setServiceBeginDate(DateFormatter.getStartDateTimestamp(newServiceBeginDate));
            	metaDataMap.put("ServiceBeginDate", getServiceBeginDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
              "/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("ServiceBeginDate") != null) {
                    	metaDataMap.put("ServiceBeginDate", "");
                	}
                	isValidServiceBeginDate = false;
                	eObjMilitaryInfo.setServiceBeginDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the serviceEndDate attribute.
     * 
     * @generated
     */
    public String getServiceEndDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getServiceEndDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the serviceEndDate attribute.
     * 
     * @param newServiceEndDate
     *     The new value of serviceEndDate.
     * @generated
     */
    public void setServiceEndDate( String newServiceEndDate ) throws Exception {
        metaDataMap.put("ServiceEndDate", newServiceEndDate);
       	isValidServiceEndDate = true;

        if (newServiceEndDate == null || newServiceEndDate.equals("")) {
            newServiceEndDate = null;
            eObjMilitaryInfo.setServiceEndDate(null);


        }
    else {
        	if (DateValidator.validates(newServiceEndDate)) {
           		eObjMilitaryInfo.setServiceEndDate(DateFormatter.getStartDateTimestamp(newServiceEndDate));
            	metaDataMap.put("ServiceEndDate", getServiceEndDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
              "/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("ServiceEndDate") != null) {
                    	metaDataMap.put("ServiceEndDate", "");
                	}
                	isValidServiceEndDate = false;
                	eObjMilitaryInfo.setServiceEndDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the startDate attribute.
     * 
     * @generated
     */
    public String getStartDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getStartDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the startDate attribute.
     * 
     * @param newStartDate
     *     The new value of startDate.
     * @generated
     */
    public void setStartDate( String newStartDate ) throws Exception {
        metaDataMap.put("StartDate", newStartDate);
       	isValidStartDate = true;

        if (newStartDate == null || newStartDate.equals("")) {
            newStartDate = null;
            eObjMilitaryInfo.setStartDate(null);


        }
    else {
        	if (DateValidator.validates(newStartDate)) {
           		eObjMilitaryInfo.setStartDate(DateFormatter.getStartDateTimestamp(newStartDate));
            	metaDataMap.put("StartDate", getStartDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
              "/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("StartDate") != null) {
                    	metaDataMap.put("StartDate", "");
                	}
                	isValidStartDate = false;
                	eObjMilitaryInfo.setStartDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the endDate attribute.
     * 
     * @generated
     */
    public String getEndDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getEndDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the endDate attribute.
     * 
     * @param newEndDate
     *     The new value of endDate.
     * @generated
     */
    public void setEndDate( String newEndDate ) throws Exception {
        metaDataMap.put("EndDate", newEndDate);
       	isValidEndDate = true;

        if (newEndDate == null || newEndDate.equals("")) {
            newEndDate = null;
            eObjMilitaryInfo.setEndDate(null);


        }
    else {
        	if (DateValidator.validates(newEndDate)) {
           		eObjMilitaryInfo.setEndDate(DateFormatter.getStartDateTimestamp(newEndDate));
            	metaDataMap.put("EndDate", getEndDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
              "/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("EndDate") != null) {
                    	metaDataMap.put("EndDate", "");
                	}
                	isValidEndDate = false;
                	eObjMilitaryInfo.setEndDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the lastVerifiedBy attribute.
     * 
     * @generated
     */
    public String getLastVerifiedBy (){
   
        return eObjMilitaryInfo.getLastVerifiedBy();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the lastVerifiedBy attribute.
     * 
     * @param newLastVerifiedBy
     *     The new value of lastVerifiedBy.
     * @generated
     */
    public void setLastVerifiedBy( String newLastVerifiedBy ) throws Exception {
        metaDataMap.put("LastVerifiedBy", newLastVerifiedBy);

        if (newLastVerifiedBy == null || newLastVerifiedBy.equals("")) {
            newLastVerifiedBy = null;


        }
        eObjMilitaryInfo.setLastVerifiedBy( newLastVerifiedBy );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateTxId attribute.
     *
     * @generated
     */
    public String getMilitaryInfoLastUpdateTxId() {
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getLastUpdateTxId());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateUser attribute.
     *
     * @generated
     */
    public String getMilitaryInfoLastUpdateUser() {
        return eObjMilitaryInfo.getLastUpdateUser();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateDt attribute.
     *
     * @generated
     */
    public String getMilitaryInfoLastUpdateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getLastUpdateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateTxId attribute.
     *
     * @param newLastUpdateTxId
     *     The new value of LastUpdateTxId.
     * @generated
     */
    public void setMilitaryInfoLastUpdateTxId(String newLastUpdateTxId) {
        metaDataMap.put("MilitaryInfoLastUpdateTxId", newLastUpdateTxId);

        if ((newLastUpdateTxId == null) || newLastUpdateTxId.equals("")) {
            newLastUpdateTxId = null;
        }
        eObjMilitaryInfo.setLastUpdateTxId(DWLFunctionUtils.getLongFromString(newLastUpdateTxId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateUser attribute.
     *
     * @param newLastUpdateUser
     *     The new value of LastUpdateUser.
     * @generated
     */
    public void setMilitaryInfoLastUpdateUser(String newLastUpdateUser) {
        metaDataMap.put("MilitaryInfoLastUpdateUser", newLastUpdateUser);

        if ((newLastUpdateUser == null) || newLastUpdateUser.equals("")) {
            newLastUpdateUser = null;
        }
        eObjMilitaryInfo.setLastUpdateUser(newLastUpdateUser);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateDt attribute.
     *
     * @param newLastUpdateDt
     *     The new value of LastUpdateDt.
     * @throws Exception
     * @generated
     */
    public void setMilitaryInfoLastUpdateDate(String newLastUpdateDt) throws Exception {
        metaDataMap.put("MilitaryInfoLastUpdateDate", newLastUpdateDt);

        if ((newLastUpdateDt == null) || newLastUpdateDt.equals("")) {
            newLastUpdateDt = null;
        }

        eObjMilitaryInfo.setLastUpdateDt(DWLFunctionUtils.getTimestampFromTimestampString(newLastUpdateDt));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the MilitaryInfoHistActionCode history attribute.
     *
     * @generated
     */
    public String getMilitaryInfoHistActionCode() {
        return eObjMilitaryInfo.getHistActionCode();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the MilitaryInfoHistActionCode history attribute.
     *
     * @param aMilitaryInfoHistActionCode
     *     The new value of MilitaryInfoHistActionCode.
     * @generated
     */
    public void setMilitaryInfoHistActionCode(String aMilitaryInfoHistActionCode) {
        metaDataMap.put("MilitaryInfoHistActionCode", aMilitaryInfoHistActionCode);

        if ((aMilitaryInfoHistActionCode == null) || aMilitaryInfoHistActionCode.equals("")) {
            aMilitaryInfoHistActionCode = null;
        }
        eObjMilitaryInfo.setHistActionCode(aMilitaryInfoHistActionCode);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the MilitaryInfoHistCreateDate history attribute.
     *
     * @generated
     */
    public String getMilitaryInfoHistCreateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getHistCreateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the MilitaryInfoHistCreateDate history attribute.
     *
     * @param aMilitaryInfoHistCreateDate
     *     The new value of MilitaryInfoHistCreateDate.
     * @generated
     */
    public void setMilitaryInfoHistCreateDate(String aMilitaryInfoHistCreateDate) throws Exception{
        metaDataMap.put("MilitaryInfoHistCreateDate", aMilitaryInfoHistCreateDate);

        if ((aMilitaryInfoHistCreateDate == null) || aMilitaryInfoHistCreateDate.equals("")) {
            aMilitaryInfoHistCreateDate = null;
        }

        eObjMilitaryInfo.setHistCreateDt(DWLFunctionUtils.getTimestampFromTimestampString(aMilitaryInfoHistCreateDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the MilitaryInfoHistCreatedBy history attribute.
     *
     * @generated
     */
    public String getMilitaryInfoHistCreatedBy() {
        return eObjMilitaryInfo.getHistCreatedBy();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the MilitaryInfoHistCreatedBy history attribute.
     *
     * @param aMilitaryInfoHistCreatedBy
     *     The new value of MilitaryInfoHistCreatedBy.
     * @generated
     */
    public void setMilitaryInfoHistCreatedBy(String aMilitaryInfoHistCreatedBy) {
        metaDataMap.put("MilitaryInfoHistCreatedBy", aMilitaryInfoHistCreatedBy);

        if ((aMilitaryInfoHistCreatedBy == null) || aMilitaryInfoHistCreatedBy.equals("")) {
            aMilitaryInfoHistCreatedBy = null;
        }

        eObjMilitaryInfo.setHistCreatedBy(aMilitaryInfoHistCreatedBy);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the MilitaryInfoHistEndDate history attribute.
     *
     * @generated
     */
    public String getMilitaryInfoHistEndDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjMilitaryInfo.getHistEndDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the MilitaryInfoHistEndDate history attribute.
     *
     * @param aMilitaryInfoHistEndDate
     *     The new value of MilitaryInfoHistEndDate.
     * @generated
     */
    public void setMilitaryInfoHistEndDate(String aMilitaryInfoHistEndDate) throws Exception{
        metaDataMap.put("MilitaryInfoHistEndDate", aMilitaryInfoHistEndDate);

        if ((aMilitaryInfoHistEndDate == null) || aMilitaryInfoHistEndDate.equals("")) {
            aMilitaryInfoHistEndDate = null;
        }
        eObjMilitaryInfo.setHistEndDt(DWLFunctionUtils.getTimestampFromTimestampString(aMilitaryInfoHistEndDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the MilitaryInfoHistoryIdPK history attribute.
     *
     * @generated
     */
    public String getMilitaryInfoHistoryIdPK() {
        return DWLFunctionUtils.getStringFromLong(eObjMilitaryInfo.getHistoryIdPK());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the MilitaryInfoHistoryIdPK history attribute.
     *
     * @param aMilitaryInfoHistoryIdPK
     *     The new value of MilitaryInfoHistoryIdPK.
     * @generated
     */
    public void setMilitaryInfoHistoryIdPK(String aMilitaryInfoHistoryIdPK) {
        metaDataMap.put("MilitaryInfoHistoryIdPK", aMilitaryInfoHistoryIdPK);

        if ((aMilitaryInfoHistoryIdPK == null) || aMilitaryInfoHistoryIdPK.equals("")) {
            aMilitaryInfoHistoryIdPK = null;
        }
        eObjMilitaryInfo.setHistoryIdPK(DWLFunctionUtils.getLongFromString(aMilitaryInfoHistoryIdPK));
    }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an add transaction.
     *
     * @generated
     */
    public DWLStatus validateAdd(int level, DWLStatus status) throws Exception {

        status = super.validateAdd(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO0: CDKWB0038I Add any controller-level custom validation logic to be
            // executed for this object during an "add" transaction

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO0: CDKWB0039I Add any component-level custom validation logic to be
            // executed for this object during an "add" transaction
        }
        status = getValidationStatus(level, status);
        return status;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an update transaction.
     *
     * @generated
     */
    public DWLStatus validateUpdate(int level, DWLStatus status) throws Exception {
    logger.finest("ENTER validateUpdate(int level, DWLStatus status)");

        status = super.validateUpdate(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO0: CDKWB0040I Add any controller-level custom validation logic to be
            // executed for this object during an "update" transaction

            if (eObjMilitaryInfo.getMilitaryInfoPkId() == null) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.MILITARYINFO_MILITARYINFOPKID_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
        if (logger.isFinestEnabled()) {
                	String infoForLogging="Error: Validation error occured for update. No primary key for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
      logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
                }
                status.addError(err);
            }
            if (eObjMilitaryInfo.getLastUpdateDt() == null) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(DWLUtilErrorReasonCode.LAST_UPDATED_DATE_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                	String infoForLogging="Error: Validation error occured for update. No last update date for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
      logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
                }
                status.addError(err);
            }
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            assignBeforeImageValues(metaDataMap);
            
            // MDM_TODO0: CDKWB0041I Add any component-level custom validation logic to be
            // executed for this object during an "update" transaction
        }
        status = getValidationStatus(level, status);
    if (logger.isFinestEnabled()) {
        	String returnValue = status.toString();
      logger.finest("RETURN validateUpdate(int level, DWLStatus status) " + returnValue);
    }
        return status;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Populate the before image of this business object.
     *
     * @see com.dwl.base.DWLCommon#populateBeforeImage()
     * @generated
     */
    public void populateBeforeImage() throws DWLBaseException {
    logger.finest("ENTER populateBeforeImage()");

        CMDMAdditions comp = null;
        try {
        
      comp = (CMDMAdditions)TCRMClassFactory.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
        	
        } catch (Exception e) {
      if (logger.isFinestEnabled()) {
        String infoForLogging="Error: Fatal error while updating record " + e.getMessage();
      logger.finest("populateBeforeImage() " + infoForLogging);
      }
            DWLExceptionUtils.throwDWLBaseException(e, 
            									new DWLUpdateException(e.getMessage()), 
            									this.getStatus(), DWLStatus.FATAL,
                                  CMDMAdditionsComponentID.MILITARY_INFO_BOBJ, 
                                  TCRMErrorCode.UPDATE_RECORD_ERROR,
                                  CMDMAdditionsErrorReasonCode.MILITARYINFO_BEFORE_IMAGE_NOT_POPULATED, 
                                  this.getControl());
        }
        
        comp.loadBeforeImage(this);
    logger.finest("RETURN populateBeforeImage()");
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation common to both add and update transactions.
     *
     * @generated NOT
     */
     
    private DWLStatus getValidationStatus(int level, DWLStatus status) throws Exception {
    
    	/*	logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");

    	if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO0: CDKWB0034I Add any common controller-level custom validation
            // logic to be executed for this object during either "add" or
            // "update" transactions
    		controllerValidation_PartyId(status);
    		controllerValidation_Branch(status);
    		controllerValidation_Affiliation(status);
    		controllerValidation_ServiceBeginDate(status);
    		controllerValidation_ServiceEndDate(status);
    		controllerValidation_StartDate(status);
    		controllerValidation_EndDate(status);
    	}

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO0: CDKWB0035I Add any common component-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions
    		componentValidation_PartyId(status);
    		componentValidation_Branch(status);
    		componentValidation_Affiliation(status);
    		componentValidation_ServiceBeginDate(status);
    		componentValidation_ServiceEndDate(status);
    		componentValidation_StartDate(status);
    		componentValidation_EndDate(status);
        }
        
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
      logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);
        }
    
        return status; */

        logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");


        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO: Add any common controller-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

            boolean isMilitaryInfoPkIdNull = (eObjMilitaryInfo.getMilitaryInfoPkId() == null);
            boolean isPartyIdNull = (eObjMilitaryInfo.getPartyId() == null);
            //persistent code type
            boolean isBranchNull = false;
            if ((eObjMilitaryInfo.getBRANCH_TP_CD() == null)
                 && ((getBranchValue() == null) || getBranchValue()
                     .trim().equals(""))) {
                isBranchNull = true;
            }
            if( !isBranchNull ){
                if( checkForInvalidMilitaryinfoBranch()){
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_BRANCH).longValue());
                    err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                    if (logger.isFinestEnabled()) {
                        String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                    }
                    status.addError(err);
                 }
            }
            //persistent code type
            boolean isAffiliationNull = false;
            if ((eObjMilitaryInfo.getAffiliation() == null)
                 && ((getAffiliationValue() == null) || getAffiliationValue()
                     .trim().equals(""))) {
                isAffiliationNull = true;
            }
            if( !isAffiliationNull ){
                if( checkForInvalidMilitaryinfoAffiliation()){
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_AFFILIATION).longValue());
                    err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                    if (logger.isFinestEnabled()) {
                        String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                    }
                    status.addError(err);
                 }
            }
            boolean isServiceBeginDateNull = (eObjMilitaryInfo.getServiceBeginDate() == null);
            if (!isValidServiceBeginDate) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_SERVICEBEGINDATE).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property ServiceBeginDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                status.addError(err);
            } 
            boolean isServiceEndDateNull = (eObjMilitaryInfo.getServiceEndDate() == null);
            if (!isValidServiceEndDate) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_SERVICEENDDATE).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property ServiceEndDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                status.addError(err);
            } 
            boolean isStartDateNull = (eObjMilitaryInfo.getStartDate() == null);
            if (!isValidStartDate) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_STARTDATE).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property StartDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                status.addError(err);
            } 
            boolean isEndDateNull = (eObjMilitaryInfo.getEndDate() == null);
            if (!isValidEndDate) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_ENDDATE).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property EndDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                status.addError(err);
            } 
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO: Add any common component-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

            boolean isMilitaryInfoPkIdNull = (eObjMilitaryInfo.getMilitaryInfoPkId() == null);
            boolean isPartyIdNull = (eObjMilitaryInfo.getPartyId() == null);
            if( isPartyIdNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.MILITARYINFO_PARTYID_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property PartyId is null, in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            }
            //persistent code type
            boolean isBranchNull = false;
            if ((eObjMilitaryInfo.getBRANCH_TP_CD() == null)
                 && ((getBranchValue() == null) || getBranchValue()
                     .trim().equals(""))) {
                isBranchNull = true;
            }
            if( isBranchNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.MILITARYINFO_BRANCH_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property Branch is null, in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            } else {
            	if( checkForInvalidMilitaryinfoBranch()){
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_BRANCH).longValue());
                    err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                    if (logger.isFinestEnabled()) {
                        String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                    }
                    status.addError(err);
                 }
            }
            //persistent code type
            boolean isAffiliationNull = false;
            if ((eObjMilitaryInfo.getAffiliation() == null)
                 && ((getAffiliationValue() == null) || getAffiliationValue()
                     .trim().equals(""))) {
                isAffiliationNull = true;
            }
            
            if( !isAffiliationNull ){
                if( checkForInvalidMilitaryinfoAffiliation()){
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_AFFILIATION).longValue());
                    err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                    if (logger.isFinestEnabled()) {
                        String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                    }
                    status.addError(err);
                 }
            }
            boolean isServiceBeginDateNull = (eObjMilitaryInfo.getServiceBeginDate() == null);
            boolean isServiceEndDateNull = (eObjMilitaryInfo.getServiceEndDate() == null);
            boolean isStartDateNull = (eObjMilitaryInfo.getStartDate() == null);
            if( isStartDateNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.MILITARYINFO_STARTDATE_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property StartDate is null, in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            }
            boolean isEndDateNull = (eObjMilitaryInfo.getEndDate() == null);
            if(! isStartDateNull && ! isEndDateNull) {
            	if(eObjMilitaryInfo.getStartDate().after(eObjMilitaryInfo.getEndDate())) {
            		DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.STARTDATE_GREATERTHAN_ENDDATE));
                    err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                    status.addError(err);
            	}
            }
        }
        
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);

        }
        
        return status;
    
    	}

    /*	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "PartyId"
     *
     * @generated
     *//*
	private void componentValidation_PartyId(DWLStatus status) {
  
            boolean isPartyIdNull = (eObjMilitaryInfo.getPartyId() == null);
            if (isPartyIdNull) {
                DWLError err = createDWLError("MilitaryInfo", "PartyId", CMDMAdditionsErrorReasonCode.MILITARYINFO_PARTYID_NULL);
                status.addError(err); 
            }
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "Branch"
     *
     * @generated
     *//*
	private void componentValidation_Branch(DWLStatus status) {
  
      //persistent code type
            boolean isBranchNull = false;
            if ((eObjMilitaryInfo.getBRANCH_TP_CD() == null) &&
               ((getBranchValue() == null) || 
                 getBranchValue().trim().equals(""))) {
                isBranchNull = true;
            }
            if (isBranchNull) {
                DWLError err = createDWLError("MilitaryInfo", "Branch", CMDMAdditionsErrorReasonCode.MILITARYINFO_BRANCH_NULL);
                status.addError(err); 
            }
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "Affiliation"
     *
     * @generated
     *//*
	private void componentValidation_Affiliation(DWLStatus status) {
  
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "ServiceBeginDate"
     *
     * @generated
     *//*
	private void componentValidation_ServiceBeginDate(DWLStatus status) {
  
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "ServiceEndDate"
     *
     * @generated
     *//*
	private void componentValidation_ServiceEndDate(DWLStatus status) {
  
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "StartDate"
     *
     * @generated
     *//*
	private void componentValidation_StartDate(DWLStatus status) {
  
            boolean isStartDateNull = (eObjMilitaryInfo.getStartDate() == null);
            if (isStartDateNull) {
                DWLError err = createDWLError("MilitaryInfo", "StartDate", CMDMAdditionsErrorReasonCode.MILITARYINFO_STARTDATE_NULL);
                status.addError(err); 
            }
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform component-level custom validation logic for attribute "EndDate"
     *
     * @generated
     *//*
	private void componentValidation_EndDate(DWLStatus status) {
  
  }
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "PartyId"
     *
     * @generated
     *//*
	private void controllerValidation_PartyId(DWLStatus status) throws Exception {
  
            boolean isPartyIdNull = (eObjMilitaryInfo.getPartyId() == null);
            if (isPartyIdNull) {
                DWLError err = createDWLError("MilitaryInfo", "PartyId", CMDMAdditionsErrorReasonCode.MILITARYINFO_PARTYID_NULL);
                status.addError(err); 
            }
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "Branch"
     *
     * @generated
     *//*
	private void controllerValidation_Branch(DWLStatus status) throws Exception {
  
      //persistent code type
            boolean isBranchNull = false;
            if ((eObjMilitaryInfo.getBRANCH_TP_CD() == null) &&
               ((getBranchValue() == null) || 
                 getBranchValue().trim().equals(""))) {
                isBranchNull = true;
            }
            if (isBranchNull) {
                DWLError err = createDWLError("MilitaryInfo", "Branch", CMDMAdditionsErrorReasonCode.MILITARYINFO_BRANCH_NULL);
                status.addError(err); 
            }
            if (!isBranchNull) {
                if (checkForInvalidMilitaryinfoBranch()) {
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_BRANCH).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
        if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
      logger.finest("controllerValidation_Branch " + infoForLogging);
        }
                status.addError(err);
            }

             }
    			
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "Affiliation"
     *
     * @generated
     *//*
	private void controllerValidation_Affiliation(DWLStatus status) throws Exception {
  
      //persistent code type
            boolean isAffiliationNull = false;
            if ((eObjMilitaryInfo.getAffiliation() == null) &&
               ((getAffiliationValue() == null) || 
                 getAffiliationValue().trim().equals(""))) {
                isAffiliationNull = true;
            }
            if (!isAffiliationNull) {
                if (checkForInvalidMilitaryinfoAffiliation()) {
                    DWLError err = new DWLError();
                    err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
                    err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_AFFILIATION).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
        if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Custom validation error occured for entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
      logger.finest("controllerValidation_Affiliation " + infoForLogging);
        }
                status.addError(err);
            }

             }
    			
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "ServiceBeginDate"
     *
     * @generated
     *//*
	private void controllerValidation_ServiceBeginDate(DWLStatus status) throws Exception {
  
            boolean isServiceBeginDateNull = (eObjMilitaryInfo.getServiceBeginDate() == null);
            if (!isValidServiceBeginDate) {
              	DWLError err = new DWLError();
               	err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
               	err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_SERVICEBEGINDATE).longValue());
               	err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property ServiceBeginDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
      logger.finest("controllerValidation_ServiceBeginDate " + infoForLogging);
               	status.addError(err);
            } 
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "ServiceEndDate"
     *
     * @generated
     *//*
	private void controllerValidation_ServiceEndDate(DWLStatus status) throws Exception {
  
            boolean isServiceEndDateNull = (eObjMilitaryInfo.getServiceEndDate() == null);
            if (!isValidServiceEndDate) {
              	DWLError err = new DWLError();
               	err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
               	err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_SERVICEENDDATE).longValue());
               	err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property ServiceEndDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
      logger.finest("controllerValidation_ServiceEndDate " + infoForLogging);
               	status.addError(err);
            } 
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "StartDate"
     *
     * @generated
     *//*
	private void controllerValidation_StartDate(DWLStatus status) throws Exception {
  
            boolean isStartDateNull = (eObjMilitaryInfo.getStartDate() == null);
            if (!isValidStartDate) {
              	DWLError err = new DWLError();
               	err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
               	err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_STARTDATE).longValue());
               	err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property StartDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
      logger.finest("controllerValidation_StartDate " + infoForLogging);
               	status.addError(err);
            } 
            if (isStartDateNull) {
                DWLError err = createDWLError("MilitaryInfo", "StartDate", CMDMAdditionsErrorReasonCode.MILITARYINFO_STARTDATE_NULL);
                status.addError(err); 
            }
    	}
    	
    *//**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform controller-level custom validation logic for attribute "EndDate"
     *
     * @generated
     *//*
	private void controllerValidation_EndDate(DWLStatus status) throws Exception {
  
            boolean isEndDateNull = (eObjMilitaryInfo.getEndDate() == null);
            if (!isValidEndDate) {
              	DWLError err = new DWLError();
               	err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
               	err.setReasonCode(new Long(CMDMAdditionsErrorReasonCode.INVALID_MILITARYINFO_ENDDATE).longValue());
               	err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                String infoForLogging="Error: Validation error. Invalid time specified on property EndDate in entity MilitaryInfo, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
      logger.finest("controllerValidation_EndDate " + infoForLogging);
               	status.addError(err);
            } 
    	}
    private DWLError createDWLError(String entityName, String propertyName,String reasonCode){	
		DWLError err = new DWLError();
		err.setComponentType(new Long(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ).longValue());
		err.setReasonCode(new Long(reasonCode).longValue());
		err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
        if (logger.isFinestEnabled()) {
			String infoForLogging="Error: Validation error occured. Property " + propertyName + " is null, in entity " + entityName + ", component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
			logger.finest("createDWLError " + infoForLogging);
		}
		return err;
    }
    */
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Check the value of the field Branch and return true if the error reason
     * INVALID_MILITARYINFO_BRANCH should be returned.
     *
     * @generated
  **/
	private boolean checkForInvalidMilitaryinfoBranch() throws Exception {
    logger.finest("ENTER checkForInvalidMilitaryinfoBranch()");
        boolean notValid = false;
      	String langId = (String) this.getControl().get(DWLControlKeys.LANG_ID);
      	CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
    Long codeType = DWLFunctionUtils.getLongFromString( getBranchType() );
    String codeValue = getBranchValue();
    if( codeValue != null && codeValue.trim().equals("")){
      codeValue = null;
    }
     
        if ( codeType != null && codeValue == null ){

             if( codeTypeCompHelper.isCodeValid("t416_mty_lct_typ", langId, getBranchType(),
                 CodeTypeComponentHelper.ACTIVE, getControl())) {
                 CodeTypeBObj ctBObj = codeTypeCompHelper
                 					.getCodeTypeByCode("t416_mty_lct_typ", langId, getBranchType(),
                         								getControl());
               	if (ctBObj != null) {
                   	setBranchValue( ctBObj.getvalue() );
               	}
             }
             else{
             	if (logger.isFinestEnabled()) {
              	 	String infoForLogging="NotValid 1";
      logger.finest("checkForInvalidMilitaryinfoBranch() " + infoForLogging);
        }
                 notValid = true;
           }
        }
        else if (codeType == null && codeValue != null ){
          
             CodeTypeBObj ctBObj = codeTypeCompHelper
                      .getCodeTypeByValue("t416_mty_lct_typ", langId, codeValue,
                                 getControl());

             if (ctBObj != null) {
                 setBranchType(ctBObj.gettp_cd());
             } 
             else {
             	if (logger.isFinestEnabled()) {
              	  	String infoForLogging="NotValid 2";
      logger.finest("checkForInvalidMilitaryinfoBranch() " + infoForLogging);
        }
                  notValid = true;
             }
        }
        else if ( codeType != null && codeValue != null
             && !codeTypeCompHelper.isCodeValuePairValid("t416_mty_lct_typ", langId, getBranchType(), 
                     new String[] { codeValue }, CodeTypeComponentHelper.ACTIVE, getControl())) {	
       if (logger.isFinestEnabled()) {
       		String infoForLogging="NotValid 3";
      logger.finest("checkForInvalidMilitaryinfoBranch() " + infoForLogging);
       }
             notValid = true;
        }
        if (logger.isFinestEnabled()) {
      String returnValue ="" +notValid;
      logger.finest("RETURN checkForInvalidMilitaryinfoBranch() " + returnValue);
    }
    return notValid;
     } 
				 
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Check the value of the field Affiliation and return true if the error
     * reason INVALID_MILITARYINFO_AFFILIATION should be returned.
     *
     * @generated
  **/
	private boolean checkForInvalidMilitaryinfoAffiliation() throws Exception {
    logger.finest("ENTER checkForInvalidMilitaryinfoAffiliation()");
        boolean notValid = false;
      	String langId = (String) this.getControl().get(DWLControlKeys.LANG_ID);
      	CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
    Long codeType = DWLFunctionUtils.getLongFromString( getAffiliationType() );
    String codeValue = getAffiliationValue();
    if( codeValue != null && codeValue.trim().equals("")){
      codeValue = null;
    }
     
        if ( codeType != null && codeValue == null ){

             if( codeTypeCompHelper.isCodeValid("t395_mty_afl_sts_typ", langId, getAffiliationType(),
                 CodeTypeComponentHelper.ACTIVE, getControl())) {
                 CodeTypeBObj ctBObj = codeTypeCompHelper
                 					.getCodeTypeByCode("t395_mty_afl_sts_typ", langId, getAffiliationType(),
                         								getControl());
               	if (ctBObj != null) {
                   	setAffiliationValue( ctBObj.getvalue() );
               	}
             }
             else{
             	if (logger.isFinestEnabled()) {
              	 	String infoForLogging="NotValid 1";
      logger.finest("checkForInvalidMilitaryinfoAffiliation() " + infoForLogging);
        }
                 notValid = true;
           }
        }
        else if (codeType == null && codeValue != null ){
          
             CodeTypeBObj ctBObj = codeTypeCompHelper
                      .getCodeTypeByValue("t395_mty_afl_sts_typ", langId, codeValue,
                                 getControl());

             if (ctBObj != null) {
                 setAffiliationType(ctBObj.gettp_cd());
             } 
             else {
             	if (logger.isFinestEnabled()) {
              	  	String infoForLogging="NotValid 2";
      logger.finest("checkForInvalidMilitaryinfoAffiliation() " + infoForLogging);
        }
                  notValid = true;
             }
        }
        else if ( codeType != null && codeValue != null
             && !codeTypeCompHelper.isCodeValuePairValid("t395_mty_afl_sts_typ", langId, getAffiliationType(), 
                     new String[] { codeValue }, CodeTypeComponentHelper.ACTIVE, getControl())) {	
       if (logger.isFinestEnabled()) {
       		String infoForLogging="NotValid 3";
      logger.finest("checkForInvalidMilitaryinfoAffiliation() " + infoForLogging);
       }
             notValid = true;
        }
        if (logger.isFinestEnabled()) {
      String returnValue ="" +notValid;
      logger.finest("RETURN checkForInvalidMilitaryinfoAffiliation() " + returnValue);
    }
    return notValid;
     } 
				 



}

