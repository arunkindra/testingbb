package com.lowes.app.cmdm.mrp.component;

import java.util.HashMap;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.tcrm.common.IExtension;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.exception.TCRMDeleteException;
import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.exception.TCRMUpdateException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsPropertyKeys;;

public class XPersonBObjExt extends TCRMPersonBObj implements IExtension {

	protected Vector<MilitaryInfoBObj> vecMilitaryInfoBObj = null;

	private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager
			.getLogger(XPersonBObjExt.class);

	public XPersonBObjExt() {
		super();
		vecMilitaryInfoBObj = new Vector<MilitaryInfoBObj>();
		setComponentID(CMDMAdditionsComponentID.MILITARY_INFO_BOBJ);
	}

	public void setControl(DWLControl newDWLControl) {
		super.setControl(newDWLControl);

		if (vecMilitaryInfoBObj != null && vecMilitaryInfoBObj.size() > 0) {
			for (MilitaryInfoBObj militaryInfoBObj : vecMilitaryInfoBObj) {
				militaryInfoBObj.setControl(newDWLControl);
			}
		}
	}

	public Vector<MilitaryInfoBObj> getItemsMilitaryInfoBObj() {
		return this.vecMilitaryInfoBObj;
	}

	public void setMilitaryInfoBObj(MilitaryInfoBObj militaryInfoBObj) {
		vecMilitaryInfoBObj.addElement(militaryInfoBObj);
	}

	public void addRecord() throws DWLBaseException {
		try {
			if (vecMilitaryInfoBObj != null && vecMilitaryInfoBObj.size() > 0) {
				Vector<MilitaryInfoBObj> vecAddedMilitaryInfoBObj = new Vector<MilitaryInfoBObj>();
				CMDMAdditionsComponent cmdmMRPComp = (CMDMAdditionsComponent) TCRMClassFactory
						.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
				for (MilitaryInfoBObj militaryInfoBObj : vecMilitaryInfoBObj) {
					militaryInfoBObj.setPartyId(this.getPartyId());
					if (StringUtils.isBlank(militaryInfoBObj.getStartDate())) {
						militaryInfoBObj.setStartDate(DWLDateTimeUtilities
								.getCurrentSystemTime());
					}
					DWLResponse response = cmdmMRPComp
							.addMilitaryInfo(militaryInfoBObj);
					if (response != null
							&& (response.getStatus().getStatus()) != DWLStatus.FATAL) {
						MilitaryInfoBObj addedMilitaryInfoBObj = (MilitaryInfoBObj) response
								.getData();
						vecAddedMilitaryInfoBObj.add(addedMilitaryInfoBObj);
					}
				}
				this.vecMilitaryInfoBObj.clear();
				if (vecAddedMilitaryInfoBObj != null
						&& vecAddedMilitaryInfoBObj.size() > 0)
					this.vecMilitaryInfoBObj.addAll(vecAddedMilitaryInfoBObj);
			}
		} catch (DWLBaseException e) {
			throw e;
		} catch (Exception e) {
			if (logger.isFinestEnabled()) {
				String infoForLogging = "Error: Insert record error "
						+ e.getMessage();
				logger.finest("addRecord() " + infoForLogging);

			}
			status = new DWLStatus();

			TCRMInsertException insertEx = new TCRMInsertException();
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler.getErrorMessage(
					CMDMAdditionsComponentID.MILITARY_INFO_BOBJ,
					TCRMErrorCode.INSERT_RECORD_ERROR,
					CMDMAdditionsErrorReasonCode.ADDMILITARYINFO_FAILED,
					getControl(), new String[0]);
			error.setThrowable(e);
			status.addError(error);
			status.setStatus(DWLStatus.FATAL);
			insertEx.setStatus(status);
			throw insertEx;
		}
		logger.finest("RETURN addRecord()");
	}

	public void updateRecord() throws DWLBaseException {
		logger.finest("ENTER updateRecord()");

		try {
			if (vecMilitaryInfoBObj != null && vecMilitaryInfoBObj.size() > 0) {
				Vector<MilitaryInfoBObj> vecProcessedMilitaryInfoBObj = new Vector<MilitaryInfoBObj>();
				CMDMAdditionsComponent cmdmMRPComp = (CMDMAdditionsComponent) TCRMClassFactory
						.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
				for (MilitaryInfoBObj militaryInfoBObj : vecMilitaryInfoBObj) {
					DWLResponse response = null;

					if (StringUtils.isNonBlank(militaryInfoBObj
							.getMilitaryInfoPkId())) {
						response = cmdmMRPComp
								.updateMilitaryInfo(militaryInfoBObj);
					} else // prepare for add
					{
						militaryInfoBObj.setPartyId(this.getPartyId());
						if (StringUtils
								.isBlank(militaryInfoBObj.getStartDate())) {
							militaryInfoBObj.setStartDate(DWLDateTimeUtilities
									.getCurrentSystemTime());
						}
						response = cmdmMRPComp
								.addMilitaryInfo(militaryInfoBObj);
					}
					if (response != null
							&& (response.getStatus().getStatus()) != DWLStatus.FATAL) {
						MilitaryInfoBObj tempMilitaryInfoBObj = (MilitaryInfoBObj) response
								.getData();
						vecProcessedMilitaryInfoBObj.add(tempMilitaryInfoBObj);
					}
				}
				this.getItemsMilitaryInfoBObj().clear();
				if (vecProcessedMilitaryInfoBObj != null
						&& vecProcessedMilitaryInfoBObj.size() > 0) {
					this.getItemsMilitaryInfoBObj().addAll(
							vecProcessedMilitaryInfoBObj);
				}
			}
		} catch (DWLBaseException e) {
			throw e;
		} catch (Exception e) {
			if (logger.isFinestEnabled()) {
				String infoForLogging = "Error: Update record error "
						+ e.getMessage();
				logger.finest("UpdateRecord() " + infoForLogging);

			}
			status = new DWLStatus();

			TCRMUpdateException updateEx = new TCRMUpdateException();
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler.getErrorMessage(
					CMDMAdditionsComponentID.MILITARY_INFO_BOBJ,
					TCRMErrorCode.UPDATE_RECORD_ERROR,
					CMDMAdditionsErrorReasonCode.UPDATEMILITARYINFO_FAILED,
					getControl(), new String[0]);
			error.setThrowable(e);
			status.addError(error);
			status.setStatus(DWLStatus.FATAL);
			updateEx.setStatus(status);
			throw updateEx;
		}
		logger.finest("RETURN updateRecord()");
	}

	public void getRecord() throws DWLBaseException {
		logger.finest("ENTER getRecord()");
		if(((HashMap<String, Boolean>)((this.getControl().getCustomizationInquiryLevelMap()).get("Party"))).get("MilitaryInfoBObj") != null) {
			try {
				//logger.fatal((this.getControl().getCustomizationInquiryLevelMap()).get("Party"));
				
				CMDMAdditionsComponent cmdmMRPComp = (CMDMAdditionsComponent) TCRMClassFactory
						.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
				Vector<MilitaryInfoBObj> vecExistingMilitaryInfoBObj = null;
				DWLResponse response = cmdmMRPComp.getAllMilitaryInfoByPartyId(this.getPartyId(), this.getControl());
				if (response != null
						&& (response.getStatus().getStatus()) != DWLStatus.FATAL) {
					vecExistingMilitaryInfoBObj = (Vector<MilitaryInfoBObj>) response
							.getData();
				}	
				
				if (vecExistingMilitaryInfoBObj != null
						&& vecExistingMilitaryInfoBObj.size() > 0) {

					this.getItemsMilitaryInfoBObj().clear();
					this.getItemsMilitaryInfoBObj().addAll(
							vecExistingMilitaryInfoBObj);
				}
			} catch (DWLBaseException e) {
				throw e;
			} catch (Exception e) {
				if (logger.isFinestEnabled()) {
					String infoForLogging = "Error: Error reading record "
							+ e.getMessage();
					logger.finest("getRecord() " + infoForLogging);
				}
				status = new DWLStatus();

				TCRMReadException readEx = new TCRMReadException();
				IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
				DWLError error = errHandler.getErrorMessage(
						CMDMAdditionsComponentID.MILITARY_INFO_BOBJ,
						TCRMErrorCode.READ_RECORD_ERROR,
						CMDMAdditionsErrorReasonCode.GETMILITARYINFO_FAILED,
						getControl(), new String[0]);
				error.setThrowable(e);
				status.addError(error);
				status.setStatus(DWLStatus.FATAL);
				readEx.setStatus(status);
				throw readEx;
			}
		} 
		logger.finest("RETURN getRecord()");
	}
	
	public void deleteRecord() throws DWLBaseException {
		logger.finest("ENTER deleteRecord()");
		try {
			CMDMAdditionsComponent cmdmMRPComp = (CMDMAdditionsComponent) TCRMClassFactory.getTCRMComponent(
					CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
			cmdmMRPComp.deleteMilitaryInfo(this.getPartyId());
		} catch (DWLBaseException e) {
			throw e;
		} catch (Exception e) {
			if (logger.isFinestEnabled()) {
				String infoForLogging = "Error: Delete record error "
						+ e.getMessage();
				logger.finest("DeleteRecord() " + infoForLogging);

			}
			status = new DWLStatus();

			TCRMDeleteException deleteEx = new TCRMDeleteException();
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler.getErrorMessage(
					CMDMAdditionsComponentID.MILITARY_INFO_BOBJ,
					TCRMErrorCode.DELETE_RECORD_ERROR,
					CMDMAdditionsErrorReasonCode.DELETEMILITARYINFO_FAILED,
					getControl(), new String[0]);
			error.setThrowable(e);
			status.addError(error);
			status.setStatus(DWLStatus.FATAL);
			deleteEx.setStatus(status);
			throw deleteEx;
		}
		logger.finest("RETURN deleteRecord()");
	}

}
