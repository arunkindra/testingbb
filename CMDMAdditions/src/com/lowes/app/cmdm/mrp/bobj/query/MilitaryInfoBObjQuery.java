
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[1b3b023a80bc7061e62d335372a7458b]
 */
package com.lowes.app.cmdm.mrp.bobj.query;




import com.dwl.base.DWLControl;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.base.DWLCommon;

import com.dwl.base.db.DataAccessFactory;


import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;

import com.dwl.base.exception.DWLDuplicateKeyException;

import com.dwl.base.interfaces.IGenericResultSetProcessor;

import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;

import com.lowes.app.cmdm.mrp.component.MilitaryInfoBObj;
import com.lowes.app.cmdm.mrp.component.MilitaryInfoResultSetProcessor;

import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;

import com.lowes.app.cmdm.mrp.entityObject.EObjMilitaryInfoData;
import com.lowes.app.cmdm.mrp.entityObject.MilitaryInfoInquiryData;


/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides query information for the business object
 * <code>MilitaryInfoBObj</code>.
 *
 * @generated
 */
public class MilitaryInfoBObjQuery  extends com.dwl.bobj.query.GenericBObjQuery {

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String MILITARY_INFO_QUERY = "getMilitaryInfo(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String MILITARY_INFO_HISTORY_QUERY = "getMilitaryInfoHistory(Object[])";

	/**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String ALL_MILITARY_INFO_BY_PARTY_ID_QUERY = "getAllMilitaryInfoByPartyId(Object[])";

    /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String ALL_MILITARY_INFO_BY_PARTY_ID_HISTORY_QUERY = "getAllMilitaryInfoByPartyIdHistory(Object[])";

  /**
    * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
    * @generated 
    */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(MilitaryInfoBObjQuery.class);
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String MILITARY_INFO_ADD = "MILITARY_INFO_ADD";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String MILITARY_INFO_DELETE = "MILITARY_INFO_DELETE";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String MILITARY_INFO_UPDATE = "MILITARY_INFO_UPDATE";


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param queryName
     * The name of the query.
     * @param control
     * The control object.
     *
     * @generated
     */
    public MilitaryInfoBObjQuery(String queryName, DWLControl control) {
        super(queryName, control);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param persistenceStrategyName
     * The persistence strategy name.  This parameter indicates the type of
     * database action to be taken such as addition, update or deletion of
     * records.
     * @param objectToPersist
     * The business object to be persisted.
     *
     * @generated
     */
    public MilitaryInfoBObjQuery(String persistenceStrategyName, DWLCommon objectToPersist) {
        super(persistenceStrategyName, objectToPersist);
    }

	 
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	protected void persist() throws Exception{
    logger.finest("ENTER persist()");
    if (logger.isFinestEnabled()) {
   		String infoForLogging="Persistence strategy is " + persistenceStrategyName;
      logger.finest("persist() " + infoForLogging);
        }
    if (persistenceStrategyName.equals(MILITARY_INFO_ADD)) {
      addMilitaryInfo();
    }else if(persistenceStrategyName.equals(MILITARY_INFO_UPDATE)) {
      updateMilitaryInfo();
    }else if(persistenceStrategyName.equals(MILITARY_INFO_DELETE)) {
      deleteMilitaryInfo();
    }
    logger.finest("RETURN persist()");
  }
  
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Inserts militaryinfo data by calling
      * <code>EObjMilitaryInfoData.createEObjMilitaryInfo</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void addMilitaryInfo() throws Exception{
    logger.finest("ENTER addMilitaryInfo()");
    
    EObjMilitaryInfoData theEObjMilitaryInfoData = (EObjMilitaryInfoData) DataAccessFactory
      .getQuery(EObjMilitaryInfoData.class, connection);
    theEObjMilitaryInfoData.createEObjMilitaryInfo(((MilitaryInfoBObj) objectToPersist).getEObjMilitaryInfo());
    logger.finest("RETURN addMilitaryInfo()");
  }

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Updates militaryinfo data by calling
      * <code>EObjMilitaryInfoData.updateEObjMilitaryInfo</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void updateMilitaryInfo() throws Exception{
    logger.finest("ENTER updateMilitaryInfo()");
    EObjMilitaryInfoData theEObjMilitaryInfoData = (EObjMilitaryInfoData) DataAccessFactory
      .getQuery(EObjMilitaryInfoData.class, connection);
    theEObjMilitaryInfoData.updateEObjMilitaryInfo(((MilitaryInfoBObj) objectToPersist).getEObjMilitaryInfo());
    logger.finest("RETURN updateMilitaryInfo()");
  }

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
      * Deletes {0} data by calling <{1}>{2}.{3}{4}</{1}>
   *
     * @throws Exception
     *
     * @generated
     */
	protected void deleteMilitaryInfo() throws Exception{
    logger.finest("ENTER deleteMilitaryInfo()");
         // MDM_TODO: CDKWB0018I Write customized business logic for the extension here.
    logger.finest("RETURN deleteMilitaryInfo()");
  } 
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * This method is overridden to construct
      * <code>DWLDuplicateKeyException</code> based on MilitaryInfo component
      * specific values.
     * 
     * @param errParams
     * The values to be substituted in the error message.
   *
     * @throws Exception
     *
     * @generated
     */
    protected void throwDuplicateKeyException(String[] errParams) throws Exception {
    if (logger.isFinestEnabled()) {
      	StringBuilder errParamsStringBuilder = new StringBuilder("Error: Duplicate key Exception parameters are ");
      	for(int i=0;i<errParams.length;i++) {
      		errParamsStringBuilder .append(errParams[i]);
      		if (i!=errParams.length-1) {
      			errParamsStringBuilder .append(" , ");
      		}
      	}
          String infoForLogging="Error: Duplicate key Exception parameters are " + errParamsStringBuilder;
      logger.finest("Unknown method " + infoForLogging);
    }
    	DWLExceptionUtils.throwDWLDuplicateKeyException(
    		new DWLDuplicateKeyException(buildDupThrowableMessage(errParams)),
    		objectToPersist.getStatus(), 
    		DWLStatus.FATAL,
    		CMDMAdditionsComponentID.CMDMADDITIONS_COMPONENT,
    		DWLErrorCode.DUPLICATE_KEY_ERROR, 
    		CMDMAdditionsErrorReasonCode.DUPLICATE_PRIMARY_KEY_MILITARYINFO,
    		objectToPersist.getControl(), 
    		DWLClassFactory.getErrorHandler()
    		);
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Provides the result set processor that is used to populate the business
     * object.
     *
     * @return
     * An instance of <code>MilitaryInfoResultSetProcessor</code>.
     *
     * @see com.dwl.bobj.query.AbstractBObjQuery#provideResultSetProcessor()
     * @see com.lowes.app.cmdm.mrp.component.MilitaryInfoResultSetProcessor
     *
     * @generated
     */
    protected IGenericResultSetProcessor provideResultSetProcessor()
            throws BObjQueryException {

        return new MilitaryInfoResultSetProcessor();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    @Override
    protected Class<MilitaryInfoInquiryData> provideQueryInterfaceClass() throws BObjQueryException {
        return MilitaryInfoInquiryData.class;
    }

}


