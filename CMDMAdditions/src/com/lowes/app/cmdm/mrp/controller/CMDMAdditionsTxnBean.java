	
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[aaffe603f566f752b8479934c05eff78]
 */

package com.lowes.app.cmdm.mrp.controller;

import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.lowes.app.cmdm.mrp.component.MilitaryInfoBObj;

import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsPropertyKeys;

import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditions;
import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditionsTxn;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller implementation for all persistent CMDMAdditions services.
 * @generated
 */
@Controller(errorComponentID = CMDMAdditionsComponentID.CMDMADDITIONS_CONTROLLER)
public class CMDMAdditionsTxnBean  extends TCRMCommonComponent implements CMDMAdditionsTxn {

	/**
    * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
    * @generated 
    */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CMDMAdditionsTxnBean.class);
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleAddMilitaryInfo.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleaddMilitaryInfo
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR, 
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.ADDMILITARYINFO_FAILED)
    public DWLResponse addMilitaryInfo(MilitaryInfoBObj theBObj) throws DWLBaseException {
    logger.finest("ENTER addMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addMilitaryInfo", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction addMilitaryInfo execution";
      logger.finest("addMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
    }
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction addMilitaryInfo execution";
      logger.finest("addMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN addMilitaryInfo(MilitaryInfoBObj theBObj) " + returnValue);
    }
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction addMilitaryInfo.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #addMilitaryInfo
     *
     * @generated
     */
    public DWLResponse handleAddMilitaryInfo(MilitaryInfoBObj theBObj) throws Exception {
    logger.finest("ENTER handleAddMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CMDMAdditions aCMDMAdditionsComponent = (CMDMAdditions) TCRMClassFactory
      .getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
        response = aCMDMAdditionsComponent.addMilitaryInfo(theBObj);
    logger.finest("RETURN handleAddMilitaryInfo(MilitaryInfoBObj theBObj)");
        return response;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleUpdateMilitaryInfo.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleupdateMilitaryInfo
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR, 
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.UPDATEMILITARYINFO_FAILED)
    public DWLResponse updateMilitaryInfo(MilitaryInfoBObj theBObj) throws DWLBaseException {
    logger.finest("ENTER updateMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateMilitaryInfo", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction updateMilitaryInfo execution";
      logger.finest("updateMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
    }
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction updateMilitaryInfo execution";
      logger.finest("updateMilitaryInfo(MilitaryInfoBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN updateMilitaryInfo(MilitaryInfoBObj theBObj) " + returnValue);
    }
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction updateMilitaryInfo.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #updateMilitaryInfo
     *
     * @generated
     */
    public DWLResponse handleUpdateMilitaryInfo(MilitaryInfoBObj theBObj) throws Exception {
    logger.finest("ENTER handleUpdateMilitaryInfo(MilitaryInfoBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CMDMAdditions aCMDMAdditionsComponent = (CMDMAdditions) TCRMClassFactory
      .getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
        response = aCMDMAdditionsComponent.updateMilitaryInfo(theBObj);
    logger.finest("RETURN handleUpdateMilitaryInfo(MilitaryInfoBObj theBObj)");
        return response;
    }
}



