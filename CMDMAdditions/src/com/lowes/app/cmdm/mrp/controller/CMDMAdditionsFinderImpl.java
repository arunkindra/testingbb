/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[d2f73bd5ef3e914683d0c621931598d3]
 */

package com.lowes.app.cmdm.mrp.controller;


import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.dwl.base.error.DWLStatus;

import com.dwl.base.util.DWLExceptionUtils;

import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsComponentID;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsErrorReasonCode;
import com.lowes.app.cmdm.mrp.constant.CMDMAdditionsPropertyKeys;

import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditions;
import com.lowes.app.cmdm.mrp.interfaces.CMDMAdditionsFinder;

import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller class to handle inquiry requests.
 * @generated
 */
 @Controller(errorComponentID = CMDMAdditionsComponentID.CMDMADDITIONS_CONTROLLER)
public class CMDMAdditionsFinderImpl extends TCRMCommonComponent implements CMDMAdditionsFinder {

    private IDWLErrorMessage errHandler;
	/**
    * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
    * @generated 
    */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CMDMAdditionsFinderImpl.class);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     * @generated
     */
    public CMDMAdditionsFinderImpl() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getMilitaryInfo.
     *
     * @param MilitaryInfoPkId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetMilitaryInfo
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.GETMILITARYINFO_FAILED)
     public DWLResponse getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) throws DWLBaseException {
    logger.finest("ENTER getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(MilitaryInfoPkId);
        DWLTransaction txObj = new DWLTransactionInquiry("getMilitaryInfo", params, control);
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
      String infoForLogging="Before finder transaction execution for getMilitaryInfo";
      logger.finest("getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + infoForLogging);
    }
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
       		String infoForLogging="After finder transaction execution for getMilitaryInfo";
      logger.finest("getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN getMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) " + returnValue);
    }
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getMilitaryInfo.
     * 
     * @param MilitaryInfoPkId
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated
     */
    public DWLResponse  handleGetMilitaryInfo(String MilitaryInfoPkId,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

    CMDMAdditions comp = 
      (CMDMAdditions)TCRMClassFactory.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
      
        response = comp.getMilitaryInfo(MilitaryInfoPkId,  control);
        if (response.getData() == null) {
            String[] params = new String[] { MilitaryInfoPkId };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
          CMDMAdditionsComponentID.CMDMADDITIONS_CONTROLLER,
          TCRMErrorCode.READ_RECORD_ERROR,
          CMDMAdditionsErrorReasonCode.GETMILITARYINFO_FAILED,
          control, params, errHandler);					
        }
        return response;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllMilitaryInfoByPartyId.
     *
     * @param PartyId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllMilitaryInfoByPartyId
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CMDMAdditionsErrorReasonCode.GETALLMILITARYINFOBYPARTYID_FAILED)
     public DWLResponse getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) throws DWLBaseException {
    logger.finest("ENTER getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(PartyId);
        DWLTransaction txObj = new DWLTransactionInquiry("getAllMilitaryInfoByPartyId", params, control);
        DWLResponse retObj = null;
    if (logger.isFinestEnabled()) {
      String infoForLogging="Before finder transaction execution for getAllMilitaryInfoByPartyId";
      logger.finest("getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + infoForLogging);
    }
        retObj = executeTx(txObj);
    if (logger.isFinestEnabled()) {
       		String infoForLogging="After finder transaction execution for getAllMilitaryInfoByPartyId";
      logger.finest("getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
      logger.finest("RETURN getAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) " + returnValue);
    }
        return retObj;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getAllMilitaryInfoByPartyId.
     * 
     * @param PartyId
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated
     */
    public DWLResponse  handleGetAllMilitaryInfoByPartyId(String PartyId,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

    CMDMAdditions comp = 
      (CMDMAdditions)TCRMClassFactory.getTCRMComponent(CMDMAdditionsPropertyKeys.CMDMADDITIONS_COMPONENT);
      
        response = comp.getAllMilitaryInfoByPartyId(PartyId,  control);
        if (response.getData() == null) {
            String[] params = new String[] { PartyId };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
          CMDMAdditionsComponentID.CMDMADDITIONS_CONTROLLER,
          TCRMErrorCode.READ_RECORD_ERROR,
          CMDMAdditionsErrorReasonCode.GETALLMILITARYINFOBYPARTYID_FAILED,
          control, params, errHandler);					
        }
        return response;
    }
    

}


