/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[e8df8f1d11edd0c21ca139f0dd4203f1]
 */

package com.lowes.app.cmdm.mrp.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the component IDs used in this module.
 *
 * @generated
 */
public class CMDMAdditionsComponentID {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CMDMAdditionsComponent.
     *
     * @generated
     */
    public final static String CMDMADDITIONS_COMPONENT = "8050007";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CMDMAdditionsController.
     *
     * @generated
     */
    public final static String CMDMADDITIONS_CONTROLLER = "8050008";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for BranchTypeBObj.
     *
     * @generated
     */
    public final static String BRANCH_TYPE_BOBJ = "8050010";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for AffiliationTypeBObj.
     *
     * @generated
     */
    public final static String AFFILIATION_TYPE_BOBJ = "8050063";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for MilitaryInfoBObj.
     *
     * @generated
     */
    public final static String MILITARY_INFO_BOBJ = "8050116";
}


