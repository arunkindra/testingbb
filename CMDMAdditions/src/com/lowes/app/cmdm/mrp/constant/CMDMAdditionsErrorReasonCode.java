/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[6a3eb30e544ab52a06725bca8eeada83]
 */
package com.lowes.app.cmdm.mrp.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the error message codes used in this module.
 *
 * @generated
 */
public class CMDMAdditionsErrorReasonCode {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Branch
     *
     * @generated
     */
    public final static String BRANCH_BRANCH_NULL = "8050050";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Name
     *
     * @generated
     */
    public final static String BRANCH_NAME_NULL = "8050058";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Affiliation
     *
     * @generated
     */
    public final static String AFFILIATION_AFFILIATION_NULL = "8050103";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Name
     *
     * @generated
     */
    public final static String AFFILIATION_NAME_NULL = "8050111";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: MilitaryInfoPkId
     *
     * @generated
     */
    public final static String MILITARYINFO_MILITARYINFOPKID_NULL = "8050145";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: PartyId
     *
     * @generated
     */
    public final static String MILITARYINFO_PARTYID_NULL = "8050212";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Branch is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_BRANCH = "8050221";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Branch
     *
     * @generated
     */
    public final static String MILITARYINFO_BRANCH_NULL = "8050225";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Affiliation is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_AFFILIATION = "8050234";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * ServiceBeginDate is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_SERVICEBEGINDATE = "8050252";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * ServiceEndDate is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_SERVICEENDDATE = "8050264";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: StartDate
     *
     * @generated
     */
    public final static String MILITARYINFO_STARTDATE_NULL = "8050270";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * StartDate is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_STARTDATE = "8050276";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * EndDate is not correct
     *
     * @generated
     */
    public final static String INVALID_MILITARYINFO_ENDDATE = "8050288";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of MilitaryInfo is empty.
     *
     * @generated
     */
    public final static String MILITARYINFO_BEFORE_IMAGE_NOT_POPULATED = "8050149";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the MilitaryInfo failed.
     *
     * @generated
     */
    public final static String GETMILITARYINFO_FAILED = "8050166";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETMILITARYINFO_ID_NULL = "8050170";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETMILITARYINFO_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "8050174";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the MilitaryInfo failed.
     *
     * @generated
     */
    public final static String GETALLMILITARYINFOBYPARTYID_FAILED = "8050313";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETALLMILITARYINFOBYPARTYID_ID_NULL = "8050317";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETALLMILITARYINFOBYPARTYID_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "8050321";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * MilitaryInfo insert failed.
     *
     * @generated
     */
    public final static String ADDMILITARYINFO_FAILED = "8050189";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Duplicate primary key already exists.
     *
     * @generated
     */
    public final static String DUPLICATE_PRIMARY_KEY_MILITARYINFO = "8050193";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * MilitaryInfo update failed.
     *
     * @generated
     */
    public final static String UPDATEMILITARYINFO_FAILED = "8050206";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of MilitaryInfo is empty.
     *
     * @generated NOT
     */
    public final static String STARTDATE_GREATERTHAN_ENDDATE = "1000120006";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of MilitaryInfo is empty.
     *
     * @generated NOT
     */
    public final static String DELETEMILITARYINFO_FAILED = "8059101";

}


