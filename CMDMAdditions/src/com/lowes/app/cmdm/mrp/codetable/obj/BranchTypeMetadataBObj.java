
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[7c31bbdf9d3848fb66c4aec80cfffedc]
 */

package com.lowes.app.cmdm.mrp.codetable.obj;

import com.ibm.mdm.common.codetype.obj.CodeTypeMetadataBaseBObj;
import com.ibm.mdm.common.codetype.obj.CodeTypeColumnMetadataBObj;
/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
@SuppressWarnings("serial")
public class BranchTypeMetadataBObj  extends CodeTypeMetadataBaseBObj {
	
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public BranchTypeMetadataBObj() {
          super("T416_mty_lct_typ", BranchTypeBObj.class);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("lang_tp_cd", java.lang.Long.class, false,  null , 
      		CodeTypeColumnMetadataBObj.LANGUAGE)	
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("t416_mty_lct_cd", java.lang.Long.class, false,  null , 
      		CodeTypeColumnMetadataBObj.CODE)	    	
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("name", java.lang.String.class, false,  null , 
      		CodeTypeColumnMetadataBObj.VALUE)	    			    		
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("description", java.lang.String.class, true,  null , 
      		CodeTypeColumnMetadataBObj.DESCRIPTION)	
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("expiry_dt", java.sql.Timestamp.class, true,  null , 
      		CodeTypeColumnMetadataBObj.EXPIRY_DATE)
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("last_update_dt", java.sql.Timestamp.class, false,  null , 
      		CodeTypeColumnMetadataBObj.LAST_UPDATE_DATE)		
     	);
     	setCodeTypeColumnMetadataBObj(new CodeTypeColumnMetadataBObj("last_update_user", java.lang.String.class, true,  null , 
      		CodeTypeColumnMetadataBObj.LAST_UPDATE_USER)	
     	);
   }
	 

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
	 @Override
	 public String getCategory() {
        return C3;
   }
}

