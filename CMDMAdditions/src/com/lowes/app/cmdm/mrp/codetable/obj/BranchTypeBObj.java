
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[4e9b5c06c5cd9e6154c1c216bd6471a0]
 */

package com.lowes.app.cmdm.mrp.codetable.obj;

import com.dwl.base.exception.DWLBaseException;

import com.ibm.mdm.common.codetype.obj.NLSCodeTypeAdminBObj;



/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
@SuppressWarnings("serial")
public class BranchTypeBObj extends  NLSCodeTypeAdminBObj  {

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   *
   * @generated
   */
	public BranchTypeBObj() throws DWLBaseException{
        super();
        init();
   }
	
	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   *
   * @generated
   */
	 @Override
	 protected String getCodeTypeName() {
        return "T416_mty_lct_typ";
   }
	 
	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   *
   * @generated
   */
	 private void init() {
   	metaDataMap.put("branch", null);	  
   	metaDataMap.put("name", null);	  
   }	 
	 
	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   *
   * @generated
   */
	 public void refreshMap() {
        if(this.bRequireMapRefresh) {
             super.refreshMap();
       	metaDataMap.put("branch", getbranch());
       	metaDataMap.put("name", getname());
              bRequireMapRefresh = false;
        }
   }
	   
	 
	    	       	 	
	    	/**
      	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
         *
         * @generated
      	 */
	    	public String getbranch (){
      		return gettp_cd();
      	}
	    	       	 	
		    /**
      	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
         *
         * @generated
      	 */
	    	public void setbranch (String branch){	    	   
      		metaDataMap.put("branch", branch);
        	if (branch != null && "".equals(branch.trim())) branch = null;	
  
      		super.settp_cd(branch);
      	}
	    	/**
      	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
         *
         * @generated
      	 */
	    	public String getname (){
      		return getvalue();
      	}
	    	       	 	
	        /**
      	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
         *
         * @generated
      	 */
	    	public void setname (String name){	
      		metaDataMap.put("name", name);
        	if (name != null && "".equals(name.trim())) name = null;		
  
      		super.setvalue(name);
      	}
	    	       	 	
	    	       	 	
	    	       	 	
	    	       	 	
}



