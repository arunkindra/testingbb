
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- MDM_TODO2: CDKWB0055I Replace <DBNAME> with the database name.
-- MDM_TODO2: CDKWB0058I Replace <TABLESPACENAME> with the table space name.
-- MDM_TODO2: CDKWB0056I Replace <H_DBNAME> with the history database name.
-- MDM_TODO2: CDKWB0057I Replace <H_TABLESPACENAME> with the history table space name.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CMDMAdditions_SETUP_ZOS.sql
-- 			db2 -vf CMDMAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CMDMAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CMDMAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CMDMAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CMDMAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;


CREATE TABLE DB2ADMIN.T416_MTY_LCT_TYP (  
	lang_tp_cd DECIMAL(19)  NOT NULL  , 
	T416_MTY_LCT_CD DECIMAL(19)  NOT NULL  , 
	Name VARCHAR(120)  NOT NULL  , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT, 
	last_update_user VARCHAR(20)   , 
	CONSTRAINT P_T416_MTY_LCT_TYP PRIMARY KEY (
	 lang_tp_cd, 
	 T416_MTY_LCT_CD
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_T416_MTY_LCT_TYP
  ON DB2ADMIN.T416_MTY_LCT_TYP (
	 lang_tp_cd, 
	 T416_MTY_LCT_CD
  );


CREATE TABLE DB2ADMIN.T395_MTY_AFL_STS_TYP (  
	lang_tp_cd DECIMAL(19)  NOT NULL  , 
	T395_MTY_AFL_STS_CD DECIMAL(19)  NOT NULL  , 
	Name VARCHAR(120)  NOT NULL  , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT, 
	last_update_user VARCHAR(20)   , 
	CONSTRAINT P_T395_MTY_AFL_STS_TYP PRIMARY KEY (
	 lang_tp_cd, 
	 T395_MTY_AFL_STS_CD
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_T395_MTY_AFL_STS_TYP
  ON DB2ADMIN.T395_MTY_AFL_STS_TYP (
	 lang_tp_cd, 
	 T395_MTY_AFL_STS_CD
  );


CREATE TABLE DB2ADMIN.T417_MTY_CUS_AFL (  
	T417_MTY_CUS_AFL_ID DECIMAL(19)  NOT NULL  , 
	CONT_ID DECIMAL(19)  NOT NULL  , 
	T416_MTY_LCT_CD DECIMAL(19)  NOT NULL  , 
	T395_MTY_AFL_STS_CD DECIMAL(19)   , 
	SRV_BGN_DT TIMESTAMP   , 
	SRV_END_DT TIMESTAMP   , 
	START_DT TIMESTAMP  NOT NULL  , 
	END_DT TIMESTAMP   , 
	LAST_VRF_BY_ID VARCHAR(20)   , 
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT, 
	LAST_UPDATE_TX_ID DECIMAL(19)   , 
	LAST_UPDATE_USER VARCHAR(20)   , 
	CONSTRAINT P_T417_MTY_CUS_AFL PRIMARY KEY (
	 T417_MTY_CUS_AFL_ID
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_T417_MTY_CUS_AFL
  ON DB2ADMIN.T417_MTY_CUS_AFL (
	 T417_MTY_CUS_AFL_ID
  );


CREATE TABLE DB2ADMIN.H_T416_MTY_LCT_TYP (
	h_lang_tp_cd DECIMAL(19)  NOT NULL  ,
	h_T416_MTY_LCT_CD DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd DECIMAL(19)  NOT NULL   ,
	T416_MTY_LCT_CD DECIMAL(19)  NOT NULL   ,
	Name VARCHAR(120)  NOT NULL   ,
	description VARCHAR(250)    ,
	expiry_dt TIMESTAMP    ,
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT ,
	last_update_user VARCHAR(20)    ,
	CONSTRAINT P_H_T416_MTY_LCT_TYP PRIMARY KEY (
	 h_lang_tp_cd,
	 h_T416_MTY_LCT_CD,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_T416_MTY_LCT_TYP
  ON DB2ADMIN.H_T416_MTY_LCT_TYP
  (
	 h_lang_tp_cd,
	 h_T416_MTY_LCT_CD,
	 h_create_dt
  );

CREATE TABLE DB2ADMIN.H_T395_MTY_AFL_STS_TYP (
	h_lang_tp_cd DECIMAL(19)  NOT NULL  ,
	h_T395_MTY_AFL_STS_CD DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd DECIMAL(19)  NOT NULL   ,
	T395_MTY_AFL_STS_CD DECIMAL(19)  NOT NULL   ,
	Name VARCHAR(120)  NOT NULL   ,
	description VARCHAR(250)    ,
	expiry_dt TIMESTAMP    ,
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT ,
	last_update_user VARCHAR(20)    ,
	CONSTRAINT P_H_T395_MTY_AFL_STS_TYP PRIMARY KEY (
	 h_lang_tp_cd,
	 h_T395_MTY_AFL_STS_CD,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_T395_MTY_AFL_STS_TYP
  ON DB2ADMIN.H_T395_MTY_AFL_STS_TYP
  (
	 h_lang_tp_cd,
	 h_T395_MTY_AFL_STS_CD,
	 h_create_dt
  );

CREATE TABLE DB2ADMIN.H_T417_MTY_CUS_AFL (
	h_T417_MTY_CUS_AFL_ID DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	T417_MTY_CUS_AFL_ID DECIMAL(19)  NOT NULL   ,
	CONT_ID DECIMAL(19)  NOT NULL   ,
	T416_MTY_LCT_CD DECIMAL(19)  NOT NULL   ,
	T395_MTY_AFL_STS_CD DECIMAL(19)    ,
	SRV_BGN_DT TIMESTAMP    ,
	SRV_END_DT TIMESTAMP    ,
	START_DT TIMESTAMP  NOT NULL   ,
	END_DT TIMESTAMP    ,
	LAST_VRF_BY_ID VARCHAR(20)    ,
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT ,
	LAST_UPDATE_TX_ID DECIMAL(19)    ,
	LAST_UPDATE_USER VARCHAR(20)    ,
	CONSTRAINT P_H_T417_MTY_CUS_AFL PRIMARY KEY (
	 h_T417_MTY_CUS_AFL_ID,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_T417_MTY_CUS_AFL
  ON DB2ADMIN.H_T417_MTY_CUS_AFL
  (
	 h_T417_MTY_CUS_AFL_ID,
	 h_create_dt
  );


