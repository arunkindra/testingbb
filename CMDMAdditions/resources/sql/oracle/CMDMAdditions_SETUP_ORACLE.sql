
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			sqlplus userid/password@host @CMDMAdditions_SETUP_ORACLE.sql
-- 			sqlplus userid/password@host @CMDMAdditions_TRIGGERS_ORACLE.sql
-- 			sqlplus userid/password@host @CMDMAdditions_CONSTRAINTS_ORACLE.sql
--			sqlplus userid/password@host @CMDMAdditions_ERRORS_100_ORACLE.sql
-- 			sqlplus userid/password@host @CMDMAdditions_MetaData_ORACLE.sql
-- 			sqlplus userid/password@host CONFIG_XMLSERVICES_RESPONSE_ORACLE.sql
-- 			sqlplus userid/password@host @CMDMAdditions_CODETABLES_ORACLE.sql
  
CREATE TABLE DB2ADMIN.T416_MTY_LCT_TYP (
	lang_tp_cd NUMBER(19, 0)   NOT NULL  , 
	T416_MTY_LCT_CD NUMBER(19, 0)   NOT NULL  , 
	Name VARCHAR2(120)   NOT NULL  , 
	description VARCHAR2(250)    , 
	expiry_dt TIMESTAMP    , 
	last_update_dt TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL  , 
	last_update_user VARCHAR2(20)    
  );

ALTER TABLE DB2ADMIN.T416_MTY_LCT_TYP
  ADD PRIMARY KEY (
	lang_tp_cd, 
	T416_MTY_LCT_CD
  );

  
CREATE TABLE DB2ADMIN.T395_MTY_AFL_STS_TYP (
	lang_tp_cd NUMBER(19, 0)   NOT NULL  , 
	T395_MTY_AFL_STS_CD NUMBER(19, 0)   NOT NULL  , 
	Name VARCHAR2(120)   NOT NULL  , 
	description VARCHAR2(250)    , 
	expiry_dt TIMESTAMP    , 
	last_update_dt TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL  , 
	last_update_user VARCHAR2(20)    
  );

ALTER TABLE DB2ADMIN.T395_MTY_AFL_STS_TYP
  ADD PRIMARY KEY (
	lang_tp_cd, 
	T395_MTY_AFL_STS_CD
  );

  
CREATE TABLE DB2ADMIN.T417_MTY_CUS_AFL (
	T417_MTY_CUS_AFL_ID NUMBER(19, 0)   NOT NULL  , 
	CONT_ID NUMBER(19, 0)   NOT NULL  , 
	T416_MTY_LCT_CD NUMBER(19, 0)   NOT NULL  , 
	T395_MTY_AFL_STS_CD NUMBER(19, 0)    , 
	SRV_BGN_DT TIMESTAMP    , 
	SRV_END_DT TIMESTAMP    , 
	START_DT TIMESTAMP   NOT NULL  , 
	END_DT TIMESTAMP    , 
	LAST_VRF_BY_ID VARCHAR2(20)    , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL  , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)    , 
	LAST_UPDATE_USER VARCHAR2(20)    
  );

ALTER TABLE DB2ADMIN.T417_MTY_CUS_AFL
  ADD PRIMARY KEY (
	T417_MTY_CUS_AFL_ID
  );

  
CREATE TABLE DB2ADMIN.H_T416_MTY_LCT_TYP (
	h_lang_tp_cd NUMBER(19, 0)   NOT NULL,
	h_T416_MTY_LCT_CD NUMBER(19, 0)   NOT NULL,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR2(20)    NOT NULL,
	h_create_dt                                      TIMESTAMP 	     DEFAULT LOCALTIMESTAMP NOT NULL,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd NUMBER(19, 0)   NOT NULL , 
	T416_MTY_LCT_CD NUMBER(19, 0)   NOT NULL , 
	Name VARCHAR2(120)   NOT NULL , 
	description VARCHAR2(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL , 
	last_update_user VARCHAR2(20)   
  );

ALTER TABLE DB2ADMIN.H_T416_MTY_LCT_TYP
  ADD PRIMARY KEY (
	h_lang_tp_cd,
	h_T416_MTY_LCT_CD,
   	h_create_dt    
 );

  
CREATE TABLE DB2ADMIN.H_T395_MTY_AFL_STS_TYP (
	h_lang_tp_cd NUMBER(19, 0)   NOT NULL,
	h_T395_MTY_AFL_STS_CD NUMBER(19, 0)   NOT NULL,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR2(20)    NOT NULL,
	h_create_dt                                      TIMESTAMP 	     DEFAULT LOCALTIMESTAMP NOT NULL,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd NUMBER(19, 0)   NOT NULL , 
	T395_MTY_AFL_STS_CD NUMBER(19, 0)   NOT NULL , 
	Name VARCHAR2(120)   NOT NULL , 
	description VARCHAR2(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL , 
	last_update_user VARCHAR2(20)   
  );

ALTER TABLE DB2ADMIN.H_T395_MTY_AFL_STS_TYP
  ADD PRIMARY KEY (
	h_lang_tp_cd,
	h_T395_MTY_AFL_STS_CD,
   	h_create_dt    
 );

  
CREATE TABLE DB2ADMIN.H_T417_MTY_CUS_AFL (
	h_T417_MTY_CUS_AFL_ID NUMBER(19, 0)   NOT NULL,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR2(20)    NOT NULL,
	h_create_dt                                      TIMESTAMP 	     DEFAULT LOCALTIMESTAMP NOT NULL,
	h_end_dt                                         TIMESTAMP,
	T417_MTY_CUS_AFL_ID NUMBER(19, 0)   NOT NULL , 
	CONT_ID NUMBER(19, 0)   NOT NULL , 
	T416_MTY_LCT_CD NUMBER(19, 0)   NOT NULL , 
	T395_MTY_AFL_STS_CD NUMBER(19, 0)   , 
	SRV_BGN_DT TIMESTAMP   , 
	SRV_END_DT TIMESTAMP   , 
	START_DT TIMESTAMP   NOT NULL , 
	END_DT TIMESTAMP   , 
	LAST_VRF_BY_ID VARCHAR2(20)   , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)   , 
	LAST_UPDATE_USER VARCHAR2(20)   
  );

ALTER TABLE DB2ADMIN.H_T417_MTY_CUS_AFL
  ADD PRIMARY KEY (
	h_T417_MTY_CUS_AFL_ID,
   	h_create_dt    
 );

