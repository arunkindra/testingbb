
-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CMDMAdditions_SETUP_DB2.sql
-- 			db2 -vf CMDMAdditions_TRIGGERS_DB2.sql
-- 			db2 -vf CMDMAdditions_CONSTRAINTS_DB2.sql
--			db2 -vf CMDMAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CMDMAdditions_MetaData_DB2.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_DB2.sql
-- 			db2 -vf CMDMAdditions_CODETABLES_DB2.sql
	
--#SET TERMINATOR ;


-- For locale: 100 / default
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050050, 'The following is required: Branch', 'The following is required: Branch', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050058, 'The following is required: Name', 'The following is required: Name', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050103, 'The following is required: Affiliation', 'The following is required: Affiliation', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050111, 'The following is required: Name', 'The following is required: Name', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050145, 'The following is required: MilitaryInfoPkId', 'The following is required: MilitaryInfoPkId', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050149, 'The before image for the following is empty: MilitaryInfo.', 'The before image of MilitaryInfo is empty.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050166, 'Read of the following record failed: MilitaryInfo.', 'An attempt to read the MilitaryInfo failed.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050170, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050174, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050189, 'Insert of the following failed: MilitaryInfo.', 'MilitaryInfo insert failed.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050193, 'Duplicate primary key already exists.', 'Duplicate primary key already exists.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050206, 'Update of the following failed: MilitaryInfo.', 'MilitaryInfo update failed.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050212, 'The following is required: PartyId', 'The following is required: PartyId', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050221, 'The following is not correct: Branch', 'Branch is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050225, 'The following is required: Branch', 'The following is required: Branch', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050234, 'The following is not correct: Affiliation', 'Affiliation is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050252, 'The following is not correct: ServiceBeginDate', 'ServiceBeginDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050264, 'The following is not correct: ServiceEndDate', 'ServiceEndDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050270, 'The following is required: StartDate', 'The following is required: StartDate', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050276, 'The following is not correct: StartDate', 'StartDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050288, 'The following is not correct: EndDate', 'EndDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050313, 'Read of the following record failed: MilitaryInfo.', 'An attempt to read the MilitaryInfo failed.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050317, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO DB2ADMIN.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 8050321, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
