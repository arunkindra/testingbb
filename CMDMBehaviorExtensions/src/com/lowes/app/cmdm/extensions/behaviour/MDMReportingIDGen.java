/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.lowes.app.cmdm.extensions.behaviour;

import java.sql.Timestamp;
import java.util.Random;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLIDFactory;
import com.lowes.app.cmdm.dataextensions.component.XPartyAdminSysKeyBObjExt;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This is a Java rule that extends the behavior of WCC. @see
 * com.dwl.base.extensionFramework.ClientJavaExtensionSet
 * 
 * MDM Reporting Identifier for the purpose of sync with CDW database
 * 
 * @generated
 */
public class MDMReportingIDGen extends ClientJavaExtensionSet {
	private static final IDWLLogger LOGGER = DWLLoggerManager.getLogger(GenerateCusSysIdNew.class);
	public static final String MDM_REPORTING_SOURCE_CODE = "303";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
    public void execute(ExtensionParameters params) {
// MDM_TODO: Write customized business logic for the extension here.
    
    	LOGGER.info("ENTER: MDMReportingIDGen.execute()");
    	
    	    	
    	if(params.getTransactionObjectHierarchy() instanceof TCRMPartyBObj) { 
    		TCRMPartyBObj partyBObj = (TCRMPartyBObj) params.getTransactionObjectHierarchy();
    		DWLControl dwlControl = partyBObj.getControl();
    		String partyID = partyBObj.getPartyId();
    			try {
    				
    				
    				TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
    				Vector<TCRMAdminContEquivBObj> dbAdminContEquivBObjs = partyComponent.getAllPartyAdminSysKeys(partyID, dwlControl);
    				/**Check if there is any AdminCOntEquiv objects are available in DB for this party. In some cases where incoming AdminClientId is already
    				used but no incoming name matches with the existing party then MaintainParty determines it to be add. But before calling addParty,
    				it drops all the incoming AdminContEquiv objects from the request. In this case the party will be added without any AdminContEquiv.
    				*/
    				
    				if(dbAdminContEquivBObjs == null || dbAdminContEquivBObjs.size() == 0) {
    					LOGGER.info("MDMReportingIDGen: No AdminContEquiv found for party: " + partyID);
    					/**Here simply generate a new MDM Reporting ID and return.*/
    					generateNewMDMReportingId(partyBObj); /**This will also include the newly generated MDM Reporting ID in the response*/ 
    					return;
    				}
    				
    				/***
    				 * An admin cont equiv object was found in the request. Now is the time to check if an MRID exists for the party. 
    				 * If MRID exists, simply return as no need to generate a new MRID. 
    				 * 
    				 * If MRID does not exist, generate a new MRID for the party.
    				 */
    				
    				for(TCRMAdminContEquivBObj dbAdminContEquivBObj : dbAdminContEquivBObjs) {
    					String adminSysType = dbAdminContEquivBObj.getAdminSystemType();
    					if(MDM_REPORTING_SOURCE_CODE.equals(adminSysType)) { 
    					
    						LOGGER.info("MDMReportingIDGen: Party contains a MRID, no new MRID will be generated");	
    						return;
    					} 
    					
    				}
    				LOGGER.info("MDMReportingIDGen: Before generateNewMDMReportingId(partyBObj): Party does not contains a MRID, a new MRID will be generated");
    				generateNewMDMReportingId(partyBObj);
    				LOGGER.info("MDMReportingIDGen: After generateNewMDMReportingId(partyBObj): Party does not contains a MRID, a new MRID must have been generated");

    				
    				
    			}
    			 catch (Exception ex) {
    					LOGGER.error("MDMReportingIDGen: Exception occured: " + ex.getMessage());
    				}
    	}
    	LOGGER.info("EXIT: MDMReportingIDGen.execute()");
    	
    }
    
    
    private static TCRMAdminContEquivBObj generateNewMDMReportingId(TCRMPartyBObj partyBObj) throws Exception {
    	LOGGER.info("MDMReportingIDGen: ENTER generateNewMDMReportingId");
    	XPartyAdminSysKeyBObjExt newAdminContEquivBObj = (XPartyAdminSysKeyBObjExt) TCRMClassFactory.createBObj(XPartyAdminSysKeyBObjExt.class);
		newAdminContEquivBObj.setAdminPartyId(generateNewMDMReportingId());
		newAdminContEquivBObj.setDescription("MDM Reporting ID generated by MDM extension");
		newAdminContEquivBObj.setAdminSystemType(MDM_REPORTING_SOURCE_CODE);
		newAdminContEquivBObj.setControl(partyBObj.getControl());
		newAdminContEquivBObj.setPartyId(partyBObj.getPartyId());
		newAdminContEquivBObj.setContEquivLastUpdateUser(partyBObj.getPartyLastUpdateUser());
		newAdminContEquivBObj.setXReferenceStartDate(new Timestamp(System.currentTimeMillis()).toString());
		//Persist the newly generated CUS_SYS_ID
		TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
		partyComponent.addPartyAdminSysKey(newAdminContEquivBObj);
		Vector<TCRMAdminContEquivBObj> reqAdminConEquivBObjs = partyBObj.getItemsTCRMAdminContEquivBObj();
		reqAdminConEquivBObjs.add(newAdminContEquivBObj);
		partyBObj.setFinalResponse(newAdminContEquivBObj);
		LOGGER.info("MDMReportingIDGen: New MDM Reporting ID generated for party: " + partyBObj.getPartyId());
		LOGGER.info("MDMReportingIDGen: EXIT generateNewMDMReportingId");
		
		
		return newAdminContEquivBObj;
	}
    
    private static String generateNewMDMReportingId() {
    	LOGGER.info("MDMReportingIDGen: ENTER generateNewMDMReportingId");
		DWLIDFactory idFactory = new DWLIDFactory(); 
		String aUniqueId = ""+idFactory.generateID(new Object()); 
		LOGGER.info("MDMReportingIDGen: New MDM Reporting ID generated:" + aUniqueId);
		LOGGER.info("MDMReportingIDGen: EXIT generateNewMDMReportingId");
		return aUniqueId;
	}
}

