/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.lowes.app.cmdm.extensions.behaviour;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAddressStandardizerManager;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartySearchBObj;
import com.dwl.tcrm.coreParty.interfaces.IAddressStandardizer;
import com.dwl.tcrm.coreParty.interfaces.IAddressStandardizerManager;
import com.dwl.tcrm.utilities.StringUtils;



/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This is a Java rule that extends the behavior of WCC. @see
 * com.dwl.base.extensionFramework.ClientJavaExtensionSet
 * 
 * Search Person by Standardized Phone Number
 * 
 * @generated
 */
public class SearchPartyByPhoneExt extends ClientJavaExtensionSet {
	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2002, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";
	
	private static final String ACTION_TYPE_PERSON = "searchPersonByContactMethod";
	private static final String ACTION_TYPE_ORG = "searchOrganizationByContactMethod";	
	private final static String PHONE_NUMBER = "1";	
	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(SearchPartyByPhoneExt.class);

	/** Party component that calls are delegated from. */
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
    public void execute(ExtensionParameters params) {    	
    	
    	try {

			if (ACTION_TYPE_PERSON.equalsIgnoreCase(params.getActionType())||(ACTION_TYPE_ORG.equalsIgnoreCase(params.getActionType()))) {
				//only process if this extension is configured for searchPersonByContactMethod action type or searchOrganizationByContactMethod action type 
				//id=108 for searchPersonByContactMethod
				
				//grab TCRMPartySearchBObj from params, TCRMPersonSearchBObj and TCRMOrganizationSearchBObj are child type of TCRMPartySearchBObj
				TCRMPartySearchBObj theTCRMPartySearchBObj = (TCRMPartySearchBObj)params.getTransactionObjectHierarchy();
				
				//standardization contact method
				TCRMContactMethodBObj theTCRMContactMethodBObj = this.convertPersonSearchtoContactMethod(theTCRMPartySearchBObj);				
				if (isPhoneNumberNonBlank(theTCRMContactMethodBObj)) {
					IAddressStandardizerManager theAddressStandardizerManager = new TCRMAddressStandardizerManager();
					IAddressStandardizer theAddressStandardizer = theAddressStandardizerManager.getAddressStandardizer();
					theTCRMContactMethodBObj = theAddressStandardizer.standardizeContactMethod(theTCRMContactMethodBObj);
					this.convertContactMethodtoPersonSearch(theTCRMContactMethodBObj, theTCRMPartySearchBObj);									
				}
				
				//set back and continue processing
				params.setTransactionObjectHierarchy(theTCRMPartySearchBObj);
				params.setSkipExecutionFlag(false);

			}
		} catch (Exception e) {
			logger.error(e);			  //log it
			throw new RuntimeException(e);//throw it and let up level handle it
		}
	}
	
	protected boolean isPhoneNumberNonBlank(TCRMContactMethodBObj theTCRMContactMethodBObj) {
		return (theTCRMContactMethodBObj !=null)
				 && ((theTCRMContactMethodBObj.getContactMethodType() != null) && (theTCRMContactMethodBObj.getContactMethodType().equals(PHONE_NUMBER)))
				 && (StringUtils.isNonBlank(theTCRMContactMethodBObj.getReferenceNumber()));
	}
	
    protected TCRMContactMethodBObj convertPersonSearchtoContactMethod(
    		TCRMPartySearchBObj theTCRMPartySearchBObj) {
    	//instantiate 
    	TCRMContactMethodBObj theTCRMContactMethodBObj = new TCRMContactMethodBObj();
		theTCRMContactMethodBObj.setControl(theTCRMPartySearchBObj.getControl());

		//copy fields
		theTCRMContactMethodBObj.setContactMethodType(theTCRMPartySearchBObj.getContactMethodType());
		theTCRMContactMethodBObj.setReferenceNumber(theTCRMPartySearchBObj.getContactMethodReferenceNumber());
		
		return theTCRMContactMethodBObj;
    }
    
    protected void convertContactMethodtoPersonSearch(TCRMContactMethodBObj theTCRMContactMethodBObj, TCRMPartySearchBObj theTCRMPartySearchBObj) {
		//copy fields
    	theTCRMPartySearchBObj.setContactMethodType(theTCRMContactMethodBObj.getContactMethodType());
    	theTCRMPartySearchBObj.setContactMethodReferenceNumber(theTCRMContactMethodBObj.getReferenceNumber());		
	}
}
