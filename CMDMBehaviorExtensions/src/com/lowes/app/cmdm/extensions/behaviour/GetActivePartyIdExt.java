package com.lowes.app.cmdm.extensions.behaviour;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.common.util.LowesGetResultBObj;
import com.lowes.app.cmdm.common.util.LowesGetResultSetProcessor;
import com.lowes.app.cmdm.common.util.LowesGetSQL;
import com.lowes.app.cmdm.dataextensions.component.XPartyAdminSysKeyBObjExt;

public class GetActivePartyIdExt extends ClientJavaExtensionSet{
	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(GetActivePartyIdExt.class);
	protected DWLControl theControl = null;
	
	/** Party component that calls are delegated from. */
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
    public void execute(ExtensionParameters params){    	
    	
    	try {
    		String[] parameters = params.getInquiryParameters();
    		theControl = params.getControl();
    		Vector theInquiryParams = new Vector();
    		QueryConnection conn = null;
    		boolean isSameInputId = false;
            
    		//Create a DB connection to execute SearchSQLs
			conn = DataManager.getInstance().getQueryConnection();
			
			for(int i=0; i<parameters.length; i++){
    			theInquiryParams.add(parameters[i]);
    		}
    		String adminSysType = theInquiryParams.elementAt(0).toString();
    		String adminClientId = theInquiryParams.elementAt(1).toString();
    		
    		String searchSQL = LowesGetSQL.SEARCH_BY_CONTEQUIV_ID;
    		Vector<LowesGetResultBObj> resultVector = executeInitialSearch(theInquiryParams, searchSQL, conn);
    		
    		if(resultVector.size()>0){
	    		String partyId = getFirstPartyId(resultVector);
	    		
	    		TCRMPartyComponent partyComponent = (TCRMPartyComponent )TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
	    		Object tCRMAdminContEquivBObj = partyComponent.getPartyAdminSysKeyByPartyId(adminSysType, partyId, theControl);
	    		
	    		if(tCRMAdminContEquivBObj instanceof TCRMAdminContEquivBObj){
					if((((TCRMAdminContEquivBObj) tCRMAdminContEquivBObj).getAdminSystemType().equals(adminSysType)) && 
							(((TCRMAdminContEquivBObj) tCRMAdminContEquivBObj).getAdminPartyId().equals(adminClientId))){
						params.setWorkingObjectHierarchy(tCRMAdminContEquivBObj);
						isSameInputId = true;
					}
				}
				else if(tCRMAdminContEquivBObj instanceof XPartyAdminSysKeyBObjExt){
					if((((XPartyAdminSysKeyBObjExt) tCRMAdminContEquivBObj).getAdminSystemType().equals(adminSysType)) &&
							(((XPartyAdminSysKeyBObjExt) tCRMAdminContEquivBObj).getAdminPartyId().equals(adminClientId))){
						params.setWorkingObjectHierarchy(tCRMAdminContEquivBObj);
						isSameInputId = true;
					}
				}
	    		if (!isSameInputId){
		    		partyComponent.getPartyAdminSysKeyByPartyId(adminSysType, partyId, theControl);
		    		Vector<Object> addupdateAdminContEquivs = partyComponent.getAllPartyAdminSysKeys(partyId, theControl);
		    		
		    		for(Object addupdateAdminContEquiv:addupdateAdminContEquivs){
						if(addupdateAdminContEquiv instanceof TCRMAdminContEquivBObj){
							if((((TCRMAdminContEquivBObj) addupdateAdminContEquiv).getAdminSystemType().equals(adminSysType)) && 
									(((TCRMAdminContEquivBObj) addupdateAdminContEquiv).getAdminPartyId().equals(adminClientId))){
								params.setWorkingObjectHierarchy(addupdateAdminContEquiv);
								break;
							}
						}
						else if(addupdateAdminContEquiv instanceof XPartyAdminSysKeyBObjExt){
							if((((XPartyAdminSysKeyBObjExt) addupdateAdminContEquiv).getAdminSystemType().equals(adminSysType)) &&
									(((XPartyAdminSysKeyBObjExt) addupdateAdminContEquiv).getAdminPartyId().equals(adminClientId))){
								params.setWorkingObjectHierarchy(addupdateAdminContEquiv);
								break;
							}
						}
		    		}
	    		}
	    		//params.setWorkingObjectHierarchy(tcrmPartyBObj);
    		}
    		params.setSkipExecutionFlag(true);
    	}catch (Exception e) {
			logger.error(e);			  //log it
			throw new RuntimeException(e);//throw it and let up level handle it
		}
	}
    
    /**
     * It retrieves the active partyId's for given Admin Party Id. 
     * @param request
     * @param searchSQL
     * @param conn
     * @return Vector<LowesGetResultBObj>
     * @throws Exception
     */
    private Vector<LowesGetResultBObj> executeInitialSearch(Vector request, String searchSQL, QueryConnection conn) throws Exception {
    
    	String adminSystemType = request.elementAt(0).toString();
		String adminClientNum = request.elementAt(1).toString();
		int adminSysType = Integer.parseInt(adminSystemType);
		Vector<LowesGetResultBObj> result = new Vector<LowesGetResultBObj>();
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		ResultSet rs = null;
		
		Object[] parameters = new Object[2];
        parameters[0] = adminSysType;
        parameters[1] = adminClientNum;
        
        rs = conn.queryResults(searchSQL, parameters);
        LowesGetResultSetProcessor processor = new LowesGetResultSetProcessor();
	    result = processor.getObjectFromResultSet(rs);
        
     	return result;
    }
    
    /**
     * It retrieves first Party Id
     * @param resultVector
     * @return String
     * @throws Exception
     */
    private String getFirstPartyId(Vector<LowesGetResultBObj> resultVector) throws Exception {
		
    	 LowesGetResultBObj lowesGetResultBObj =(LowesGetResultBObj)resultVector.elementAt(0);
    	 String partyId = lowesGetResultBObj.getPartyId();
    	 
    	return partyId;
    }
}
