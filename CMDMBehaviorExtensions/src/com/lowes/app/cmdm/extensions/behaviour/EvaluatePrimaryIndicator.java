package com.lowes.app.cmdm.extensions.behaviour;

import java.util.Vector;

import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.exception.TCRMException;
import com.lowes.app.cmdm.common.util.MDMCompositeUtil;
//**********************************************************************
//Property of Lowe's Companies, Inc.
//**********************************************************************
/** 
 * @author	InfoTrellis Inc.
 * [ITLS-26-08-2015-AB]-Behaviour extension class for generating preferred indicator on CUS_SYS_ID
 **/


public class EvaluatePrimaryIndicator extends ClientJavaExtensionSet  {
     
	private static final IDWLLogger logger = DWLLoggerManager.getLogger(EvaluatePrimaryIndicator.class);
	
	@Override
	public void execute(ExtensionParameters params) { 
		Vector<TCRMAdminContEquivBObj> adminContEquivList = null;
		Object topLvlObj = params.getWorkingObjectHierarchy();
		try {
		if (topLvlObj instanceof TCRMPersonBObj){
			logger.fine("Top level object is an instance of TCRMPartyListBObj");
			adminContEquivList =  ((TCRMPersonBObj)topLvlObj).getItemsTCRMAdminContEquivBObj();
			MDMCompositeUtil.setCusKeyPrefInd(adminContEquivList);
		}  else if (topLvlObj instanceof TCRMOrganizationBObj){
			logger.fine("Top level object is an instance of TCRMOrganizationBObj");
			adminContEquivList =  ((TCRMOrganizationBObj)topLvlObj).getItemsTCRMAdminContEquivBObj();
			MDMCompositeUtil.setCusKeyPrefInd(adminContEquivList);
		}  else if (topLvlObj == null) {
			logger.info("Top level object is NULL for LowesCusSysIdPreferredIndicator");
		}
		} catch (TCRMException ex){
			logger.error("Problem with CUS_SYS_ID Preferred Indicator Behaviour Extension :" + ex.getLocalizedMessage());
			
		}
	}
		
}


