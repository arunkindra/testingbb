/**
 * 
 */
package com.lowes.app.cmdm.extensions.behaviour;

import java.util.Vector;

import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLDataInvalidException;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.common.util.LowesOrgNamePI;
import com.lowes.app.cmdm.common.util.LowesPersonNamePI;
import com.lowes.app.cmdm.common.constants.SVoCGoldAttributesComponentID;
import com.lowes.app.cmdm.common.constants.SVoCGoldAttributesErrorReasonCode;
import com.lowes.app.cmdm.common.util.RefreshPartyPreferredIndicators;
import com.lowes.app.cmdm.dataextensions.component.XOrganizationNameBObjExt;
import com.lowes.app.cmdm.dataextensions.component.XPersonNameBObjExt;

//**********************************************************************
//Property of Lowe's Companies, Inc.
//**********************************************************************
/**
* @author	InfoTrellis Inc.
* The class LowesPrimaryIndicator is a behaviour extension to maintainParty transaction. It sets
* PI based on Business 
*/
public class LowesPrimaryIndicator extends ClientJavaExtensionSet {

	// Static varible to be used for Yes Indicators
	private static final String YES_INDICATOR = "Y";

	// Static varible to be used for No indicators
	private static final String NO_INDICATOR = "N";
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dwl.base.extensionFramework.ClientJavaExtensionSet#execute(com.dwl
	 * .base.extensionFramework.ExtensionParameters)
	 */
	@Override
	public void execute(ExtensionParameters params) {

		// We retrieve the Party Object from the parameters passed into the
		// behavior extension
		TCRMPartyBObj theParty = (TCRMPartyBObj) params
				.getTransactionObjectHierarchy();

		try {
			TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory
					.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
			RefreshPartyPreferredIndicators rpi = new RefreshPartyPreferredIndicators();
			// Determining the PartyType for special Person and Organization
			// processing
			if (theParty instanceof TCRMPersonBObj) {
				// If the party is a person cast it and continue with person
				// preferred processing
				TCRMPersonBObj thePerson = (TCRMPersonBObj) theParty;
				
				LowesPersonNamePI lowesPersonNamePI = new LowesPersonNamePI();
				

				if (thePerson.getItemsTCRMPersonNameBObj().size() > 0) {

					// If the person contains personName objects then we will
					// need to ensure that the preferred status is correct for
					// this persons names
					// To do this we retrieve all of this person's active names
					Vector<XPersonNameBObjExt> personNames = partyComponent
							.getAllPersonNames(theParty.getPartyId(),
									TCRMRecordFilter.ACTIVE, theParty
											.getControl());

					// We then pass this vector of personNames to a method
					// for processing it returns the preferred personName
					XPersonNameBObjExt preferredPersonName = lowesPersonNamePI.handlePersonNameRefresh(personNames);

					// We now need to know if the response object will need to
					// be altered to convey the preferred indicator change
					if(preferredPersonName != null) {
						for (int idx = 0; idx < thePerson
								.getItemsTCRMPersonNameBObj().size(); idx++) {
							TCRMPersonNameBObj personName = (TCRMPersonNameBObj) thePerson
									.getItemsTCRMPersonNameBObj().elementAt(idx);
	
							// For each person name check to see if the IDPk matches
							if (personName.getPersonNameIdPK().equals(
									preferredPersonName.getPersonNameIdPK())) {
	
								// if they match then replace the response with the
								// preferred
								preferredPersonName.setStatus(personName
										.getStatus());
								thePerson.getItemsTCRMPersonNameBObj().set(idx,
										preferredPersonName);
							} else {
	
								// if they do not match then we need to check to see
								// if it is marked as preferred in the response if
								// it is
								// then we need to reset the indicator on this
								// object to 'N' as it is no longer the preferred
								// Added for NULL Indicator issue
								if (personName instanceof XPersonNameBObjExt) {
									XPersonNameBObjExt nonPrefPersonName = (XPersonNameBObjExt) personName;
									if (nonPrefPersonName.getPreferredIndicator() == null
											|| nonPrefPersonName
													.getPreferredIndicator()
													.equalsIgnoreCase(YES_INDICATOR)) {
										nonPrefPersonName
												.setPreferredIndicator(NO_INDICATOR);
										nonPrefPersonName.setStatus(personName
												.getStatus());
										thePerson.getItemsTCRMPersonNameBObj().set(
												idx, nonPrefPersonName);
									}
								}
							}
						}
					}
				}
				
			}
			if (theParty instanceof TCRMOrganizationBObj) {
				// If the party is a organization cast it and continue with
				// person preferred processing
				TCRMOrganizationBObj theOrg = (TCRMOrganizationBObj) theParty;
				LowesOrgNamePI lowesOrgNamePI = new LowesOrgNamePI();
				
				if (theOrg.getItemsTCRMOrganizationNameBObj().size() > 0) {
					// If the org contains orgName objects then we will need to
					// ensure that the preferred status is correct for this
					// organization names
					// To do this we retrieve all of this organization's active
					// names
					Vector<XOrganizationNameBObjExt> orgNames = partyComponent
							.getAllOrganizationNames(theParty.getPartyId(),
									TCRMRecordFilter.ACTIVE, theParty
											.getControl());

					// We then pass this vector of orgNames to a private method
					// for processing it returns the preferred orgName
					XOrganizationNameBObjExt preferredOrgName = lowesOrgNamePI.handleOrgNameRefresh(orgNames);

					if(preferredOrgName != null) {
					// We now need to know if the response object will need to
					// be altered to convey the preferred indicator change
						for (int idx = 0; idx < theOrg
								.getItemsTCRMOrganizationNameBObj().size(); idx++) {
	
							TCRMOrganizationNameBObj orgName = (TCRMOrganizationNameBObj) theOrg
									.getItemsTCRMOrganizationNameBObj().elementAt(
											idx);
							// For each Org name check to see if the IDPk matches
							if (orgName.getOrganizationNameIdPK().equals(
									preferredOrgName.getOrganizationNameIdPK())) {
	
								// If they do match then replace response with the
								// preferred
								preferredOrgName.setStatus(orgName.getStatus());
	
								theOrg.getItemsTCRMOrganizationNameBObj().set(idx,
										preferredOrgName);
	
							} else {
								// if they do not match then we need to check to see
								// if it is marked as preferred in the response if
								// it is
								// then we need to reset the indicator on this
								// object to 'N' as it is no longer the preferred
								// Added for NULL Indicator issue
								if (orgName instanceof XOrganizationNameBObjExt) {
									XOrganizationNameBObjExt nonPreferredOrgName = (XOrganizationNameBObjExt) orgName;
									if (nonPreferredOrgName.getPreferredIndicator() == null
											|| nonPreferredOrgName
													.getPreferredIndicator()
													.equalsIgnoreCase(YES_INDICATOR)) {
										nonPreferredOrgName
												.setPreferredIndicator(NO_INDICATOR);
										nonPreferredOrgName.setStatus(orgName
												.getStatus());
										theOrg.getItemsTCRMOrganizationNameBObj()
												.set(idx, nonPreferredOrgName);
									}
								}
							}
						}
					}
				}
			}
			rpi.lowesAddressAndCMPI(params);
		} catch (DWLDataInvalidException de) {
			DWLStatus theStatus = params.getExtensionSetStatus();
			theStatus.addError((DWLError) de.getStatus().getDwlErrorGroup()
					.firstElement());
			de.printStackTrace();
		} catch (Exception e) {
			DWLStatus theStatus = params.getExtensionSetStatus();
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler
					.getErrorMessage(
							SVoCGoldAttributesComponentID.REFRESH_PARTY_PREFERRED_INDICATORS,
							TCRMErrorCode.UPDATE_RECORD_ERROR,
							SVoCGoldAttributesErrorReasonCode.LOWES_PARTY_PREFERRED_INDICATORS_FAILED,
							theParty.getControl(), new String[0]);
			error.setThrowable(e);
			theStatus.addError(error);
			theStatus.setStatus(DWLStatus.FATAL);
			e.printStackTrace();
		}

	}
}
