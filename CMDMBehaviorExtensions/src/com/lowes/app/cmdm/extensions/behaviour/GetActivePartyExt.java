package com.lowes.app.cmdm.extensions.behaviour;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.component.TCRMPartySearchBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.common.util.LowesGetResultBObj;
import com.lowes.app.cmdm.common.util.LowesGetSQL;
import com.lowes.app.cmdm.common.util.LowesGetResultSetProcessor;



public class GetActivePartyExt extends ClientJavaExtensionSet{
	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(GetActivePartyExt.class);
	protected DWLControl theControl = null;
	
	
	/** Party component that calls are delegated from. */
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
    public void execute(ExtensionParameters params){    	
    	
    	try {
    		String[] parameters = params.getInquiryParameters();
    		theControl = params.getControl();
    		Vector theInquiryParams = new Vector();
    		QueryConnection conn = null;
            
    		//Create a DB connection to execute SearchSQLs
			conn = DataManager.getInstance().getQueryConnection();
			
			for(int i=0; i<parameters.length; i++){
    			theInquiryParams.add(parameters[i]);
    		}
    		String inquiryLevel = theInquiryParams.elementAt(2).toString();
    		String searchSQL = LowesGetSQL.SEARCH_BY_CONTEQUIV_ID;
    		
    		Vector<LowesGetResultBObj> resultVector = executeInitialSearch(theInquiryParams, searchSQL, conn);
    		if(resultVector.size()>0){
	    		String partyId = getFirstPartyId(resultVector);
	    		
	    		/*DWLTransactionSearch searchTrans = new DWLTransactionSearch(
	    				"searchParty",
	    				createPartySearchObj(theInquiryParams, theControl),
	    				theControl);*/
	    		
	    		TCRMPartyComponent partyComponent = (TCRMPartyComponent)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
	    		TCRMPartyBObj tcrmPartyBObj = partyComponent.getParty(partyId, inquiryLevel, theControl);
	    		
	    		params.setWorkingObjectHierarchy(tcrmPartyBObj);
    		}
    		params.setSkipExecutionFlag(true);
    		
    		System.out.println("Coming into GetPartyByAdminSysKey Behaviour Extension");
    		
    	} catch (Exception e) {
			logger.error(e);			  //log it
			throw new RuntimeException(e);//throw it and let up level handle it
		}
	}
    
    /**
     * It retrieves the active partyId's for given Admin Party Id. 
     * @param request
     * @param searchSQL
     * @param conn
     * @return Vector<LowesGetResultBObj>
     * @throws Exception
     */
    private Vector<LowesGetResultBObj> executeInitialSearch(Vector request, String searchSQL, QueryConnection conn) throws Exception {
    
    	String adminSystemType = request.elementAt(0).toString();
		String adminClientNum = request.elementAt(1).toString();
		int adminSysType = Integer.parseInt(adminSystemType);
		Vector<LowesGetResultBObj> result = new Vector<LowesGetResultBObj>();
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		ResultSet rs = null;
		
		Object[] parameters = new Object[2];
        parameters[0] = adminSysType;
        parameters[1] = adminClientNum;
        
        rs = conn.queryResults(searchSQL, parameters);
        LowesGetResultSetProcessor processor = new LowesGetResultSetProcessor();
	    result = processor.getObjectFromResultSet(rs);
        
     	return result;
    }
    
    /**
     * It retrieves first Party Id
     * @param resultVector
     * @return String
     * @throws Exception
     */
    private String getFirstPartyId(Vector<LowesGetResultBObj> resultVector) throws Exception {
		
    	 LowesGetResultBObj lowesGetResultBObj =(LowesGetResultBObj)resultVector.elementAt(0);
    	 String partyId = lowesGetResultBObj.getPartyId();
    	 
    	return partyId;
    }
    

    
    private TCRMPartySearchBObj createPartySearchObj(Vector theInquiryParams, DWLControl control) {
		TCRMPartySearchBObj partySearch = new TCRMPartySearchBObj();
		String adminClientNum = theInquiryParams.elementAt(0).toString();
		String adminSystemType = theInquiryParams.elementAt(1).toString();
		String inquiryLevel = theInquiryParams.elementAt(2).toString();

		partySearch.setControl(control);
		partySearch.setAdminClientNum(adminClientNum);
		partySearch.setAdminSystemType(adminSystemType);
		partySearch.setInquiryLevelSource("1");
		partySearch.setInquiryLevelType("1");
		partySearch.setInquiryLevel(inquiryLevel);
		partySearch.setPartyFilter("ACTIVE");
		return partySearch;
	}
}

