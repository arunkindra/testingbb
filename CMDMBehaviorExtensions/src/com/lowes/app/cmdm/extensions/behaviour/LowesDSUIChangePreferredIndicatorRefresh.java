package com.lowes.app.cmdm.extensions.behaviour;

import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.dataextensions.component.XOrganizationNameBObjExt;
import com.lowes.app.cmdm.dataextensions.component.XPersonNameBObjExt;
import com.lowes.app.cmdm.common.util.LowesOrgNamePI;
import com.lowes.app.cmdm.common.util.LowesPersonNamePI;
import com.lowes.app.cmdm.common.util.RefreshPartyPreferredIndicators;
import com.lowes.app.cmdm.common.constants.SVoCGoldAttributesComponentID;
import com.lowes.app.cmdm.common.constants.SVoCGoldAttributesErrorReasonCode;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This is a Java rule that extends the behavior of WCC. @see
 * com.dwl.base.extensionFramework.ClientJavaExtensionSet
 * 
 * PersonNamePrefferedCalcExtn   LowesDSUIChangePreferedIndicatorRefresh
 * 
 * @generated
 */
public class LowesDSUIChangePreferredIndicatorRefresh extends ClientJavaExtensionSet {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
	private static final String ADD_PERSONNAME_CONST = "addPersonName";
	private static final String UPDATE_PERSONNAME_CONST = "updatePersonName";
	private static final String ADD_ORGANIZATIONNAME_CONST = "addOrganizationName";
	private static final String UPDATE_ORGANIZATIONNAME_CONST = "updateOrganizationName";
	private static final String CLIENT_SYSTEM_NAME = "DataStewardship";
	private static final String ADD_PARTY_ADDRESS_CONST = "addPartyAddress";
	private static final String UPDATE_PARTY_ADDRESS_CONST = "updatePartyAddress";
	DWLControl theControl;

	
	public void execute(ExtensionParameters params) {
		
		
		theControl = params.getControl();
		
		
		if(theControl.getClientSystemName() != null && theControl.getClientSystemName().equals(CLIENT_SYSTEM_NAME)){
    	
			
			if(params.getWorkingObjectHierarchy() instanceof TCRMPersonNameBObj){
    		
				calculateDSUIPersonNameUpdate(params,theControl);
			}
			
			if(params.getWorkingObjectHierarchy() instanceof TCRMOrganizationNameBObj){
    		
				calculateDSUIOrganizationNameUpdate(params,theControl);
        	}
			
			if(params.getWorkingObjectHierarchy() instanceof TCRMPartyAddressBObj){
	    		
				calculateDSUIPartyAddressAddUpdate(params,theControl);
        	}
		
		}
    

	}
    
   
    
    private void calculateDSUIPersonNameUpdate(ExtensionParameters params,DWLControl theControl){
    	TCRMPersonNameBObj personName;
    	LowesPersonNamePI lowesPersonNamePI;
    	XPersonNameBObjExt xpersonNameBobj;
    	
    	
    	 if(params.getTransactionType().equals(UPDATE_PERSONNAME_CONST) || params.getTransactionType().equals(ADD_PERSONNAME_CONST)){
    			
    			personName = (TCRMPersonNameBObj)params.getWorkingObjectHierarchy();
    			
    		try{
    				
    	    	TCRMPartyComponent partyComponent = (TCRMPartyComponent)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
    	    	
    	    		if(theControl.getClientSystemName() != null && theControl.getClientSystemName().equals(CLIENT_SYSTEM_NAME)){
    	    			
    	    			Vector<XPersonNameBObjExt> personNames = partyComponent.getAllPersonNames(personName.getPersonPartyId(), TCRMRecordFilter.ACTIVE, personName.getControl());
    	    			lowesPersonNamePI = new LowesPersonNamePI();
    		
    	    			if((personName.getComponentID() == null))
    	    			{
    	    				xpersonNameBobj = lowesPersonNamePI.handlePersonNameRefresh(personNames);
    	    			}
    	    				params.setSkipExecutionFlag(false);
    	    
    	    		}
    		 
    		  
    		  }
    	      catch(DWLBaseException ex)
    	      {
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		theStatus.addError((DWLError)ex.getStatus().getDwlErrorGroup().firstElement());
    	      }
    	      catch(Exception ex)
    	      {
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
                DWLError          error = errHandler.getErrorMessage(SVoCGoldAttributesComponentID.REFRESH_PERSONNAME_PREFERRED_INDICATORS,
                                                                     TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                                     SVoCGoldAttributesErrorReasonCode.REFRESH_PERSONNAME_PREFERRED_INDICATORS_FAILED,
                                                                     personName.getControl(), new String[0]);
                error.setThrowable(ex);
                theStatus.addError(error);
                theStatus.setStatus(DWLStatus.FATAL);
    	      }
    	}	
    	
    } 
    
    
    private void calculateDSUIOrganizationNameUpdate(ExtensionParameters params,DWLControl theControl){
    	TCRMOrganizationNameBObj theOrg;
    	LowesOrgNamePI lowesOrgNamePI;
    	XOrganizationNameBObjExt xorgname;
    	
    	
    	if(params.getTransactionType().equals(ADD_ORGANIZATIONNAME_CONST) || params.getTransactionType().equals(UPDATE_ORGANIZATIONNAME_CONST)){
    	    	
    	    theOrg = (TCRMOrganizationNameBObj)params.getWorkingObjectHierarchy();
    	      
    	    try{
    	    	TCRMPartyComponent partyComponent = (TCRMPartyComponent)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
    	    	theControl = params.getControl();

    	    	if(theControl.getClientSystemName() != null && theControl.getClientSystemName().equals(CLIENT_SYSTEM_NAME)){
   			
    	    		Vector<XOrganizationNameBObjExt> orgNames=partyComponent.getAllOrganizationNames(theOrg.getOrganizationPartyId(), TCRMRecordFilter.ACTIVE, theOrg.getControl());
    		
    	    		lowesOrgNamePI = new LowesOrgNamePI();
    		
    		
    	    		if((theOrg.getComponentID() == null))
    		
    	    		{
    		
    	    			xorgname= lowesOrgNamePI.handleOrgNameRefresh(orgNames);
    		
    	    			params.setSkipExecutionFlag(false);
    		
    	    		}
    		  
    	    	}
    		  
    		  
    	    }
    	        	    
    	    catch(DWLBaseException ex)
    	    
    	    {
    	    
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		theStatus.addError((DWLError)ex.getStatus().getDwlErrorGroup().firstElement());
    	        
    	    
    	    }
    	    
    	    catch(Exception ex)
    	    
    	    {
    	    
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
                DWLError          error = errHandler.getErrorMessage(SVoCGoldAttributesComponentID.REFRESH_ORGANIZATIONNAME_PREFERRED_INDICATORS,
                                                                     TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                                     SVoCGoldAttributesErrorReasonCode.REFRESH_ORGANIZATIONNAME_PREFERRED_INDICATORS_FAILED,
                                                                     theOrg.getControl(), new String[0]);
                error.setThrowable(ex);
                theStatus.addError(error);
                theStatus.setStatus(DWLStatus.FATAL);
    	        
    	    
    	    }
    	    
    	    
    	    
    	}	
    	
    
    }
    
    
    private void calculateDSUIPartyAddressAddUpdate(ExtensionParameters params,DWLControl theControl){
    	TCRMPartyAddressBObj theAddress;
    	RefreshPartyPreferredIndicators rp;
    	TCRMPartyAddressBObj addressRetHolder;
    	
    	
    	if(params.getTransactionType().equals(ADD_PARTY_ADDRESS_CONST) || params.getTransactionType().equals(UPDATE_PARTY_ADDRESS_CONST)){
    	    	
    		theAddress = (TCRMPartyAddressBObj)params.getWorkingObjectHierarchy();
    	      
    	    try{
    	    	TCRMPartyComponent partyComponent = (TCRMPartyComponent)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
    	    	theControl = params.getControl();

    	    	if(theControl.getClientSystemName() != null && theControl.getClientSystemName().equals(CLIENT_SYSTEM_NAME)){
   			
    	    		Vector<TCRMPartyAddressBObj> addressHolder=partyComponent.getAllPartyAddresses(theAddress.getPartyId(), TCRMRecordFilter.ACTIVE, theAddress.getControl());
    		
    	    		rp = new RefreshPartyPreferredIndicators();
    	    		
    		
    	    		if((theAddress.getComponentID() == null))
    		
    	    		{
    		
    	    			
    	    			addressRetHolder = rp.handlePartyAddressRefresh(addressHolder);
    	    			params.setSkipExecutionFlag(false);
    		
    	    		}
    		  
    	    	}
    		  
    		  
    	    }
    	        	    
    	    catch(DWLBaseException ex)
    	    
    	    {
    	    
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		theStatus.addError((DWLError)ex.getStatus().getDwlErrorGroup().firstElement());
    	        
    	    
    	    }
    	    
    	    catch(Exception ex)
    	    
    	    {
    	    
    	    	DWLStatus theStatus = params.getExtensionSetStatus();
        		IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
                DWLError          error = errHandler.getErrorMessage(SVoCGoldAttributesComponentID.REFRESH_PARTYADDRESS_PREFERRED_INDICATORS,
                                                                     TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                                     SVoCGoldAttributesErrorReasonCode.REFRESH_PARTYADDRESS_PREFERRED_INDICATORS_FAILED,
                                                                     theAddress.getControl(), new String[0]);
                error.setThrowable(ex);
                theStatus.addError(error);
                theStatus.setStatus(DWLStatus.FATAL);
    	        
    	    
    	    }
    	    
    	    
    	    
    	}	
    	
    
    }
}
