/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.lowes.app.cmdm.extensions.behaviour;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.ServiceLocatorException;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.common.util.TDIdGenerator;
import com.lowes.app.cmdm.dataextensions.component.XPartyAdminSysKeyBObjExt;


//**********************************************************************
//Property of Lowe's Companies, Inc.
//**********************************************************************
/**
 * This behavior extension is for CUS_ID creation when a new party is added.
 * It should be triggered as post Action of addPartySimple, addParty and updateParty transactions.
 * It generates a new CUS_ID only if:
 *   1. AddPartyStatus is NOT 3, 4 and 8
 *   2. Incoming party is NOT guest
 *   3. Incoming party does NOT already have a CUS_ID associated
 * @param extParams Instance of ExtensionParameters
 * @generated
 */
public class CusIdCreation extends ClientJavaExtensionSet {
	private final static IDWLLogger LOGGER = DWLLoggerManager.getLogger(CusIdCreation.class);
	public final static String TERADATA_SOURCE_CODE = "190";
	private final static String COMPONENT_TP_ID = "1000204";
	
	/**
     * It will create CUS_ID for non-Teradata Active (non-Guest) parties.
     * @generated
     */
    public void execute(ExtensionParameters extParams) {
    	LOGGER.info("ENTER: CusIdCreation.execute()");
    	Object transObj = extParams.getTransactionObjectHierarchy();
    	if(transObj instanceof TCRMPartyBObj) {
    		TCRMPartyBObj partyBObj = (TCRMPartyBObj) transObj;
			if("3".equals(partyBObj.getAddPartyStatus()) || "4".equals(partyBObj.getAddPartyStatus()) || "8".equals(partyBObj.getAddPartyStatus())) {
				//Add party status is A1 suspect (with different scenarios). Do nothing.
				return;
			}
			
			//Check and if it is a guest party then return
			if(("1000001").equals(partyBObj.getClientStatusType())) {
				//It's a guest party, simply return.
				return;
			}

			String partyId = partyBObj.getPartyId();
			DWLControl dwlControl = partyBObj.getControl();
			try {
				//Check and if CUS_ID is already available then return without generating a new one.
				TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
				Vector<TCRMAdminContEquivBObj> dbAdminContEquivBObjs = partyComponent.getAllPartyAdminSysKeys(partyId, dwlControl);
				int dbAdminContEquivCount = dbAdminContEquivBObjs.size();
				for (int i = 0; i < dbAdminContEquivCount; i++) {
					TCRMAdminContEquivBObj currAdminContEquivBObj = (TCRMAdminContEquivBObj)dbAdminContEquivBObjs.elementAt(i);
					if (TERADATA_SOURCE_CODE.equals(currAdminContEquivBObj.getAdminSystemType())) {
						LOGGER.info("CUS_ID already exists for party: " + partyBObj.getPartyId());
						return;
					}
				}

				long newCusId = generateNewCusId(); //Can be -1 if no available CUS_ID is generated
				if(newCusId != -1L){
		        	//Generate BObj with the CUS_ID generated and persist.
		        	XPartyAdminSysKeyBObjExt newAdminContEquivBObj = (XPartyAdminSysKeyBObjExt) TCRMClassFactory.createBObj(XPartyAdminSysKeyBObjExt.class);
					newAdminContEquivBObj.setAdminPartyId(String.valueOf(newCusId));
					newAdminContEquivBObj.setAdminSystemType(TERADATA_SOURCE_CODE);
					newAdminContEquivBObj.setDescription("Teradata System Type with Available Cus ID");
					newAdminContEquivBObj.setControl(dwlControl);
					newAdminContEquivBObj.setPartyId(partyId);
					newAdminContEquivBObj.setContEquivLastUpdateUser(partyBObj.getPartyLastUpdateUser());
					partyComponent.addPartyAdminSysKey(newAdminContEquivBObj);
					partyBObj.setTCRMAdminContEquivBObj(newAdminContEquivBObj);
					LOGGER.info("CUS_ID generated for party: " + partyId);
		        } else {
		        	//Unable to generate CUS_ID, just append a WARNING message in the response
		        	LOGGER.error("Unable to generate CUS_ID at this moment. Party ID: " + partyId);
		        	DWLStatus dwlStatus = partyBObj.getStatus();
		        	IDWLErrorMessage idwlErrMsg = TCRMClassFactory.getErrorHandler();
		    		DWLError dwlErr = idwlErrMsg.getErrorMessage(COMPONENT_TP_ID, "ADDERR", "1000205", partyBObj.getControl());
		    		String errMsg = (dwlErr.getErrorMessage() + partyId);
		    		dwlErr.setErrorMessage(errMsg);
					dwlStatus.addError(dwlErr);
		        }
			} catch (ServiceLocatorException slEx) {
				LOGGER.error(slEx.getMessage());
//				slEx.printStackTrace();
			} catch(Exception ex) {
				LOGGER.error(ex.getMessage());
//				ex.printStackTrace();
			}
    	}

    	LOGGER.info("EXIT: CusIdCreation.execute()");
	}
    
    /**
	 * This method generates a new CUS_ID using TDIdGenerator.retrieveCusId() method and then validates the newly generated CUS_ID to ensure that
	 * it has NOT already been used by some party. If it finds the newly generated CUS_ID already used then it tries once more - generate second CUS_ID
	 * and validate that. If it finds even the second CUS_ID is already used then it returns -1, which means the CUS_ID generation was failed.
	 *  
	 * @return An available CUS_ID if it generates successfully in maximum of two attempts, else -1.
	 */
	public static long generateNewCusId() {
		TDIdGenerator tdIdGen = new TDIdGenerator();
		long newCusId = tdIdGen.retrieveCusId();

		//Check if the newCusId is already used by some other party
		String cusIdUsedCountSQL = "SELECT COUNT(*) FROM CONTEQUIV WHERE ADMIN_CLIENT_ID = '" + newCusId + "' AND ADMIN_SYS_TP_CD = " + TERADATA_SOURCE_CODE;
		QueryConnection qryConn = null;
		ResultSet rs = null;
		try {
			qryConn = DataManager.getInstance().getQueryConnection();
			rs = qryConn.getConnection().createStatement().executeQuery(cusIdUsedCountSQL);

			if(rs.next()) {
				if (rs.getInt(1) == 0) {
					return newCusId;
				} else {
					LOGGER.info("Already used CUS_ID generated by TDIdGenerator. CUS_ID: " + newCusId);
				}
			}
		} catch(Exception ex) {
			LOGGER.error(ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
				} catch(SQLException sqlEx) {
					LOGGER.error("Exception while closing ResultSet: " + sqlEx.getMessage());
				}
			}
			if(qryConn != null) {
				try {
					qryConn.close();
				} catch(Exception ex) {
					LOGGER.error("Exception while closing QueryConnection: " + ex.getMessage());
				}
			}
		}

		//No available CUS_ID generated even after 2nd attempt, returning -1 so that the calling method can know the status
		return -1L;
	}
}

