/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.lowes.app.cmdm.extensions.behaviour;

import java.util.Vector;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;


//**********************************************************************
//Property of Lowe's Companies, Inc.
//**********************************************************************
/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This is a Java rule that extends the behavior of WCC. @see
 * com.dwl.base.extensionFramework.ClientJavaExtensionSet
 * 
 * @generated
 */
public class OrgNameValOverRide extends ClientJavaExtensionSet {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The execute method will be called in case of extension activation.
     *
     * @generated
     */
    public void execute(ExtensionParameters params) {
    	DWLStatus status = params.getExtensionSetStatus(); 
    	if  (status.getDwlErrorGroup() != null) {
	    	Vector errors = status.getDwlErrorGroup();
	    	
	    	for (int idx = 0; idx < errors.size(); idx++) {
				DWLError error = (DWLError) errors.elementAt(idx);
				if(error.getReasonCode() == 101 && error.getComponentType() == 1006 && error.getErrorType().equals(DWLErrorCode.DUPLICATE_RECORD_ERROR)) {
					errors.removeElementAt(idx);
					params.setSkipExecutionFlag(false);
				}
			}
    	}
    }
}

