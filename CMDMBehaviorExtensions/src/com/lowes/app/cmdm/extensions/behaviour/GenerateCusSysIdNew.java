package com.lowes.app.cmdm.extensions.behaviour;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyComponent;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.lowes.app.cmdm.common.util.PrimaryIndicatorHandler;
import com.lowes.app.cmdm.dataextensions.component.XPartyAdminSysKeyBObjExt;

//**********************************************************************
//Property of Lowe's Companies, Inc.
//**********************************************************************
/**
 * @author InfoTrellis Inc. The class GenerateCusSysIdNew is a behaviour
 *         extension to generate CUSSYSID
 */

public class GenerateCusSysIdNew extends ClientJavaExtensionSet {
	private static final IDWLLogger LOGGER = DWLLoggerManager
			.getLogger(GenerateCusSysIdNew.class);
	private final static String STERLINGSTORES_SOURCE_CODE = "234"; // Sterling
																	// Stores
																	// source-code
	private static final String LOWESDOTCOMNEXTGEN_SOURCE_CODE = "193";

	@Override
	public void execute(ExtensionParameters extParams) {
		LOGGER.info("ENTER: GenerateCusSysIdNew.execute()");
		Object transObj = extParams.getTransactionObjectHierarchy();

		if (transObj instanceof TCRMPartyBObj) {
			TCRMPartyBObj partyBObj = (TCRMPartyBObj) transObj;
			String partyId = partyBObj.getPartyId();
			DWLControl dwlControl = partyBObj.getControl();

			try {
				TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory
						.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
				Vector<TCRMAdminContEquivBObj> dbAdminContEquivBObjs = partyComponent
						.getAllPartyAdminSysKeys(partyId, dwlControl);
				// Check if there is any AdminCOntEquiv objects are available in
				// DB for this party. In some cases where incoming AdminClientId
				// is already
				// used but no incoming name matches with the existing party
				// then MaintainParty determines it to be add. But before
				// calling addParty,
				// it drops all the incoming AdminContEquiv objects from the
				// request. In this case the party will be added without any
				// AdminContEquiv.
				if (dbAdminContEquivBObjs == null
						|| dbAdminContEquivBObjs.size() == 0) {
					LOGGER.error("No AdminContEquiv found for party: "
							+ partyId);
					// Here simply generate a new CUS_SYS_ID and return, as no
					// mapping is required.
					generateNewCusSysId(partyBObj); // This will also include
													// the newly generated
													// CUS_SYS_ID in the
													// response
					return;
				}

				// Get the unmapped CUS_SYS_ID and NextGenId
				List<TCRMAdminContEquivBObj> allCusSysIds = new ArrayList<TCRMAdminContEquivBObj>();
				List<XPartyAdminSysKeyBObjExt> mappedCusSysIdsExtBObj = new ArrayList<XPartyAdminSysKeyBObjExt>();
				List<TCRMAdminContEquivBObj> unmappedNextGenIds = new ArrayList<TCRMAdminContEquivBObj>();
				boolean isCusSysIdAlreadyGenerated = false;
				for (TCRMAdminContEquivBObj dbAdminContEquivBObj : dbAdminContEquivBObjs) {
					String adminSysType = dbAdminContEquivBObj
							.getAdminSystemType();
					if (STERLINGSTORES_SOURCE_CODE.equals(adminSysType)) {
						TCRMAdminContEquivBObj newBGDTContEquiv = setBeginDateToIncomingCusSysId(
								partyBObj, dbAdminContEquivBObj);
						allCusSysIds.add(newBGDTContEquiv);
						isCusSysIdAlreadyGenerated = true; // This ensures that
															// the party already
															// has at least one
															// CUS_SYS_ID
															// generated
					} else if (LOWESDOTCOMNEXTGEN_SOURCE_CODE
							.equals(adminSysType)) {
						XPartyAdminSysKeyBObjExt xPartyAdminSysKeyBObj = (XPartyAdminSysKeyBObjExt) dbAdminContEquivBObj;
						if (STERLINGSTORES_SOURCE_CODE
								.equals(xPartyAdminSysKeyBObj
										.getXReferenceSource())) {
							mappedCusSysIdsExtBObj.add(xPartyAdminSysKeyBObj);
						} else {
							unmappedNextGenIds.add(dbAdminContEquivBObj);
						}
					}
				}

				// Get all unmapped CUS_SYS_IDs which can be used for mapping
				// unmapped NextGenIds, if any.
				List<TCRMAdminContEquivBObj> unmappedCusSysIds = new ArrayList<TCRMAdminContEquivBObj>();
				if (isCusSysIdAlreadyGenerated) {
					for (TCRMAdminContEquivBObj currCusSysId : allCusSysIds) {
						boolean isCusSysIdMapped = false;
						for (XPartyAdminSysKeyBObjExt mappedCusSysIdExtBObj : mappedCusSysIdsExtBObj) {
							if (currCusSysId.getAdminPartyId().equals(
									mappedCusSysIdExtBObj
											.getXReferenceSourceKey())) {
								isCusSysIdMapped = true;
								break;
							}
						}
						// If the currentCusSysId is not mapped then add it to
						// the unmappedCusSysIds list
						if (!isCusSysIdMapped) {
							unmappedCusSysIds.add(currCusSysId);
						}
					}
				} else {
					// Generate a new CUS_SYS_ID for the party
					TCRMAdminContEquivBObj newCusSysIdACEBObj = generateNewCusSysId(partyBObj);
					// Put the newly generated CUS_SYS_ID in the
					// unmappedCusCysIds list so that it can be used for
					// updating NextGenId mapping
					unmappedCusSysIds.add(newCusSysIdACEBObj);
				}

				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("COUNTS: All CUS_SYS_ID=" + allCusSysIds.size()
							+ ", Mapped CUS_SYS_ID="
							+ mappedCusSysIdsExtBObj.size()
							+ ", Unmapped CUS_SYS_ID="
							+ unmappedCusSysIds.size()
							+ ", Unmapped NextGenId = "
							+ unmappedNextGenIds.size());
				}

				// Update the NextGenId Vs CUS_SYS_ID mapping for all unmapped
				// NextGenIds
				List<TCRMAdminContEquivBObj> existingNewlyMappedCusSysIds = updateNextGenMappings(
						partyBObj, unmappedNextGenIds, unmappedCusSysIds);
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("COUNTS: Existing previously unmapped CUS_SYS_IDs that are mapped now to previously unmapped NextGenIds: "
							+ existingNewlyMappedCusSysIds.size());
				}

				// Populate the response to reflected new mapping of NextGenIds
				Vector<TCRMAdminContEquivBObj> resACEBObjs = partyBObj
						.getItemsTCRMAdminContEquivBObj();
				Vector<TCRMAdminContEquivBObj> allCusSysIdsInRes = new Vector<TCRMAdminContEquivBObj>();
				for (int i = 0; i < resACEBObjs.size(); i++) {
					TCRMAdminContEquivBObj resACEBObj = resACEBObjs
							.elementAt(i);
					if (LOWESDOTCOMNEXTGEN_SOURCE_CODE.equals(resACEBObj
							.getAdminSystemType())) {
						for (TCRMAdminContEquivBObj unmappedNextGenId : unmappedNextGenIds) {
							if (unmappedNextGenId.getAdminContEquivIdPK()
									.equals(resACEBObj.getAdminContEquivIdPK())) {
								resACEBObjs.set(i, unmappedNextGenId);
							}
						}
					} else if (STERLINGSTORES_SOURCE_CODE.equals(resACEBObj
							.getAdminSystemType())) {
						allCusSysIdsInRes.add(resACEBObj); // To scan through to
															// include existing
															// previously
															// unmapped
															// CUS_SYS_IDs that
															// are mapped now
					}
				}
				// Scan through the AdminContEquiv objects in response and add
				// all the exiting previously unmapped CUS_SYS_IDs that are
				// mapped now but
				// not already present in the response object. Newly generated
				// CUS_SYS_IDs, if any, would have already been included in the
				// response by
				// generateNewCusSysId() method
				for (TCRMAdminContEquivBObj existingNewlyMappedCusSysId : existingNewlyMappedCusSysIds) {
					boolean isCusSysIdAvlblInRes = false;
					for (TCRMAdminContEquivBObj resCusSysId : allCusSysIdsInRes) {
						if (existingNewlyMappedCusSysId.getAdminContEquivIdPK()
								.equals(resCusSysId.getAdminContEquivIdPK())) {
							isCusSysIdAvlblInRes = true;
							break;
						}
					}
					if (!isCusSysIdAvlblInRes) {
						partyBObj
								.setTCRMAdminContEquivBObj(existingNewlyMappedCusSysId);
					}
				}
				prepareContEquivResponse(
						partyBObj.getItemsTCRMAdminContEquivBObj(),
						dbAdminContEquivBObjs);
			} catch (Exception ex) {
				LOGGER.error("Exception occured: " + ex.getMessage());
			}
		}

		LOGGER.info("EXIT: GenerateCusSysIdNew.execute()");
	}

	/**
	 * Generates a unique numeric string which can be used as CUS_SYS_ID
	 * 
	 * @return a unique numeric string
	 */
	private static String generateNewCusSysId() {
		Random randGenerator = new Random();
		String newCusSysId = String.valueOf(System.currentTimeMillis())
				+ (10000000 + randGenerator.nextInt(89999999));
		return newCusSysId;
	}

	/**
	 * Generates a new CUS_SYS_ID and persist that for the given party. Please
	 * note that it does NOT handle the required mapping for NextGenID, it
	 * simply adds the newly created AdminContEquivBObj for the new CUS_SYS_ID
	 * to vector of the given party's AdminContEquivBObj. ITLS-2015-08-20-AK -
	 * Added Begin Date in contequiv Extension while generating cus_sys_id
	 * 
	 * @param partyBObj
	 *            Instance of TCRMPartyBObj for the party for which CUS_SYS_ID
	 *            to be generated
	 * @return newly created AdminContEquivBObj for the new CUS_SYS_ID
	 * @throws Exception
	 */
	private static TCRMAdminContEquivBObj generateNewCusSysId(
			TCRMPartyBObj partyBObj) throws Exception {
		XPartyAdminSysKeyBObjExt newAdminContEquivBObj = (XPartyAdminSysKeyBObjExt) TCRMClassFactory
				.createBObj(XPartyAdminSysKeyBObjExt.class);
		newAdminContEquivBObj.setAdminPartyId(generateNewCusSysId());
		newAdminContEquivBObj
				.setDescription("CUS_SYS_ID generated by extension class");
		newAdminContEquivBObj.setAdminSystemType(STERLINGSTORES_SOURCE_CODE);
		newAdminContEquivBObj.setControl(partyBObj.getControl());
		newAdminContEquivBObj.setPartyId(partyBObj.getPartyId());
		newAdminContEquivBObj.setContEquivLastUpdateUser(partyBObj
				.getPartyLastUpdateUser());
		newAdminContEquivBObj.setXReferenceStartDate(new Timestamp(System
				.currentTimeMillis()).toString());
		// Persist the newly generated CUS_SYS_ID
		TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
		partyComponent.addPartyAdminSysKey(newAdminContEquivBObj);
		Vector<TCRMAdminContEquivBObj> reqAdminConEquivBObjs = partyBObj
				.getItemsTCRMAdminContEquivBObj();
		reqAdminConEquivBObjs.add(newAdminContEquivBObj);
		partyBObj.setFinalResponse(newAdminContEquivBObj);
		LOGGER.info("New CUS_SYS_ID generated for party: "
				+ partyBObj.getPartyId());
		return newAdminContEquivBObj;
	}

	/**
	 * It maps all the unmappedNextGenIds to given unmappedCusSysIds. If
	 * unmappedCusSysIds are lesser than unmappedNextGenIds then it generates
	 * the required number of new CUS_SYS_IDs and map them with the unmapped
	 * NextGenIds.
	 * 
	 * @param partyBObj
	 *            Instance of TCRMPartyBObj for the party for which NextGenId
	 *            mapping is to be done
	 * @param unmappedNextGenIds
	 *            List of unmapped NextGenIds (TCRMAdminContEquivBObj)
	 * @param unmappedCusSysIds
	 *            List of unmapped CUS_SYS_IDs (TCRMAdminContEquivBObj)
	 *            available for mapping
	 * @return List of existing CUS_SYS_IDs that are mapped now to unmapped
	 *         NextGenIds
	 * @throws Exception
	 */
	private static List<TCRMAdminContEquivBObj> updateNextGenMappings(
			TCRMPartyBObj partyBObj,
			List<TCRMAdminContEquivBObj> unmappedNextGenIds,
			List<TCRMAdminContEquivBObj> unmappedCusSysIds) throws Exception {
		LOGGER.info("ENTER: GenerateCusSysIdNew.updateNextGenMappings()");
		List<TCRMAdminContEquivBObj> existingNewlyMappedCusSysIds = new ArrayList<TCRMAdminContEquivBObj>();
		if (unmappedNextGenIds == null || unmappedNextGenIds.size() == 0) {
			return existingNewlyMappedCusSysIds;
		}

		// Sort the unmappedNextGenIds by their LUD - oldest first
		sortAdminContEquivBObjsByLUD(unmappedNextGenIds);
		// Sort the unmappedCusSysIds by their LUD - oldest first
		sortAdminContEquivBObjsByLUD(unmappedCusSysIds);

		// Now both the lists unmappedNextGenIds and unmappedCusSysIds are
		// sorted by their LUD in a order of oldest first.
		// Map the first NetxGenId to the first CusSysId, second NextGenId to
		// the second CusSysId, and so on till the CUS_SYS_IDs are available.
		int nextGenIdsIndex;
		for (nextGenIdsIndex = 0; (nextGenIdsIndex < unmappedNextGenIds.size())
				&& (nextGenIdsIndex < unmappedCusSysIds.size()); nextGenIdsIndex++) {
			mapNextGenIdAndPersist(partyBObj,
					unmappedNextGenIds.get(nextGenIdsIndex),
					unmappedCusSysIds.get(nextGenIdsIndex));
			existingNewlyMappedCusSysIds.add(unmappedCusSysIds
					.get(nextGenIdsIndex));
		}

		// If there is still some NextGenId left unmapped then generate new
		// CUS_SYS_IDs and map them.
		for (; nextGenIdsIndex < unmappedNextGenIds.size(); nextGenIdsIndex++) {
			TCRMAdminContEquivBObj newCusSysId = generateNewCusSysId(partyBObj);
			mapNextGenIdAndPersist(partyBObj,
					unmappedNextGenIds.get(nextGenIdsIndex), newCusSysId);
		}
		LOGGER.info("EXIT: GenerateCusSysIdNew.updateNextGenMappings()");
		return existingNewlyMappedCusSysIds;
	}

	/**
	 * It sorts the given list of TCRMAdminContEquivBObj by their LUD in
	 * ascending order, i.e., older first.
	 * 
	 * @param listToSort
	 *            List of TCRMAdminContEquivBObj to be sorted
	 * @return sorted List of TCRMAdminContEquivBObj
	 */
	public static List<TCRMAdminContEquivBObj> sortAdminContEquivBObjsByLUD(
			List<TCRMAdminContEquivBObj> listToSort) {
		int listSize = listToSort.size();
		for (int i = 1; i < listSize; i++) {
			TCRMAdminContEquivBObj currACEBObj = listToSort.get(i);
			String currACELUD = listToSort.get(i).getContEquivLastUpdateDate();

			int j;
			for (j = (i - 1); j >= 0; j--) {
				String innerACELUD = listToSort.get(j)
						.getContEquivLastUpdateDate();
				if (PrimaryIndicatorHandler.compareDateAsString(innerACELUD,
						currACELUD) <= 0) {
					break;
				} else {
					listToSort.set((j + 1), listToSort.get(j));
				}
			}
			listToSort.set((j + 1), currACEBObj);
		}

		return listToSort;
	}

	/**
	 * Maps the given unmappedNextGenIdACEBObj to the given
	 * unmappedCusSusIdACEBObj. The calling method should ensure that the
	 * unmappedCusSusIdACEBObj is NOT already mapped to any other NextGenId,
	 * this method does not handle that.
	 * 
	 * @param partyBObj
	 *            Instance of TCRMPartyBObj for the party for which NextGenId
	 *            mapping is to be done
	 * @param unmappedNextGenACEBObj
	 *            Instance of TCRMAdminContEquivBObj which represents the
	 *            unmapped NextGenId
	 * @param unmappedCusSysIdACEBObj
	 *            Instance of TCRMAdminContEquivBObj which represents the
	 *            available CUS_SYS_ID
	 * @return Instance of XPartyAdminSysKeyBObjExt which contains mapped
	 *         NextGenId and CUS_SYS_ID
	 * @throws Exception
	 */
	public static XPartyAdminSysKeyBObjExt mapNextGenIdAndPersist(
			TCRMPartyBObj partyBObj,
			TCRMAdminContEquivBObj unmappedNextGenACEBObj,
			TCRMAdminContEquivBObj unmappedCusSysIdACEBObj) throws Exception {
		XPartyAdminSysKeyBObjExt xPartyAdminSysKeyBObj;
		
		// ITLS-2016-09-14-VK- AS PART OF UPGRADE
		
		//if (unmappedNextGenACEBObj instanceof XPartyAdminSysKeyBObjExt) {
			xPartyAdminSysKeyBObj = (XPartyAdminSysKeyBObjExt) unmappedNextGenACEBObj;
		//} else {
			//xPartyAdminSysKeyBObj = new XPartyAdminSysKeyBObjExt();
			//xPartyAdminSysKeyBObj.setControl(partyBObj.getControl());
			//xPartyAdminSysKeyBObj.setEObjContEquiv(unmappedNextGenACEBObj
			//		.getEObjContEquiv()); // TODO: Or should we use cloning
			//								// here?
		//}
		xPartyAdminSysKeyBObj.setXReferenceSource(STERLINGSTORES_SOURCE_CODE);
		//xPartyAdminSysKeyBObj.setXPartyContEquivIdPk(xPartyAdminSysKeyBObj
				//.getAdminContEquivIdPK());
		xPartyAdminSysKeyBObj.setXReferenceSourceKey(unmappedCusSysIdACEBObj
				.getAdminPartyId());
		xPartyAdminSysKeyBObj.setContEquivLastUpdateUser(partyBObj
				.getPartyLastUpdateUser());
		TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
		partyComponent.updatePartyAdminSysKey(xPartyAdminSysKeyBObj);
		// TODO: Do we need the below statement: partyBObj.setFinalResponse()
		// partyBObj.setFinalResponse(xPartyAdminSysKeyBObj); //Do we need to
		// reflect the updates in the partyBObj separately or will this work for
		// that?

		return xPartyAdminSysKeyBObj;
	}

	/**
	 * ITLS-2015-08-19-AK - This method is used to determine the CUS_SYS_ID is
	 * the new CUS_SYS_ID or the existing
	 * 
	 * @param adminSysKey1
	 * @param adminSysKey2
	 * @return
	 */
	private boolean matchAdminContEquivKey(TCRMAdminContEquivBObj adminSysKey1,
			TCRMAdminContEquivBObj adminSysKey2) {
		// admin system key key-fields: AdminPartyId, AdminSystemType
		return StringUtils.compareIgnoreCaseWithTrim(
				adminSysKey1.getAdminPartyId(), adminSysKey2.getAdminPartyId())
				&& StringUtils.compareIgnoreCaseWithTrim(
						adminSysKey1.getAdminSystemType(),
						adminSysKey2.getAdminSystemType());
	}

	/**
	 * ITLS-2015-08-19-AK - This method is used to set begin date to incoming
	 * cus_sys_id
	 * 
	 * @param partyBObj
	 * @param adminContEquivBObj
	 * @return
	 * @throws Exception
	 */
	private TCRMAdminContEquivBObj setBeginDateToIncomingCusSysId(
			TCRMPartyBObj partyBObj, TCRMAdminContEquivBObj adminContEquivBObj)
			throws Exception {
		Vector<TCRMAdminContEquivBObj> vecAdminContBObjs = partyBObj
				.getItemsTCRMAdminContEquivBObj();
		int index = -1;
		boolean isBeginDateInComing = isBeginDatePresent(adminContEquivBObj);
		TCRMAdminContEquivBObj newBDContEquiv = (TCRMAdminContEquivBObj) TCRMClassFactory
				.createBObj(TCRMAdminContEquivBObj.class);
		if (!isBeginDateInComing) {
			newBDContEquiv = setBeginDateToGenCusSysId(adminContEquivBObj);
			for (int i = 0; i < vecAdminContBObjs.size(); i++) {
				TCRMAdminContEquivBObj adminContEquiv = vecAdminContBObjs
						.get(i);
				if (matchAdminContEquivKey(adminContEquiv, adminContEquivBObj)) {
					index = i;
					break;
				}
			}
			if (index != -1) {
				vecAdminContBObjs.remove(index);
				vecAdminContBObjs.add(newBDContEquiv);
			}

		}
		return isBeginDateInComing ? adminContEquivBObj : newBDContEquiv;
	}

	/**
	 * ITLS-2015-08-19-AK - This method is used to set Begin Date to newly
	 * generated CUS_SYS_ID
	 * 
	 * @param adminContEquiv
	 * @return
	 * @throws Exception
	 */
	private TCRMAdminContEquivBObj setBeginDateToGenCusSysId(
			TCRMAdminContEquivBObj adminContEquiv) throws Exception {
		if (adminContEquiv != null) {
			XPartyAdminSysKeyBObjExt xContEquiv;
			if (adminContEquiv instanceof XPartyAdminSysKeyBObjExt) {
				xContEquiv = (XPartyAdminSysKeyBObjExt) adminContEquiv;
			} else {
				xContEquiv = new XPartyAdminSysKeyBObjExt();
				xContEquiv.setEObjContEquiv(adminContEquiv.getEObjContEquiv());
			}
			if (StringUtils.isBlank(xContEquiv.getXReferenceStartDate())) {
				xContEquiv.setXReferenceStartDate(xContEquiv
						.getContEquivLastUpdateDate());
				xContEquiv.setControl(xContEquiv.getControl());
				TCRMPartyComponent partyComponent = (TCRMPartyComponent) TCRMClassFactory
						.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
				partyComponent.updatePartyAdminSysKey(xContEquiv);
			}
			return xContEquiv;
		}
		return adminContEquiv;
	}

	/**
	 * ITLS-2015-08-20-AK - This method is used to determine BeginDate is
	 * present in ContEquiv or not
	 * 
	 * @param adminContEquiv
	 * @return
	 */
	private boolean isBeginDatePresent(TCRMAdminContEquivBObj adminContEquiv) {
		if (adminContEquiv instanceof XPartyAdminSysKeyBObjExt) {
			XPartyAdminSysKeyBObjExt xContEquiv = (XPartyAdminSysKeyBObjExt) adminContEquiv;
			if (StringUtils.isNonBlank(xContEquiv.getXReferenceStartDate())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ITLS-2015-08-31-AK -This method is used to send all the adminContequivs
	 * to update party response even though there is no update applied on it.
	 * 
	 * @param vecTempAdminContEquivs
	 * @param vecDBAdminContEquivs
	 */
	private void prepareContEquivResponse(
			Vector<TCRMAdminContEquivBObj> vecTempAdminContEquivs,
			Vector<TCRMAdminContEquivBObj> vecDBAdminContEquivs) {
		Vector<TCRMAdminContEquivBObj> vecNonMatchDBContEquivs = new Vector<TCRMAdminContEquivBObj>();
		boolean isMatch = false;
		if (vecDBAdminContEquivs != null && vecDBAdminContEquivs.size() > 0) {
			if (vecTempAdminContEquivs != null
					&& vecTempAdminContEquivs.size() > 0) {
				for (TCRMAdminContEquivBObj dbAdminContEquiv : vecDBAdminContEquivs) {
					isMatch = false;
					for (TCRMAdminContEquivBObj tempAdminContEquiv : vecTempAdminContEquivs) {
						if (matchAdminContEquivKey(dbAdminContEquiv,
								tempAdminContEquiv)) {
							isMatch = true;
							break;
						}
					}
					if (!isMatch) {
						vecNonMatchDBContEquivs.add(dbAdminContEquiv);
					}
				}
				vecTempAdminContEquivs.addAll(vecNonMatchDBContEquivs);
			} else {
				vecTempAdminContEquivs.addAll(vecDBAdminContEquivs);
			}
		}
	}
}